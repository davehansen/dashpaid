var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    sourcemaps = require('gulp-sourcemaps');
    connect = require('gulp-connect'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

gulp.task('connect', function() {
  connect.server({
    livereload: false,
    root: 'example'
  });
});

gulp.task('build', function(){
  return gulp.src("src/index.js")
    .pipe(browserify())
    .pipe(sourcemaps.init())
    .pipe(rename('index.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist'))
    .pipe(gulp.dest('example/js'));
});

gulp.task('build-debug', function(){
  return gulp.src("src/index.js")
    .pipe(browserify({
        debug: true
     }))
    .pipe(rename('index.js'))
    .pipe(gulp.dest('dist'))
    .pipe(gulp.dest('example/js'));
});

gulp.task('default', ['build', 'build-debug'], function(){});
