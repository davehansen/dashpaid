Running the project
-------------------

  - install nodejs and gulp
  - `npm install` to install all dependencies
  - `gulp build` to build and output compiled files to dist
  - `gulp connect` run local web server hosting project example files
