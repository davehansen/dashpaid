var VPAIDHTML5 = require('./vpaid.html5').VPAIDHTML5;

var dashbidTag = 'http://search.spotxchange.com/vast/2.00/85394?VPAID=js&content_page_url=__page-url__&cb=__random-number__&device[os]=Android&device[devicetype]=1&device[dnt]=0';

new VPAIDHTML5(
  dashbidTag,
  document.getElementById('video-slot'),
  registerLoggingEvents
);

function registerLoggingEvents() {
  var vpaidEvents = [
    'AdStarted',
    'AdStopped',
    'AdSkipped',
    'AdLoaded',
    'AdLinearChange',
    'AdSizeChange',
    'AdExpandedChange',
    'AdSkippableStateChange',
    'AdDurationChange',
    'AdRemainingTimeChange',
    'AdVolumeChange',
    'AdImpression',
    'AdClickThru',
    'AdInteraction',
    'AdVideoStart',
    'AdVideoFirstQuartile',
    'AdVideoMidpoint',
    'AdVideoThirdQuartile',
    'AdVideoComplete',
    'AdUserAcceptInvitation',
    'AdUserMinimize',
    'AdUserClose',
    'AdPaused',
    'AdPlaying',
    'AdError',
    'AdLog'
    ];
  var logElement = document.getElementById('ad-log-body');

  var startTime = new Date().getTime();

  for (var i in vpaidEvents) {
    this.on(vpaidEvents[i], function (vpaidEvent) {
      var timeElapsed = ((new Date().getTime() - startTime) / 1000).toFixed(1);
      logElement.innerHTML = '<tr><td>' + timeElapsed + '</td><td>' +
                              vpaidEvent.type + '</td></tr>' +
                              logElement.innerHTML;
    });

  }
}