var AdManager = function(vpaidCreative) {
  this._creative = vpaidCreative;

  if (!this.isValidInterface(vpaidCreative)) {
    return;
  }

  vpaidCreative.subscribe(this.onClickThru, 'AdClickThru', this);
};

AdManager.prototype.isValidInterface = function (vpaidAd) {
  var vpaidFunctions = [
    'collapseAd',
    'expandAd',
    'handshakeVersion',
    'initAd',
    'pauseAd',
    'resizeAd',
    'resumeAd',
    'skipAd',
    'startAd',
    'stopAd',
    'subscribe',
    'unsubscribe'
  ];

  for (var i in vpaidFunctions) {
    if (
      !vpaidAd.hasOwnProperty(vpaidFunctions[i]) &&
      typeof vpaidAd[vpaidFunctions[i]] !== 'function'
    ) {
      return false;
    }
  }

  return true;
};

AdManager.prototype.onClickThru = function(url, id, playerHandles) {
  if (typeof url === 'object') {
      var params = url;
      url = params[0];
      id = params[1];
      playerHandles = params[2];
  }

  if (playerHandles) {
    window.open(url, '_blank');
  }
};

AdManager.prototype.initAd = function(width, height, viewMode, desiredBitrate, creativeData, environmentVars) {
  this._creative.initAd(width, height, viewMode, desiredBitrate, creativeData, environmentVars);
};

AdManager.prototype.getAdExpanded = function getAdExpanded() {
  return this._creative.getAdExpanded();
};

AdManager.prototype.getAdSkippableState = function getAdSkippableState() {
  return this._creative.getAdSkippableState();
};

AdManager.prototype.getAdDuration = function getAdDuration() {
  return this._creative.getAdDuration();
};

AdManager.prototype.getAdWidth = function getAdWidth() {
  return this._creative.getAdWidth();
};

AdManager.prototype.getAdHeight = function getAdHeight() {
  return this._creative.getAdHeight();
};

AdManager.prototype.getAdRemainingTime = function getAdRemainingTime() {
  return this._creative.getAdRemainingTime();
};

AdManager.prototype.startAd = function startAd() {
  this._creative.startAd();
};

AdManager.prototype.stopAd = function stopAd() {
  this._creative.stopAd();
};

AdManager.prototype.setAdVolume = function setAdVolume(level) {
  this._creative.setAdVolume(level);
};

AdManager.prototype.getAdVolume = function getAdVolume() {
  return this._creative.getAdVolume();
};

AdManager.prototype.resizeAd = function resizeAd(width, height, viewMode) {
  this._creative.resizeAd(width, height, viewMode);
};

AdManager.prototype.pauseAd = function pauseAd() {
  this._creative.pauseAd();
};

AdManager.prototype.resumeAd = function resumeAd() {
  this._creative.resumeAd();
};

AdManager.prototype.expandAd = function expandAd() {
  this._creative.expandAd();
};

AdManager.prototype.collapseAd = function collapseAd() {
  this._creative.collapseAd();
};

AdManager.prototype.skipAd = function skipAd() {
  this._creative.skipAd();
};

module.exports = {
  'AdManager': AdManager
};