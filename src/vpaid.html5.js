require('./../node_modules/dom4/build/dom4');    // for CustomEvent support in IE

var DMVAST = require('./../node_modules/vast-client');
var AdManager = require('./AdManager.js').AdManager;

var VPAIDHTML5 = function(adTag, videoElement, cb) {
  var that = this,
      vastTag = this.applyTagMacros(adTag);

  this._cbOnReady = cb;
  this.adInfo = {};
  this.videoElement = videoElement;

  // wait for video playback to start; browsers that block autoplay make ads sad
  videoElement.addEventListener('playing', function waitForVideoReady() {
    that.videoElement.removeEventListener('playing', waitForVideoReady);

    DMVAST.client.get(vastTag, onVASTResponse.bind(that));
  });

  // load and play ad
  function onVASTResponse(response) {
    if (!response || !response.ads.length) {
      return;
    }

    this.adInfo = response.ads[0];    // TODO: only plays first ad

    this.loadAd(function () {
      this._cbOnReady();
      this.startEventListeners();

      this.playAd();      // autoplay
    }.bind(this));
  }
};

VPAIDHTML5.prototype.applyTagMacros = function applyTagMacros(vastTag) {
  var macros = {
    '__page-url__': window.location.href,
    '__random-number__': Math.round(new Date().getTime() / 1000)
  };

  for (var macro in macros) {
    vastTag = vastTag.replace(macro, macros[macro]);
  }

  return vastTag;
};

// TODO: make Promise-compatible
VPAIDHTML5.prototype.loadAd = function init(cb) {
  var mediaFile = this.getMediaFile();

  var jsCreativeTag = document.createElement('script');
  jsCreativeTag.src = mediaFile.fileURL;
  document.head.appendChild(jsCreativeTag);

  var timeStart = new Date().getTime();
  var adReadyInterval = setInterval(function() {
    var _getVPAIDAd = window['getVPAIDAd'];

    if (_getVPAIDAd && typeof _getVPAIDAd === 'function') {
      clearInterval(adReadyInterval);

      var vpaidAd = _getVPAIDAd();
      this.adManager = new AdManager(vpaidAd);
      return cb();
    }

    var timeNow = new Date().getTime();
    if (timeNow > (timeStart + 2500)) {
      clearInterval(adReadyInterval);

      console.warn('vpaid interface unavailable... timing out.');
    }
  }.bind(this), 50);
};

VPAIDHTML5.prototype.startEventListeners = function startEventListeners() {
  this.on('AdLoaded', function (e) {
    this.adManager.startAd();
  }.bind(this));

  this.on('AdError', function (e) {
    this.killAd();
  }.bind(this));

  this.on('AdStopped', function (e) {
    this.tearDown();
  }.bind(this));

  this.on('AdError AdLog', function logEventOutput(adEvent) {
    console.log(adEvent.type + ': ' + adEvent.detail.message);
  });
};

VPAIDHTML5.prototype.playAd = function playAd() {
  var creative = this.getCreative();
  var environmentVars = {
    videoSlot: this.videoElement,
    slot: this.videoElement.parentNode
  };

  this.adManager.initAd(
    parseInt(environmentVars.videoSlot.style.width),
    parseInt(environmentVars.videoSlot.style.height),
    'normal',
    -1,
    creative.adParameters,
    environmentVars
  );
};

// for failing fast
VPAIDHTML5.prototype.killAd = function killAd() {
  console.warn('forcing ad end...');

  this.tearDown();
  this.videoElement.play();
};

// clean up ad iframes and such just in case of fatal errors
VPAIDHTML5.prototype.tearDown = function tearDown() {
  if (this.adManager && this.adManager.stopAd) {
    this.adManager.stopAd();
  }

  var adWrapper = this.videoElement.parentElement;
  var adElements = adWrapper.children;

  if (adElements.length > 1) {
    for (var i=adElements.length-1;i >= 0;i--) {
      if (adElements[i] !== this.videoElement) {
        adWrapper.removeChild(adElements[i]);
      }
    }
  }
};

// interface for .subscribe()
VPAIDHTML5.prototype.on = function on(eventName, fn) {
  var events = eventName.split(' ');

  for (var i in events) {
    var eventDetails = {
      detail: {
        onEvent: fn,
        creative: this.getCreative(),
        mediaFile: this.getMediaFile()
      }
    };
    var adEvent = new CustomEvent(events[i], eventDetails);

    this.adManager._creative.subscribe(
      function (message) {
        this.detail.message = message;
        this.detail.onEvent(this);
      }.bind(adEvent),
      events[i],
      this);
  }
};

// interface for .unsubscribe()
VPAIDHTML5.prototype.off = function off(fn, eventName) {
  var events = eventName.split(' ');

  for (var i in events) {
    this.adManager._creative.unsubscribe(fn, events[i], this);
  }
};

// find and return the first linear creative available in the VAST response
VPAIDHTML5.prototype.getCreative = function getCreative() {
  for (var i in this.adInfo.creatives) {
    if (this.adInfo.creatives[i].type === 'linear') {
      return this.adInfo.creatives[i];
    }
  }
};

// get media file with ideal dimensions for ad placement
VPAIDHTML5.prototype.getMediaFile = function getMediaFile() {
  var creative = this.getCreative(),
      idealMediaFile;

  for (var i in creative.mediaFiles) {
    var mediaFile = creative.mediaFiles[i];

    if (!idealMediaFile) {
      idealMediaFile = mediaFile;
    }

    if (
      mediaFile.mimeType == 'application/javascript' &&
      mediaFile.width > idealMediaFile.width &&
      mediaFile.width <= this.videoElement.style.width
    ) {
      idealMediaFile = mediaFile;
    }
  }

  return idealMediaFile;
};

module.exports = {
  'VPAIDHTML5': VPAIDHTML5
};
