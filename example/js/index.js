(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*! (C) Andrea Giammarchi - @WebReflection - Mit Style License */
(function(e){"use strict";function t(){return c.createDocumentFragment()}function n(e){return c.createElement(e)}function r(e,t){if(!e)throw new Error("Failed to construct "+t+": 1 argument required, but only 0 present.")}function i(e){if(e.length===1)return s(e[0]);for(var n=t(),r=R.call(e),i=0;i<e.length;i++)n.appendChild(s(r[i]));return n}function s(e){return typeof e=="string"?c.createTextNode(e):e}for(var o,u,a,f,l,c=e.document,h=Object.prototype.hasOwnProperty,p=Object.defineProperty||function(e,t,n){return h.call(n,"value")?e[t]=n.value:(h.call(n,"get")&&e.__defineGetter__(t,n.get),h.call(n,"set")&&e.__defineSetter__(t,n.set)),e},d=[].indexOf||function(t){var n=this.length;while(n--)if(this[n]===t)break;return n},v=function(e){if(!e)throw"SyntaxError";if(w.test(e))throw"InvalidCharacterError";return e},m=function(e){var t=typeof e.className=="undefined",n=t?e.getAttribute("class")||"":e.className,r=t||typeof n=="object",i=(r?t?n:n.baseVal:n).replace(b,"");i.length&&q.push.apply(this,i.split(w)),this._isSVG=r,this._=e},g={get:function(){return new m(this)},set:function(){}},y="dom4-tmp-".concat(Math.random()*+(new Date)).replace(".","-"),b=/^\s+|\s+$/g,w=/\s+/,E=" ",S="classList",x=function(t,n){if(this.contains(t))n||this.remove(t);else if(n===undefined||n)n=!0,this.add(t);return!!n},T=e.DocumentFragment&&DocumentFragment.prototype,N=e.Node,C=(N||Element).prototype,k=e.CharacterData||N,L=k&&k.prototype,A=e.DocumentType,O=A&&A.prototype,M=(e.Element||N||e.HTMLElement).prototype,_=e.HTMLSelectElement||n("select").constructor,D=_.prototype.remove,P=e.ShadowRoot,H=e.SVGElement,B=/ /g,j="\\ ",F=function(e){var t=e==="querySelectorAll";return function(n){var r,i,s,o,u,a,f=this.parentNode;if(f){for(s=this.getAttribute("id")||y,o=s===y?s:s.replace(B,j),a=n.split(","),i=0;i<a.length;i++)a[i]="#"+o+" "+a[i];n=a.join(",")}s===y&&this.setAttribute("id",s),u=(f||this)[e](n),s===y&&this.removeAttribute("id");if(t){i=u.length,r=new Array(i);while(i--)r[i]=u[i]}else r=u;return r}},I=function(e){"query"in e||(e.query=M.query),"queryAll"in e||(e.queryAll=M.queryAll)},q=["matches",M.matchesSelector||M.webkitMatchesSelector||M.khtmlMatchesSelector||M.mozMatchesSelector||M.msMatchesSelector||M.oMatchesSelector||function(t){var n=this.parentNode;return!!n&&-1<d.call(n.querySelectorAll(t),this)},"closest",function(t){var n=this,r;while((r=n&&n.matches)&&!n.matches(t))n=n.parentNode;return r?n:null},"prepend",function(){var t=this.firstChild,n=i(arguments);t?this.insertBefore(n,t):this.appendChild(n)},"append",function(){this.appendChild(i(arguments))},"before",function(){var t=this.parentNode;t&&t.insertBefore(i(arguments),this)},"after",function(){var t=this.parentNode,n=this.nextSibling,r=i(arguments);t&&(n?t.insertBefore(r,n):t.appendChild(r))},"replace",function(){this.replaceWith.apply(this,arguments)},"replaceWith",function(){var t=this.parentNode;t&&t.replaceChild(i(arguments),this)},"remove",function(){var t=this.parentNode;t&&t.removeChild(this)},"query",F("querySelector"),"queryAll",F("querySelectorAll")],R=q.slice,U=q.length;U;U-=2){u=q[U-2],u in M||(M[u]=q[U-1]),u==="remove"&&(_.prototype[u]=function(){return 0<arguments.length?D.apply(this,arguments):M.remove.call(this)}),/^(?:before|after|replace|replaceWith|remove)$/.test(u)&&(k&&!(u in L)&&(L[u]=q[U-1]),A&&!(u in O)&&(O[u]=q[U-1]));if(/^(?:append|prepend)$/.test(u))if(T)u in T||(T[u]=q[U-1]);else try{t().constructor.prototype[u]=q[U-1]}catch(z){}}I(c);if(T)I(T);else try{I(t().constructor.prototype)}catch(z){}P&&I(P.prototype),n("a").matches("a")||(M[u]=function(e){return function(n){return e.call(this.parentNode?this:t().appendChild(this),n)}}(M[u])),m.prototype={length:0,add:function(){for(var t=0,n;t<arguments.length;t++)n=arguments[t],this.contains(n)||q.push.call(this,u);this._isSVG?this._.setAttribute("class",""+this):this._.className=""+this},contains:function(e){return function(n){return U=e.call(this,u=v(n)),-1<U}}([].indexOf||function(e){U=this.length;while(U--&&this[U]!==e);return U}),item:function(t){return this[t]||null},remove:function(){for(var t=0,n;t<arguments.length;t++)n=arguments[t],this.contains(n)&&q.splice.call(this,U,1);this._isSVG?this._.setAttribute("class",""+this):this._.className=""+this},toggle:x,toString:function W(){return q.join.call(this,E)}},H&&!(S in H.prototype)&&p(H.prototype,S,g),S in c.documentElement?(f=n("div")[S],f.add("a","b","a"),"a b"!=f&&(a=f.constructor.prototype,"add"in a||(a=e.TemporaryTokenList.prototype),l=function(e){return function(){var t=0;while(t<arguments.length)e.call(this,arguments[t++])}},a.add=l(a.add),a.remove=l(a.remove),a.toggle=x)):p(M,S,g),"contains"in C||p(C,"contains",{value:function(e){while(e&&e!==this)e=e.parentNode;return this===e}}),"head"in c||p(c,"head",{get:function(){return o||(o=c.getElementsByTagName("head")[0])}}),function(){for(var t,n=e.requestAnimationFrame,r=e.cancelAnimationFrame,i=["o","ms","moz","webkit"],s=i.length;!r&&s--;)n=n||e[i[s]+"RequestAnimationFrame"],r=e[i[s]+"CancelAnimationFrame"]||e[i[s]+"CancelRequestAnimationFrame"];r||(n?(t=n,n=function(e){var n=!0;return t(function(){n&&e.apply(this,arguments)}),function(){n=!1}},r=function(e){e()}):(n=function(e){return setTimeout(e,15,15)},r=function(e){clearTimeout(e)})),e.requestAnimationFrame=n,e.cancelAnimationFrame=r}();try{new e.CustomEvent("?")}catch(z){e.CustomEvent=function(e,t){function n(n,i){var s=c.createEvent(e);if(typeof n!="string")throw new Error("An event name must be provided");return e=="Event"&&(s.initCustomEvent=r),i==null&&(i=t),s.initCustomEvent(n,i.bubbles,i.cancelable,i.detail),s}function r(e,t,n,r){this.initEvent(e,t,n),this.detail=r}return n}(e.CustomEvent?"CustomEvent":"Event",{bubbles:!1,cancelable:!1,detail:null})}try{new Event("_")}catch(z){z=function(e){function t(e,t){r(arguments.length,"Event");var n=c.createEvent("Event");return t||(t={}),n.initEvent(e,!!t.bubbles,!!t.cancelable),n}return t.prototype=e.prototype,t}(e.Event||function(){}),p(e,"Event",{value:z}),Event!==z&&(Event=z)}try{new KeyboardEvent("_",{})}catch(z){z=function(t){function a(e){for(var t=[],n=["ctrlKey","Control","shiftKey","Shift","altKey","Alt","metaKey","Meta","altGraphKey","AltGraph"],r=0;r<n.length;r+=2)e[n[r]]&&t.push(n[r+1]);return t.join(" ")}function f(e,t){for(var n in t)t.hasOwnProperty(n)&&!t.hasOwnProperty.call(e,n)&&(e[n]=t[n]);return e}function l(e,t,n){try{t[e]=n[e]}catch(r){}}function h(t,o){r(arguments.length,"KeyboardEvent"),o=f(o||{},i);var u=c.createEvent(s),h=o.ctrlKey,p=o.shiftKey,d=o.altKey,v=o.metaKey,m=o.altGraphKey,g=n>3?a(o):null,y=String(o.key),b=String(o.char),w=o.location,E=o.keyCode||(o.keyCode=y)&&y.charCodeAt(0)||0,S=o.charCode||(o.charCode=b)&&b.charCodeAt(0)||0,x=o.bubbles,T=o.cancelable,N=o.repeat,C=o.locale,k=o.view||e,L;o.which||(o.which=o.keyCode);if("initKeyEvent"in u)u.initKeyEvent(t,x,T,k,h,d,p,v,E,S);else if(0<n&&"initKeyboardEvent"in u){L=[t,x,T,k];switch(n){case 1:L.push(y,w,h,p,d,v,m);break;case 2:L.push(h,d,p,v,E,S);break;case 3:L.push(y,w,h,d,p,v,m);break;case 4:L.push(y,w,g,N,C);break;default:L.push(char,y,w,g,N,C)}u.initKeyboardEvent.apply(u,L)}else u.initEvent(t,x,T);for(y in u)i.hasOwnProperty(y)&&u[y]!==o[y]&&l(y,u,o);return u}var n=0,i={"char":"",key:"",location:0,ctrlKey:!1,shiftKey:!1,altKey:!1,metaKey:!1,altGraphKey:!1,repeat:!1,locale:navigator.language,detail:0,bubbles:!1,cancelable:!1,keyCode:0,charCode:0,which:0},s;try{var o=c.createEvent("KeyboardEvent");o.initKeyboardEvent("keyup",!1,!1,e,"+",3,!0,!1,!0,!1,!1),n=(o.keyIdentifier||o.key)=="+"&&(o.keyLocation||o.location)==3&&(o.ctrlKey?o.altKey?1:3:o.shiftKey?2:4)||9}catch(u){}return s=0<n?"KeyboardEvent":"Event",h.prototype=t.prototype,h}(e.KeyboardEvent||function(){}),p(e,"KeyboardEvent",{value:z}),KeyboardEvent!==z&&(KeyboardEvent=z)}try{new MouseEvent("_",{})}catch(z){z=function(t){function n(t,n){r(arguments.length,"MouseEvent");var i=c.createEvent("MouseEvent");return n||(n={}),i.initMouseEvent(t,!!n.bubbles,!!n.cancelable,n.view||e,n.detail||1,n.screenX||0,n.screenY||0,n.clientX||0,n.clientY||0,!!n.ctrlKey,!!n.altKey,!!n.shiftKey,!!n.metaKey,n.button||0,n.relatedTarget||null),i}return n.prototype=t.prototype,n}(e.MouseEvent||function(){}),p(e,"MouseEvent",{value:z}),MouseEvent!==z&&(MouseEvent=z)}})(window),function(e){"use strict";function n(){}function r(e,t,n){function i(e){i.once&&(e.currentTarget.removeEventListener(e.type,t,i),i.removed=!0),i.passive&&(e.preventDefault=r.preventDefault),typeof i.callback=="function"?i.callback.call(this,e):i.callback&&i.callback.handleEvent(e),i.passive&&delete e.preventDefault}return i.type=e,i.callback=t,i.capture=!!n.capture,i.passive=!!n.passive,i.once=!!n.once,i.removed=!1,i}var t=e.WeakMap||function(){function s(e,i,s){n=s,t=!1,r=undefined,e.dispatchEvent(i)}function o(e){this.value=e}function u(){e++,this.__ce__=new i("@DOMMap:"+e+Math.random())}var e=0,t=!1,n=!1,r;return o.prototype.handleEvent=function(i){t=!0,n?i.currentTarget.removeEventListener(i.type,this,!1):r=this.value},u.prototype={constructor:u,"delete":function(n){return s(n,this.__ce__,!0),t},get:function(t){s(t,this.__ce__,!1);var n=r;return r=undefined,n},has:function(n){return s(n,this.__ce__,!1),t},set:function(t,n){return s(t,this.__ce__,!0),t.addEventListener(this.__ce__.type,new o(n),!1),this}},u}();n.prototype=(Object.create||Object)(null),r.preventDefault=function(){};var i=e.CustomEvent,s=Object.prototype.hasOwnProperty,o=e.dispatchEvent,u=e.addEventListener,a=e.removeEventListener,f=0,l=function(){f++},c=[].indexOf||function(t){var n=this.length;while(n--)if(this[n]===t)break;return n},h=function(e){return"".concat(e.capture?"1":"0",e.passive?"1":"0",e.once?"1":"0")},p,d;try{u("_",l,{once:!0}),o(new i("_")),o(new i("_")),a("_",l,{once:!0})}catch(v){}f!==1&&function(){function s(e){return function(s,o,u){if(u&&typeof u!="boolean"){var a=i.get(this),f=h(u),l,p,d;a||i.set(this,a=new n),s in a||(a[s]={handler:[],wrap:[]}),p=a[s],l=c.call(p.handler,o),l<0?(l=p.handler.push(o)-1,p.wrap[l]=d=new n):d=p.wrap[l],f in d||(d[f]=r(s,o,u),e.call(this,s,d[f],d[f].capture))}else e.call(this,s,o,u)}}function o(e){return function(n,r,s){if(s&&typeof s!="boolean"){var o=i.get(this),u,a,f,l;if(o&&n in o){f=o[n],a=c.call(f.handler,r);if(-1<a){u=h(s),l=f.wrap[a];if(u in l){e.call(this,n,l[u],l[u].capture),delete l[u];for(u in l)return;f.handler.splice(a,1),f.wrap.splice(a,1),f.handler.length===0&&delete o[n]}}}}else e.call(this,n,r,s)}}var i=new t;p=function(e){if(!e)return;var t=e.prototype;t.addEventListener=s(t.addEventListener),t.removeEventListener=o(t.removeEventListener)},e.EventTarget?p(EventTarget):(p(e.Text),p(e.Element||e.HTMLElement),p(e.HTMLDocument),p(e.Window||{prototype:e}),p(e.XMLHttpRequest))}()}(self);
},{}],2:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      }
      throw TypeError('Uncaught, unspecified "error" event.');
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        len = arguments.length;
        args = new Array(len - 1);
        for (i = 1; i < len; i++)
          args[i - 1] = arguments[i];
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    len = arguments.length;
    args = new Array(len - 1);
    for (i = 1; i < len; i++)
      args[i - 1] = arguments[i];

    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    var m;
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.listenerCount = function(emitter, type) {
  var ret;
  if (!emitter._events || !emitter._events[type])
    ret = 0;
  else if (isFunction(emitter._events[type]))
    ret = 1;
  else
    ret = emitter._events[type].length;
  return ret;
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],3:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTAd;

  VASTAd = (function() {
    function VASTAd() {
      this.id = null;
      this.sequence = null;
      this.system = null;
      this.title = null;
      this.description = null;
      this.advertiser = null;
      this.pricing = null;
      this.survey = null;
      this.errorURLTemplates = [];
      this.impressionURLTemplates = [];
      this.creatives = [];
      this.extensions = [];
    }

    return VASTAd;

  })();

  module.exports = VASTAd;

}).call(this);

},{}],4:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTClient, VASTParser, VASTUtil;

  VASTParser = require('./parser');

  VASTUtil = require('./util');

  VASTClient = (function() {
    function VASTClient() {}

    VASTClient.cappingFreeLunch = 0;

    VASTClient.cappingMinimumTimeInterval = 0;

    VASTClient.options = {
      withCredentials: false,
      timeout: 0
    };

    VASTClient.get = function(url, opts, cb) {
      var extend, now, options, timeSinceLastCall;
      now = +new Date();
      extend = exports.extend = function(object, properties) {
        var key, val;
        for (key in properties) {
          val = properties[key];
          object[key] = val;
        }
        return object;
      };
      if (!cb) {
        if (typeof opts === 'function') {
          cb = opts;
        }
        options = {};
      }
      options = extend(this.options, opts);
      if (this.totalCallsTimeout < now) {
        this.totalCalls = 1;
        this.totalCallsTimeout = now + (60 * 60 * 1000);
      } else {
        this.totalCalls++;
      }
      if (this.cappingFreeLunch >= this.totalCalls) {
        cb(null, new Error("VAST call canceled – FreeLunch capping not reached yet " + this.totalCalls + "/" + this.cappingFreeLunch));
        return;
      }
      timeSinceLastCall = now - this.lastSuccessfullAd;
      if (timeSinceLastCall < 0) {
        this.lastSuccessfullAd = 0;
      } else if (timeSinceLastCall < this.cappingMinimumTimeInterval) {
        cb(null, new Error("VAST call canceled – (" + this.cappingMinimumTimeInterval + ")ms minimum interval reached"));
        return;
      }
      return VASTParser.parse(url, options, (function(_this) {
        return function(response, err) {
          return cb(response, err);
        };
      })(this));
    };

    (function() {
      var defineProperty, storage;
      storage = VASTUtil.storage;
      defineProperty = Object.defineProperty;
      ['lastSuccessfullAd', 'totalCalls', 'totalCallsTimeout'].forEach(function(property) {
        defineProperty(VASTClient, property, {
          get: function() {
            return storage.getItem(property);
          },
          set: function(value) {
            return storage.setItem(property, value);
          },
          configurable: false,
          enumerable: true
        });
      });
      if (VASTClient.lastSuccessfullAd == null) {
        VASTClient.lastSuccessfullAd = 0;
      }
      if (VASTClient.totalCalls == null) {
        VASTClient.totalCalls = 0;
      }
      if (VASTClient.totalCallsTimeout == null) {
        VASTClient.totalCallsTimeout = 0;
      }
    })();

    return VASTClient;

  })();

  module.exports = VASTClient;

}).call(this);

},{"./parser":13,"./util":19}],5:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTCompanionAd;

  VASTCompanionAd = (function() {
    function VASTCompanionAd() {
      this.id = null;
      this.width = 0;
      this.height = 0;
      this.type = null;
      this.staticResource = null;
      this.htmlResource = null;
      this.iframeResource = null;
      this.companionClickThroughURLTemplate = null;
      this.trackingEvents = {};
    }

    return VASTCompanionAd;

  })();

  module.exports = VASTCompanionAd;

}).call(this);

},{}],6:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTCreative, VASTCreativeCompanion, VASTCreativeLinear, VASTCreativeNonLinear,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  VASTCreative = (function() {
    function VASTCreative() {
      this.trackingEvents = {};
    }

    return VASTCreative;

  })();

  VASTCreativeLinear = (function(superClass) {
    extend(VASTCreativeLinear, superClass);

    function VASTCreativeLinear() {
      VASTCreativeLinear.__super__.constructor.apply(this, arguments);
      this.type = "linear";
      this.duration = 0;
      this.skipDelay = null;
      this.mediaFiles = [];
      this.videoClickThroughURLTemplate = null;
      this.videoClickTrackingURLTemplates = [];
      this.videoCustomClickURLTemplates = [];
      this.adParameters = null;
      this.icons = [];
    }

    return VASTCreativeLinear;

  })(VASTCreative);

  VASTCreativeNonLinear = (function(superClass) {
    extend(VASTCreativeNonLinear, superClass);

    function VASTCreativeNonLinear() {
      VASTCreativeNonLinear.__super__.constructor.apply(this, arguments);
      this.type = "nonlinear";
      this.variations = [];
    }

    return VASTCreativeNonLinear;

  })(VASTCreative);

  VASTCreativeCompanion = (function(superClass) {
    extend(VASTCreativeCompanion, superClass);

    function VASTCreativeCompanion() {
      this.type = "companion";
      this.variations = [];
    }

    return VASTCreativeCompanion;

  })(VASTCreative);

  module.exports = {
    VASTCreativeLinear: VASTCreativeLinear,
    VASTCreativeNonLinear: VASTCreativeNonLinear,
    VASTCreativeCompanion: VASTCreativeCompanion
  };

}).call(this);

},{}],7:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTAdExtension;

  VASTAdExtension = (function() {
    function VASTAdExtension() {
      this.attributes = {};
      this.children = [];
    }

    return VASTAdExtension;

  })();

  module.exports = VASTAdExtension;

}).call(this);

},{}],8:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTAdExtensionChild;

  VASTAdExtensionChild = (function() {
    function VASTAdExtensionChild() {
      this.name = null;
      this.value = null;
      this.attributes = {};
    }

    return VASTAdExtensionChild;

  })();

  module.exports = VASTAdExtensionChild;

}).call(this);

},{}],9:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTIcon;

  VASTIcon = (function() {
    function VASTIcon() {
      this.program = null;
      this.height = 0;
      this.width = 0;
      this.xPosition = 0;
      this.yPosition = 0;
      this.apiFramework = null;
      this.offset = null;
      this.duration = 0;
      this.type = null;
      this.staticResource = null;
      this.htmlResource = null;
      this.iframeResource = null;
      this.iconClickThroughURLTemplate = null;
      this.iconClickTrackingURLTemplates = [];
      this.iconViewTrackingURLTemplate = null;
    }

    return VASTIcon;

  })();

  module.exports = VASTIcon;

}).call(this);

},{}],10:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  module.exports = {
    client: require('./client'),
    tracker: require('./tracker'),
    parser: require('./parser'),
    util: require('./util')
  };

}).call(this);

},{"./client":4,"./parser":13,"./tracker":15,"./util":19}],11:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTMediaFile;

  VASTMediaFile = (function() {
    function VASTMediaFile() {
      this.id = null;
      this.fileURL = null;
      this.deliveryType = "progressive";
      this.mimeType = null;
      this.codec = null;
      this.bitrate = 0;
      this.minBitrate = 0;
      this.maxBitrate = 0;
      this.width = 0;
      this.height = 0;
      this.apiFramework = null;
      this.scalable = null;
      this.maintainAspectRatio = null;
    }

    return VASTMediaFile;

  })();

  module.exports = VASTMediaFile;

}).call(this);

},{}],12:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTNonLinear;

  VASTNonLinear = (function() {
    function VASTNonLinear() {
      this.id = null;
      this.width = 0;
      this.height = 0;
      this.minSuggestedDuration = "00:00:00";
      this.apiFramework = "static";
      this.type = null;
      this.staticResource = null;
      this.htmlResource = null;
      this.iframeResource = null;
      this.nonlinearClickThroughURLTemplate = null;
    }

    return VASTNonLinear;

  })();

  module.exports = VASTNonLinear;

}).call(this);

},{}],13:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var EventEmitter, URLHandler, VASTAd, VASTAdExtension, VASTAdExtensionChild, VASTCompanionAd, VASTCreativeCompanion, VASTCreativeLinear, VASTCreativeNonLinear, VASTIcon, VASTMediaFile, VASTNonLinear, VASTParser, VASTResponse, VASTUtil,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  URLHandler = require('./urlhandler');

  VASTResponse = require('./response');

  VASTAd = require('./ad');

  VASTAdExtension = require('./extension');

  VASTAdExtensionChild = require('./extensionchild');

  VASTUtil = require('./util');

  VASTCreativeLinear = require('./creative').VASTCreativeLinear;

  VASTCreativeCompanion = require('./creative').VASTCreativeCompanion;

  VASTCreativeNonLinear = require('./creative').VASTCreativeNonLinear;

  VASTMediaFile = require('./mediafile');

  VASTIcon = require('./icon');

  VASTCompanionAd = require('./companionad');

  VASTNonLinear = require('./nonlinear');

  EventEmitter = require('events').EventEmitter;

  VASTParser = (function() {
    var URLTemplateFilters;

    function VASTParser() {}

    URLTemplateFilters = [];

    VASTParser.addURLTemplateFilter = function(func) {
      if (typeof func === 'function') {
        URLTemplateFilters.push(func);
      }
    };

    VASTParser.removeURLTemplateFilter = function() {
      return URLTemplateFilters.pop();
    };

    VASTParser.countURLTemplateFilters = function() {
      return URLTemplateFilters.length;
    };

    VASTParser.clearUrlTemplateFilters = function() {
      return URLTemplateFilters = [];
    };

    VASTParser.parse = function(url, options, cb) {
      if (!cb) {
        if (typeof options === 'function') {
          cb = options;
        }
        options = {};
      }
      return this._parse(url, null, options, function(err, response) {
        return cb(response, err);
      });
    };

    VASTParser.vent = new EventEmitter();

    VASTParser.track = function(templates, errorCode) {
      this.vent.emit('VAST-error', errorCode);
      return VASTUtil.track(templates, errorCode);
    };

    VASTParser.on = function(eventName, cb) {
      return this.vent.on(eventName, cb);
    };

    VASTParser.once = function(eventName, cb) {
      return this.vent.once(eventName, cb);
    };

    VASTParser._parse = function(url, parentURLs, options, cb) {
      var filter, i, len;
      if (!cb) {
        if (typeof options === 'function') {
          cb = options;
        }
        options = {};
      }
      for (i = 0, len = URLTemplateFilters.length; i < len; i++) {
        filter = URLTemplateFilters[i];
        url = filter(url);
      }
      if (parentURLs == null) {
        parentURLs = [];
      }
      parentURLs.push(url);
      return URLHandler.get(url, options, (function(_this) {
        return function(err, xml) {
          var ad, complete, j, k, len1, len2, loopIndex, node, ref, ref1, response;
          if (err != null) {
            return cb(err);
          }
          response = new VASTResponse();
          if (!(((xml != null ? xml.documentElement : void 0) != null) && xml.documentElement.nodeName === "VAST")) {
            return cb(new Error('Invalid VAST XMLDocument'));
          }
          ref = xml.documentElement.childNodes;
          for (j = 0, len1 = ref.length; j < len1; j++) {
            node = ref[j];
            if (node.nodeName === 'Error') {
              response.errorURLTemplates.push(_this.parseNodeText(node));
            }
          }
          ref1 = xml.documentElement.childNodes;
          for (k = 0, len2 = ref1.length; k < len2; k++) {
            node = ref1[k];
            if (node.nodeName === 'Ad') {
              ad = _this.parseAdElement(node);
              if (ad != null) {
                response.ads.push(ad);
              } else {
                _this.track(response.errorURLTemplates, {
                  ERRORCODE: 101
                });
              }
            }
          }
          complete = function(errorAlreadyRaised) {
            var l, len3, noCreatives, ref2;
            if (errorAlreadyRaised == null) {
              errorAlreadyRaised = false;
            }
            if (!response) {
              return;
            }
            noCreatives = true;
            ref2 = response.ads;
            for (l = 0, len3 = ref2.length; l < len3; l++) {
              ad = ref2[l];
              if (ad.nextWrapperURL != null) {
                return;
              }
              if (ad.creatives.length > 0) {
                noCreatives = false;
              }
            }
            if (noCreatives) {
              if (!errorAlreadyRaised) {
                _this.track(response.errorURLTemplates, {
                  ERRORCODE: 303
                });
              }
            }
            if (response.ads.length === 0) {
              response = null;
            }
            return cb(null, response);
          };
          loopIndex = response.ads.length;
          while (loopIndex--) {
            ad = response.ads[loopIndex];
            if (ad.nextWrapperURL == null) {
              continue;
            }
            (function(ad) {
              var baseURL, protocol, ref2;
              if (parentURLs.length >= 10 || (ref2 = ad.nextWrapperURL, indexOf.call(parentURLs, ref2) >= 0)) {
                _this.track(ad.errorURLTemplates, {
                  ERRORCODE: 302
                });
                response.ads.splice(response.ads.indexOf(ad), 1);
                complete();
                return;
              }
              if (ad.nextWrapperURL.indexOf('//') === 0) {
                protocol = location.protocol;
                ad.nextWrapperURL = "" + protocol + ad.nextWrapperURL;
              } else if (ad.nextWrapperURL.indexOf('://') === -1) {
                baseURL = url.slice(0, url.lastIndexOf('/'));
                ad.nextWrapperURL = baseURL + "/" + ad.nextWrapperURL;
              }
              return _this._parse(ad.nextWrapperURL, parentURLs, options, function(err, wrappedResponse) {
                var base, creative, errorAlreadyRaised, eventName, index, l, len3, len4, len5, len6, len7, len8, m, n, o, p, q, ref3, ref4, ref5, ref6, ref7, ref8, wrappedAd;
                errorAlreadyRaised = false;
                if (err != null) {
                  _this.track(ad.errorURLTemplates, {
                    ERRORCODE: 301
                  });
                  response.ads.splice(response.ads.indexOf(ad), 1);
                  errorAlreadyRaised = true;
                } else if (wrappedResponse == null) {
                  _this.track(ad.errorURLTemplates, {
                    ERRORCODE: 303
                  });
                  response.ads.splice(response.ads.indexOf(ad), 1);
                  errorAlreadyRaised = true;
                } else {
                  response.errorURLTemplates = response.errorURLTemplates.concat(wrappedResponse.errorURLTemplates);
                  index = response.ads.indexOf(ad);
                  response.ads.splice(index, 1);
                  ref3 = wrappedResponse.ads;
                  for (l = 0, len3 = ref3.length; l < len3; l++) {
                    wrappedAd = ref3[l];
                    wrappedAd.errorURLTemplates = ad.errorURLTemplates.concat(wrappedAd.errorURLTemplates);
                    wrappedAd.impressionURLTemplates = ad.impressionURLTemplates.concat(wrappedAd.impressionURLTemplates);
                    wrappedAd.extensions = ad.extensions.concat(wrappedAd.extensions);
                    if (ad.trackingEvents != null) {
                      ref4 = wrappedAd.creatives;
                      for (m = 0, len4 = ref4.length; m < len4; m++) {
                        creative = ref4[m];
                        if (ad.trackingEvents[creative.type] != null) {
                          ref5 = Object.keys(ad.trackingEvents[creative.type]);
                          for (n = 0, len5 = ref5.length; n < len5; n++) {
                            eventName = ref5[n];
                            (base = creative.trackingEvents)[eventName] || (base[eventName] = []);
                            creative.trackingEvents[eventName] = creative.trackingEvents[eventName].concat(ad.trackingEvents[creative.type][eventName]);
                          }
                        }
                      }
                    }
                    if (ad.videoClickTrackingURLTemplates != null) {
                      ref6 = wrappedAd.creatives;
                      for (o = 0, len6 = ref6.length; o < len6; o++) {
                        creative = ref6[o];
                        if (creative.type === 'linear') {
                          creative.videoClickTrackingURLTemplates = creative.videoClickTrackingURLTemplates.concat(ad.videoClickTrackingURLTemplates);
                        }
                      }
                    }
                    if (ad.videoCustomClickURLTemplates != null) {
                      ref7 = wrappedAd.creatives;
                      for (p = 0, len7 = ref7.length; p < len7; p++) {
                        creative = ref7[p];
                        if (creative.type === 'linear') {
                          creative.videoCustomClickURLTemplates = creative.videoCustomClickURLTemplates.concat(ad.videoCustomClickURLTemplates);
                        }
                      }
                    }
                    if (ad.videoClickThroughURLTemplate != null) {
                      ref8 = wrappedAd.creatives;
                      for (q = 0, len8 = ref8.length; q < len8; q++) {
                        creative = ref8[q];
                        if (creative.type === 'linear' && (creative.videoClickThroughURLTemplate == null)) {
                          creative.videoClickThroughURLTemplate = ad.videoClickThroughURLTemplate;
                        }
                      }
                    }
                    response.ads.splice(++index, 0, wrappedAd);
                  }
                }
                delete ad.nextWrapperURL;
                return complete(errorAlreadyRaised);
              });
            })(ad);
          }
          return complete();
        };
      })(this));
    };

    VASTParser.childByName = function(node, name) {
      var child, i, len, ref;
      ref = node.childNodes;
      for (i = 0, len = ref.length; i < len; i++) {
        child = ref[i];
        if (child.nodeName === name) {
          return child;
        }
      }
    };

    VASTParser.childsByName = function(node, name) {
      var child, childs, i, len, ref;
      childs = [];
      ref = node.childNodes;
      for (i = 0, len = ref.length; i < len; i++) {
        child = ref[i];
        if (child.nodeName === name) {
          childs.push(child);
        }
      }
      return childs;
    };

    VASTParser.parseAdElement = function(adElement) {
      var adTypeElement, i, len, ref, ref1;
      ref = adElement.childNodes;
      for (i = 0, len = ref.length; i < len; i++) {
        adTypeElement = ref[i];
        if ((ref1 = adTypeElement.nodeName) !== "Wrapper" && ref1 !== "InLine") {
          continue;
        }
        this.copyNodeAttribute("id", adElement, adTypeElement);
        this.copyNodeAttribute("sequence", adElement, adTypeElement);
        if (adTypeElement.nodeName === "Wrapper") {
          return this.parseWrapperElement(adTypeElement);
        } else if (adTypeElement.nodeName === "InLine") {
          return this.parseInLineElement(adTypeElement);
        }
      }
    };

    VASTParser.parseWrapperElement = function(wrapperElement) {
      var ad, creative, i, len, ref, wrapperCreativeElement, wrapperURLElement;
      ad = this.parseInLineElement(wrapperElement);
      wrapperURLElement = this.childByName(wrapperElement, "VASTAdTagURI");
      if (wrapperURLElement != null) {
        ad.nextWrapperURL = this.parseNodeText(wrapperURLElement);
      } else {
        wrapperURLElement = this.childByName(wrapperElement, "VASTAdTagURL");
        if (wrapperURLElement != null) {
          ad.nextWrapperURL = this.parseNodeText(this.childByName(wrapperURLElement, "URL"));
        }
      }
      ref = ad.creatives;
      for (i = 0, len = ref.length; i < len; i++) {
        creative = ref[i];
        wrapperCreativeElement = null;
        if (creative.type === 'linear' || creative.type === 'nonlinear') {
          wrapperCreativeElement = creative;
          if (wrapperCreativeElement != null) {
            if (wrapperCreativeElement.trackingEvents != null) {
              ad.trackingEvents || (ad.trackingEvents = {});
              ad.trackingEvents[wrapperCreativeElement.type] = wrapperCreativeElement.trackingEvents;
            }
            if (wrapperCreativeElement.videoClickTrackingURLTemplates != null) {
              ad.videoClickTrackingURLTemplates = wrapperCreativeElement.videoClickTrackingURLTemplates;
            }
            if (wrapperCreativeElement.videoClickThroughURLTemplate != null) {
              ad.videoClickThroughURLTemplate = wrapperCreativeElement.videoClickThroughURLTemplate;
            }
            if (wrapperCreativeElement.videoCustomClickURLTemplates != null) {
              ad.videoCustomClickURLTemplates = wrapperCreativeElement.videoCustomClickURLTemplates;
            }
          }
        }
      }
      if (ad.nextWrapperURL != null) {
        return ad;
      }
    };

    VASTParser.parseInLineElement = function(inLineElement) {
      var ad, creative, creativeElement, creativeTypeElement, i, j, k, len, len1, len2, node, ref, ref1, ref2;
      ad = new VASTAd();
      ad.id = inLineElement.getAttribute("id") || null;
      ad.sequence = inLineElement.getAttribute("sequence") || null;
      ref = inLineElement.childNodes;
      for (i = 0, len = ref.length; i < len; i++) {
        node = ref[i];
        switch (node.nodeName) {
          case "Error":
            ad.errorURLTemplates.push(this.parseNodeText(node));
            break;
          case "Impression":
            ad.impressionURLTemplates.push(this.parseNodeText(node));
            break;
          case "Creatives":
            ref1 = this.childsByName(node, "Creative");
            for (j = 0, len1 = ref1.length; j < len1; j++) {
              creativeElement = ref1[j];
              ref2 = creativeElement.childNodes;
              for (k = 0, len2 = ref2.length; k < len2; k++) {
                creativeTypeElement = ref2[k];
                switch (creativeTypeElement.nodeName) {
                  case "Linear":
                    creative = this.parseCreativeLinearElement(creativeTypeElement);
                    if (creative) {
                      ad.creatives.push(creative);
                    }
                    break;
                  case "NonLinearAds":
                    creative = this.parseNonLinear(creativeTypeElement);
                    if (creative) {
                      ad.creatives.push(creative);
                    }
                    break;
                  case "CompanionAds":
                    creative = this.parseCompanionAd(creativeTypeElement);
                    if (creative) {
                      ad.creatives.push(creative);
                    }
                }
              }
            }
            break;
          case "Extensions":
            this.parseExtension(ad.extensions, this.childsByName(node, "Extension"));
            break;
          case "AdSystem":
            ad.system = {
              value: this.parseNodeText(node),
              version: node.getAttribute("version") || null
            };
            break;
          case "AdTitle":
            ad.title = this.parseNodeText(node);
            break;
          case "Description":
            ad.description = this.parseNodeText(node);
            break;
          case "Advertiser":
            ad.advertiser = this.parseNodeText(node);
            break;
          case "Pricing":
            ad.pricing = {
              value: this.parseNodeText(node),
              model: node.getAttribute("model") || null,
              currency: node.getAttribute("currency") || null
            };
            break;
          case "Survey":
            ad.survey = this.parseNodeText(node);
        }
      }
      return ad;
    };

    VASTParser.parseExtension = function(collection, extensions) {
      var childNode, ext, extChild, extChildNodeAttr, extNode, extNodeAttr, i, j, k, l, len, len1, len2, len3, ref, ref1, ref2, results;
      results = [];
      for (i = 0, len = extensions.length; i < len; i++) {
        extNode = extensions[i];
        ext = new VASTAdExtension();
        if (extNode.attributes) {
          ref = extNode.attributes;
          for (j = 0, len1 = ref.length; j < len1; j++) {
            extNodeAttr = ref[j];
            ext.attributes[extNodeAttr.nodeName] = extNodeAttr.nodeValue;
          }
        }
        ref1 = extNode.childNodes;
        for (k = 0, len2 = ref1.length; k < len2; k++) {
          childNode = ref1[k];
          if (childNode.nodeName !== '#text') {
            extChild = new VASTAdExtensionChild();
            extChild.name = childNode.nodeName;
            extChild.value = this.parseNodeText(childNode);
            if (childNode.attributes) {
              ref2 = childNode.attributes;
              for (l = 0, len3 = ref2.length; l < len3; l++) {
                extChildNodeAttr = ref2[l];
                extChild.attributes[extChildNodeAttr.nodeName] = extChildNodeAttr.nodeValue;
              }
            }
            ext.children.push(extChild);
          }
        }
        results.push(collection.push(ext));
      }
      return results;
    };

    VASTParser.parseCreativeLinearElement = function(creativeElement) {
      var adParamsElement, base, clickTrackingElement, creative, customClickElement, eventName, htmlElement, i, icon, iconClickTrackingElement, iconClicksElement, iconElement, iconsElement, iframeElement, j, k, l, len, len1, len10, len2, len3, len4, len5, len6, len7, len8, len9, m, maintainAspectRatio, mediaFile, mediaFileElement, mediaFilesElement, n, o, offset, p, percent, q, r, ref, ref1, ref10, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, s, scalable, skipOffset, staticElement, trackingElement, trackingEventsElement, trackingURLTemplate, videoClicksElement;
      creative = new VASTCreativeLinear();
      creative.duration = this.parseDuration(this.parseNodeText(this.childByName(creativeElement, "Duration")));
      if (creative.duration === -1 && creativeElement.parentNode.parentNode.parentNode.nodeName !== 'Wrapper') {
        return null;
      }
      skipOffset = creativeElement.getAttribute("skipoffset");
      if (skipOffset == null) {
        creative.skipDelay = null;
      } else if (skipOffset.charAt(skipOffset.length - 1) === "%") {
        percent = parseInt(skipOffset, 10);
        creative.skipDelay = creative.duration * (percent / 100);
      } else {
        creative.skipDelay = this.parseDuration(skipOffset);
      }
      videoClicksElement = this.childByName(creativeElement, "VideoClicks");
      if (videoClicksElement != null) {
        creative.videoClickThroughURLTemplate = this.parseNodeText(this.childByName(videoClicksElement, "ClickThrough"));
        ref = this.childsByName(videoClicksElement, "ClickTracking");
        for (i = 0, len = ref.length; i < len; i++) {
          clickTrackingElement = ref[i];
          creative.videoClickTrackingURLTemplates.push(this.parseNodeText(clickTrackingElement));
        }
        ref1 = this.childsByName(videoClicksElement, "CustomClick");
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          customClickElement = ref1[j];
          creative.videoCustomClickURLTemplates.push(this.parseNodeText(customClickElement));
        }
      }
      adParamsElement = this.childByName(creativeElement, "AdParameters");
      if (adParamsElement != null) {
        creative.adParameters = this.parseNodeText(adParamsElement);
      }
      ref2 = this.childsByName(creativeElement, "TrackingEvents");
      for (k = 0, len2 = ref2.length; k < len2; k++) {
        trackingEventsElement = ref2[k];
        ref3 = this.childsByName(trackingEventsElement, "Tracking");
        for (l = 0, len3 = ref3.length; l < len3; l++) {
          trackingElement = ref3[l];
          eventName = trackingElement.getAttribute("event");
          trackingURLTemplate = this.parseNodeText(trackingElement);
          if ((eventName != null) && (trackingURLTemplate != null)) {
            if (eventName === "progress") {
              offset = trackingElement.getAttribute("offset");
              if (!offset) {
                continue;
              }
              if (offset.charAt(offset.length - 1) === '%') {
                eventName = "progress-" + offset;
              } else {
                eventName = "progress-" + (Math.round(this.parseDuration(offset)));
              }
            }
            if ((base = creative.trackingEvents)[eventName] == null) {
              base[eventName] = [];
            }
            creative.trackingEvents[eventName].push(trackingURLTemplate);
          }
        }
      }
      ref4 = this.childsByName(creativeElement, "MediaFiles");
      for (m = 0, len4 = ref4.length; m < len4; m++) {
        mediaFilesElement = ref4[m];
        ref5 = this.childsByName(mediaFilesElement, "MediaFile");
        for (n = 0, len5 = ref5.length; n < len5; n++) {
          mediaFileElement = ref5[n];
          mediaFile = new VASTMediaFile();
          mediaFile.id = mediaFileElement.getAttribute("id");
          mediaFile.fileURL = this.parseNodeText(mediaFileElement);
          mediaFile.deliveryType = mediaFileElement.getAttribute("delivery");
          mediaFile.codec = mediaFileElement.getAttribute("codec");
          mediaFile.mimeType = mediaFileElement.getAttribute("type");
          mediaFile.apiFramework = mediaFileElement.getAttribute("apiFramework");
          mediaFile.bitrate = parseInt(mediaFileElement.getAttribute("bitrate") || 0);
          mediaFile.minBitrate = parseInt(mediaFileElement.getAttribute("minBitrate") || 0);
          mediaFile.maxBitrate = parseInt(mediaFileElement.getAttribute("maxBitrate") || 0);
          mediaFile.width = parseInt(mediaFileElement.getAttribute("width") || 0);
          mediaFile.height = parseInt(mediaFileElement.getAttribute("height") || 0);
          scalable = mediaFileElement.getAttribute("scalable");
          if (scalable && typeof scalable === "string") {
            scalable = scalable.toLowerCase();
            if (scalable === "true") {
              mediaFile.scalable = true;
            } else if (scalable === "false") {
              mediaFile.scalable = false;
            }
          }
          maintainAspectRatio = mediaFileElement.getAttribute("maintainAspectRatio");
          if (maintainAspectRatio && typeof maintainAspectRatio === "string") {
            maintainAspectRatio = maintainAspectRatio.toLowerCase();
            if (maintainAspectRatio === "true") {
              mediaFile.maintainAspectRatio = true;
            } else if (maintainAspectRatio === "false") {
              mediaFile.maintainAspectRatio = false;
            }
          }
          creative.mediaFiles.push(mediaFile);
        }
      }
      iconsElement = this.childByName(creativeElement, "Icons");
      if (iconsElement != null) {
        ref6 = this.childsByName(iconsElement, "Icon");
        for (o = 0, len6 = ref6.length; o < len6; o++) {
          iconElement = ref6[o];
          icon = new VASTIcon();
          icon.program = iconElement.getAttribute("program");
          icon.height = parseInt(iconElement.getAttribute("height") || 0);
          icon.width = parseInt(iconElement.getAttribute("width") || 0);
          icon.xPosition = this.parseXPosition(iconElement.getAttribute("xPosition"));
          icon.yPosition = this.parseYPosition(iconElement.getAttribute("yPosition"));
          icon.apiFramework = iconElement.getAttribute("apiFramework");
          icon.offset = this.parseDuration(iconElement.getAttribute("offset"));
          icon.duration = this.parseDuration(iconElement.getAttribute("duration"));
          ref7 = this.childsByName(iconElement, "HTMLResource");
          for (p = 0, len7 = ref7.length; p < len7; p++) {
            htmlElement = ref7[p];
            icon.type = htmlElement.getAttribute("creativeType") || 'text/html';
            icon.htmlResource = this.parseNodeText(htmlElement);
          }
          ref8 = this.childsByName(iconElement, "IFrameResource");
          for (q = 0, len8 = ref8.length; q < len8; q++) {
            iframeElement = ref8[q];
            icon.type = iframeElement.getAttribute("creativeType") || 0;
            icon.iframeResource = this.parseNodeText(iframeElement);
          }
          ref9 = this.childsByName(iconElement, "StaticResource");
          for (r = 0, len9 = ref9.length; r < len9; r++) {
            staticElement = ref9[r];
            icon.type = staticElement.getAttribute("creativeType") || 0;
            icon.staticResource = this.parseNodeText(staticElement);
          }
          iconClicksElement = this.childByName(iconElement, "IconClicks");
          if (iconClicksElement != null) {
            icon.iconClickThroughURLTemplate = this.parseNodeText(this.childByName(iconClicksElement, "IconClickThrough"));
            ref10 = this.childsByName(iconClicksElement, "IconClickTracking");
            for (s = 0, len10 = ref10.length; s < len10; s++) {
              iconClickTrackingElement = ref10[s];
              icon.iconClickTrackingURLTemplates.push(this.parseNodeText(iconClickTrackingElement));
            }
          }
          icon.iconViewTrackingURLTemplate = this.parseNodeText(this.childByName(iconElement, "IconViewTracking"));
          creative.icons.push(icon);
        }
      }
      return creative;
    };

    VASTParser.parseNonLinear = function(creativeElement) {
      var base, creative, eventName, htmlElement, i, iframeElement, j, k, l, len, len1, len2, len3, len4, len5, m, n, nonlinearAd, nonlinearResource, ref, ref1, ref2, ref3, ref4, ref5, staticElement, trackingElement, trackingEventsElement, trackingURLTemplate;
      creative = new VASTCreativeNonLinear();
      ref = this.childsByName(creativeElement, "TrackingEvents");
      for (i = 0, len = ref.length; i < len; i++) {
        trackingEventsElement = ref[i];
        ref1 = this.childsByName(trackingEventsElement, "Tracking");
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          trackingElement = ref1[j];
          eventName = trackingElement.getAttribute("event");
          trackingURLTemplate = this.parseNodeText(trackingElement);
          if ((eventName != null) && (trackingURLTemplate != null)) {
            if ((base = creative.trackingEvents)[eventName] == null) {
              base[eventName] = [];
            }
            creative.trackingEvents[eventName].push(trackingURLTemplate);
          }
        }
      }
      ref2 = this.childsByName(creativeElement, "NonLinear");
      for (k = 0, len2 = ref2.length; k < len2; k++) {
        nonlinearResource = ref2[k];
        nonlinearAd = new VASTNonLinear();
        nonlinearAd.id = nonlinearResource.getAttribute("id") || null;
        nonlinearAd.width = nonlinearResource.getAttribute("width");
        nonlinearAd.height = nonlinearResource.getAttribute("height");
        nonlinearAd.minSuggestedDuration = nonlinearResource.getAttribute("minSuggestedDuration");
        nonlinearAd.apiFramework = nonlinearResource.getAttribute("apiFramework");
        ref3 = this.childsByName(nonlinearResource, "HTMLResource");
        for (l = 0, len3 = ref3.length; l < len3; l++) {
          htmlElement = ref3[l];
          nonlinearAd.type = htmlElement.getAttribute("creativeType") || 'text/html';
          nonlinearAd.htmlResource = this.parseNodeText(htmlElement);
        }
        ref4 = this.childsByName(nonlinearResource, "IFrameResource");
        for (m = 0, len4 = ref4.length; m < len4; m++) {
          iframeElement = ref4[m];
          nonlinearAd.type = iframeElement.getAttribute("creativeType") || 0;
          nonlinearAd.iframeResource = this.parseNodeText(iframeElement);
        }
        ref5 = this.childsByName(nonlinearResource, "StaticResource");
        for (n = 0, len5 = ref5.length; n < len5; n++) {
          staticElement = ref5[n];
          nonlinearAd.type = staticElement.getAttribute("creativeType") || 0;
          nonlinearAd.staticResource = this.parseNodeText(staticElement);
        }
        nonlinearAd.nonlinearClickThroughURLTemplate = this.parseNodeText(this.childByName(nonlinearResource, "NonLinearClickThrough"));
        creative.variations.push(nonlinearAd);
      }
      return creative;
    };

    VASTParser.parseCompanionAd = function(creativeElement) {
      var base, clickTrackingElement, companionAd, companionResource, creative, eventName, htmlElement, i, iframeElement, j, k, l, len, len1, len2, len3, len4, len5, len6, m, n, o, ref, ref1, ref2, ref3, ref4, ref5, ref6, staticElement, trackingElement, trackingEventsElement, trackingURLTemplate;
      creative = new VASTCreativeCompanion();
      ref = this.childsByName(creativeElement, "Companion");
      for (i = 0, len = ref.length; i < len; i++) {
        companionResource = ref[i];
        companionAd = new VASTCompanionAd();
        companionAd.id = companionResource.getAttribute("id") || null;
        companionAd.width = companionResource.getAttribute("width");
        companionAd.height = companionResource.getAttribute("height");
        companionAd.companionClickTrackingURLTemplates = [];
        ref1 = this.childsByName(companionResource, "HTMLResource");
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          htmlElement = ref1[j];
          companionAd.type = htmlElement.getAttribute("creativeType") || 'text/html';
          companionAd.htmlResource = this.parseNodeText(htmlElement);
        }
        ref2 = this.childsByName(companionResource, "IFrameResource");
        for (k = 0, len2 = ref2.length; k < len2; k++) {
          iframeElement = ref2[k];
          companionAd.type = iframeElement.getAttribute("creativeType") || 0;
          companionAd.iframeResource = this.parseNodeText(iframeElement);
        }
        ref3 = this.childsByName(companionResource, "StaticResource");
        for (l = 0, len3 = ref3.length; l < len3; l++) {
          staticElement = ref3[l];
          companionAd.type = staticElement.getAttribute("creativeType") || 0;
          companionAd.staticResource = this.parseNodeText(staticElement);
        }
        ref4 = this.childsByName(companionResource, "TrackingEvents");
        for (m = 0, len4 = ref4.length; m < len4; m++) {
          trackingEventsElement = ref4[m];
          ref5 = this.childsByName(trackingEventsElement, "Tracking");
          for (n = 0, len5 = ref5.length; n < len5; n++) {
            trackingElement = ref5[n];
            eventName = trackingElement.getAttribute("event");
            trackingURLTemplate = this.parseNodeText(trackingElement);
            if ((eventName != null) && (trackingURLTemplate != null)) {
              if ((base = companionAd.trackingEvents)[eventName] == null) {
                base[eventName] = [];
              }
              companionAd.trackingEvents[eventName].push(trackingURLTemplate);
            }
          }
        }
        ref6 = this.childsByName(companionResource, "CompanionClickTracking");
        for (o = 0, len6 = ref6.length; o < len6; o++) {
          clickTrackingElement = ref6[o];
          companionAd.companionClickTrackingURLTemplates.push(this.parseNodeText(clickTrackingElement));
        }
        companionAd.companionClickThroughURLTemplate = this.parseNodeText(this.childByName(companionResource, "CompanionClickThrough"));
        companionAd.companionClickTrackingURLTemplate = this.parseNodeText(this.childByName(companionResource, "CompanionClickTracking"));
        creative.variations.push(companionAd);
      }
      return creative;
    };

    VASTParser.parseDuration = function(durationString) {
      var durationComponents, hours, minutes, seconds, secondsAndMS;
      if (!(durationString != null)) {
        return -1;
      }
      durationComponents = durationString.split(":");
      if (durationComponents.length !== 3) {
        return -1;
      }
      secondsAndMS = durationComponents[2].split(".");
      seconds = parseInt(secondsAndMS[0]);
      if (secondsAndMS.length === 2) {
        seconds += parseFloat("0." + secondsAndMS[1]);
      }
      minutes = parseInt(durationComponents[1] * 60);
      hours = parseInt(durationComponents[0] * 60 * 60);
      if (isNaN(hours || isNaN(minutes || isNaN(seconds || minutes > 60 * 60 || seconds > 60)))) {
        return -1;
      }
      return hours + minutes + seconds;
    };

    VASTParser.parseXPosition = function(xPosition) {
      if (xPosition === "left" || xPosition === "right") {
        return xPosition;
      }
      return parseInt(xPosition || 0);
    };

    VASTParser.parseYPosition = function(yPosition) {
      if (yPosition === "top" || yPosition === "bottom") {
        return yPosition;
      }
      return parseInt(yPosition || 0);
    };

    VASTParser.parseNodeText = function(node) {
      return node && (node.textContent || node.text || '').trim();
    };

    VASTParser.copyNodeAttribute = function(attributeName, nodeSource, nodeDestination) {
      var attributeValue;
      attributeValue = nodeSource.getAttribute(attributeName);
      if (attributeValue) {
        return nodeDestination.setAttribute(attributeName, attributeValue);
      }
    };

    return VASTParser;

  })();

  module.exports = VASTParser;

}).call(this);

},{"./ad":3,"./companionad":5,"./creative":6,"./extension":7,"./extensionchild":8,"./icon":9,"./mediafile":11,"./nonlinear":12,"./response":14,"./urlhandler":16,"./util":19,"events":2}],14:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTResponse;

  VASTResponse = (function() {
    function VASTResponse() {
      this.ads = [];
      this.errorURLTemplates = [];
    }

    return VASTResponse;

  })();

  module.exports = VASTResponse;

}).call(this);

},{}],15:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var EventEmitter, VASTClient, VASTCreativeLinear, VASTTracker, VASTUtil,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  VASTClient = require('./client');

  VASTUtil = require('./util');

  VASTCreativeLinear = require('./creative').VASTCreativeLinear;

  EventEmitter = require('events').EventEmitter;

  VASTTracker = (function(superClass) {
    extend(VASTTracker, superClass);

    function VASTTracker(ad, creative) {
      var eventName, events, ref;
      this.ad = ad;
      this.creative = creative;
      this.muted = false;
      this.impressed = false;
      this.skipable = false;
      this.skipDelayDefault = -1;
      this.trackingEvents = {};
      this.emitAlwaysEvents = ['creativeView', 'start', 'firstQuartile', 'midpoint', 'thirdQuartile', 'complete', 'resume', 'pause', 'rewind', 'skip', 'closeLinear', 'close'];
      ref = this.creative.trackingEvents;
      for (eventName in ref) {
        events = ref[eventName];
        this.trackingEvents[eventName] = events.slice(0);
      }
      if (this.creative instanceof VASTCreativeLinear) {
        this.setDuration(this.creative.duration);
        this.skipDelay = this.creative.skipDelay;
        this.linear = true;
        this.clickThroughURLTemplate = this.creative.videoClickThroughURLTemplate;
        this.clickTrackingURLTemplates = this.creative.videoClickTrackingURLTemplates;
      } else {
        this.skipDelay = -1;
        this.linear = false;
      }
      this.on('start', function() {
        VASTClient.lastSuccessfullAd = +new Date();
      });
    }

    VASTTracker.prototype.setDuration = function(duration) {
      this.assetDuration = duration;
      return this.quartiles = {
        'firstQuartile': Math.round(25 * this.assetDuration) / 100,
        'midpoint': Math.round(50 * this.assetDuration) / 100,
        'thirdQuartile': Math.round(75 * this.assetDuration) / 100
      };
    };

    VASTTracker.prototype.setProgress = function(progress) {
      var eventName, events, i, len, percent, quartile, ref, skipDelay, time;
      skipDelay = this.skipDelay === null ? this.skipDelayDefault : this.skipDelay;
      if (skipDelay !== -1 && !this.skipable) {
        if (skipDelay > progress) {
          this.emit('skip-countdown', skipDelay - progress);
        } else {
          this.skipable = true;
          this.emit('skip-countdown', 0);
        }
      }
      if (this.linear && this.assetDuration > 0) {
        events = [];
        if (progress > 0) {
          events.push("start");
          percent = Math.round(progress / this.assetDuration * 100);
          events.push("progress-" + percent + "%");
          events.push("progress-" + (Math.round(progress)));
          ref = this.quartiles;
          for (quartile in ref) {
            time = ref[quartile];
            if ((time <= progress && progress <= (time + 1))) {
              events.push(quartile);
            }
          }
        }
        for (i = 0, len = events.length; i < len; i++) {
          eventName = events[i];
          this.track(eventName, true);
        }
        if (progress < this.progress) {
          this.track("rewind");
        }
      }
      return this.progress = progress;
    };

    VASTTracker.prototype.setMuted = function(muted) {
      if (this.muted !== muted) {
        this.track(muted ? "mute" : "unmute");
      }
      return this.muted = muted;
    };

    VASTTracker.prototype.setPaused = function(paused) {
      if (this.paused !== paused) {
        this.track(paused ? "pause" : "resume");
      }
      return this.paused = paused;
    };

    VASTTracker.prototype.setFullscreen = function(fullscreen) {
      if (this.fullscreen !== fullscreen) {
        this.track(fullscreen ? "fullscreen" : "exitFullscreen");
      }
      return this.fullscreen = fullscreen;
    };

    VASTTracker.prototype.setSkipDelay = function(duration) {
      if (typeof duration === 'number') {
        return this.skipDelay = duration;
      }
    };

    VASTTracker.prototype.load = function() {
      if (!this.impressed) {
        this.impressed = true;
        this.trackURLs(this.ad.impressionURLTemplates);
        return this.track("creativeView");
      }
    };

    VASTTracker.prototype.errorWithCode = function(errorCode) {
      return this.trackURLs(this.ad.errorURLTemplates, {
        ERRORCODE: errorCode
      });
    };

    VASTTracker.prototype.complete = function() {
      return this.track("complete");
    };

    VASTTracker.prototype.close = function() {
      return this.track(this.linear ? "closeLinear" : "close");
    };

    VASTTracker.prototype.stop = function() {};

    VASTTracker.prototype.skip = function() {
      this.track("skip");
      return this.trackingEvents = [];
    };

    VASTTracker.prototype.click = function() {
      var clickThroughURL, ref, variables;
      if ((ref = this.clickTrackingURLTemplates) != null ? ref.length : void 0) {
        this.trackURLs(this.clickTrackingURLTemplates);
      }
      if (this.clickThroughURLTemplate != null) {
        if (this.linear) {
          variables = {
            CONTENTPLAYHEAD: this.progressFormated()
          };
        }
        clickThroughURL = VASTUtil.resolveURLTemplates([this.clickThroughURLTemplate], variables)[0];
        return this.emit("clickthrough", clickThroughURL);
      }
    };

    VASTTracker.prototype.track = function(eventName, once) {
      var idx, trackingURLTemplates;
      if (once == null) {
        once = false;
      }
      if (eventName === 'closeLinear' && ((this.trackingEvents[eventName] == null) && (this.trackingEvents['close'] != null))) {
        eventName = 'close';
      }
      trackingURLTemplates = this.trackingEvents[eventName];
      idx = this.emitAlwaysEvents.indexOf(eventName);
      if (trackingURLTemplates != null) {
        this.emit(eventName, '');
        this.trackURLs(trackingURLTemplates);
      } else if (idx !== -1) {
        this.emit(eventName, '');
      }
      if (once === true) {
        delete this.trackingEvents[eventName];
        if (idx > -1) {
          this.emitAlwaysEvents.splice(idx, 1);
        }
      }
    };

    VASTTracker.prototype.trackURLs = function(URLTemplates, variables) {
      if (variables == null) {
        variables = {};
      }
      if (this.linear) {
        variables["CONTENTPLAYHEAD"] = this.progressFormated();
      }
      return VASTUtil.track(URLTemplates, variables);
    };

    VASTTracker.prototype.progressFormated = function() {
      var h, m, ms, s, seconds;
      seconds = parseInt(this.progress);
      h = seconds / (60 * 60);
      if (h.length < 2) {
        h = "0" + h;
      }
      m = seconds / 60 % 60;
      if (m.length < 2) {
        m = "0" + m;
      }
      s = seconds % 60;
      if (s.length < 2) {
        s = "0" + m;
      }
      ms = parseInt((this.progress - seconds) * 100);
      return h + ":" + m + ":" + s + "." + ms;
    };

    return VASTTracker;

  })(EventEmitter);

  module.exports = VASTTracker;

}).call(this);

},{"./client":4,"./creative":6,"./util":19,"events":2}],16:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var URLHandler, flash, xhr;

  xhr = require('./urlhandlers/xmlhttprequest');

  flash = require('./urlhandlers/flash');

  URLHandler = (function() {
    function URLHandler() {}

    URLHandler.get = function(url, options, cb) {
      var ref, response;
      if (!cb) {
        if (typeof options === 'function') {
          cb = options;
        }
        options = {};
      }
      if (options.response != null) {
        response = options.response;
        delete options.response;
        return cb(null, response);
      } else if ((ref = options.urlhandler) != null ? ref.supported() : void 0) {
        return options.urlhandler.get(url, options, cb);
      } else if (typeof window === "undefined" || window === null) {
        return require('./urlhandlers/' + 'node').get(url, options, cb);
      } else if (xhr.supported()) {
        return xhr.get(url, options, cb);
      } else if (flash.supported()) {
        return flash.get(url, options, cb);
      } else {
        return cb(new Error('Current context is not supported by any of the default URLHandlers. Please provide a custom URLHandler'));
      }
    };

    return URLHandler;

  })();

  module.exports = URLHandler;

}).call(this);

},{"./urlhandlers/flash":17,"./urlhandlers/xmlhttprequest":18}],17:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var FlashURLHandler;

  FlashURLHandler = (function() {
    function FlashURLHandler() {}

    FlashURLHandler.xdr = function() {
      var xdr;
      if (window.XDomainRequest) {
        xdr = new XDomainRequest();
      }
      return xdr;
    };

    FlashURLHandler.supported = function() {
      return !!this.xdr();
    };

    FlashURLHandler.get = function(url, options, cb) {
      var xdr, xmlDocument;
      if (xmlDocument = typeof window.ActiveXObject === "function" ? new window.ActiveXObject("Microsoft.XMLDOM") : void 0) {
        xmlDocument.async = false;
      } else {
        return cb(new Error('FlashURLHandler: Microsoft.XMLDOM format not supported'));
      }
      xdr = this.xdr();
      xdr.open('GET', url);
      xdr.timeout = options.timeout || 0;
      xdr.withCredentials = options.withCredentials || false;
      xdr.send();
      xdr.onprogress = function() {};
      return xdr.onload = function() {
        xmlDocument.loadXML(xdr.responseText);
        return cb(null, xmlDocument);
      };
    };

    return FlashURLHandler;

  })();

  module.exports = FlashURLHandler;

}).call(this);

},{}],18:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var XHRURLHandler;

  XHRURLHandler = (function() {
    function XHRURLHandler() {}

    XHRURLHandler.xhr = function() {
      var xhr;
      xhr = new window.XMLHttpRequest();
      if ('withCredentials' in xhr) {
        return xhr;
      }
    };

    XHRURLHandler.supported = function() {
      return !!this.xhr();
    };

    XHRURLHandler.get = function(url, options, cb) {
      var xhr;
      if (window.location.protocol === 'https:' && url.indexOf('http://') === 0) {
        return cb(new Error('XHRURLHandler: Cannot go from HTTPS to HTTP.'));
      }
      try {
        xhr = this.xhr();
        xhr.open('GET', url);
        xhr.timeout = options.timeout || 0;
        xhr.withCredentials = options.withCredentials || false;
        xhr.overrideMimeType && xhr.overrideMimeType('text/xml');
        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              return cb(null, xhr.responseXML);
            } else {
              return cb(new Error("XHRURLHandler: " + xhr.statusText));
            }
          }
        };
        return xhr.send();
      } catch (error) {
        return cb(new Error('XHRURLHandler: Unexpected error'));
      }
    };

    return XHRURLHandler;

  })();

  module.exports = XHRURLHandler;

}).call(this);

},{}],19:[function(require,module,exports){
// Generated by CoffeeScript 1.11.1
(function() {
  var VASTUtil;

  VASTUtil = (function() {
    function VASTUtil() {}

    VASTUtil.track = function(URLTemplates, variables) {
      var URL, URLs, i, j, len, results;
      URLs = this.resolveURLTemplates(URLTemplates, variables);
      results = [];
      for (j = 0, len = URLs.length; j < len; j++) {
        URL = URLs[j];
        if (typeof window !== "undefined" && window !== null) {
          i = new Image();
          results.push(i.src = URL);
        } else {

        }
      }
      return results;
    };

    VASTUtil.resolveURLTemplates = function(URLTemplates, variables) {
      var URLTemplate, URLs, j, key, len, macro1, macro2, resolveURL, value;
      URLs = [];
      if (variables == null) {
        variables = {};
      }
      if (!("CACHEBUSTING" in variables)) {
        variables["CACHEBUSTING"] = Math.round(Math.random() * 1.0e+10);
      }
      variables["random"] = variables["CACHEBUSTING"];
      for (j = 0, len = URLTemplates.length; j < len; j++) {
        URLTemplate = URLTemplates[j];
        resolveURL = URLTemplate;
        if (!resolveURL) {
          continue;
        }
        for (key in variables) {
          value = variables[key];
          macro1 = "[" + key + "]";
          macro2 = "%%" + key + "%%";
          resolveURL = resolveURL.replace(macro1, value);
          resolveURL = resolveURL.replace(macro2, value);
        }
        URLs.push(resolveURL);
      }
      return URLs;
    };

    VASTUtil.storage = (function() {
      var data, isDisabled, storage, storageError;
      try {
        storage = typeof window !== "undefined" && window !== null ? window.localStorage || window.sessionStorage : null;
      } catch (error) {
        storageError = error;
        storage = null;
      }
      isDisabled = function(store) {
        var e, testValue;
        try {
          testValue = '__VASTUtil__';
          store.setItem(testValue, testValue);
          if (store.getItem(testValue) !== testValue) {
            return true;
          }
        } catch (error) {
          e = error;
          return true;
        }
        return false;
      };
      if ((storage == null) || isDisabled(storage)) {
        data = {};
        storage = {
          length: 0,
          getItem: function(key) {
            return data[key];
          },
          setItem: function(key, value) {
            data[key] = value;
            this.length = Object.keys(data).length;
          },
          removeItem: function(key) {
            delete data[key];
            this.length = Object.keys(data).length;
          },
          clear: function() {
            data = {};
            this.length = 0;
          }
        };
      }
      return storage;
    })();

    return VASTUtil;

  })();

  module.exports = VASTUtil;

}).call(this);

},{}],20:[function(require,module,exports){
var AdManager = function(vpaidCreative) {
  this._creative = vpaidCreative;

  if (!this.isValidInterface(vpaidCreative)) {
    return;
  }

  vpaidCreative.subscribe(this.onClickThru, 'AdClickThru', this);
};

AdManager.prototype.isValidInterface = function (vpaidAd) {
  var vpaidFunctions = [
    'collapseAd',
    'expandAd',
    'handshakeVersion',
    'initAd',
    'pauseAd',
    'resizeAd',
    'resumeAd',
    'skipAd',
    'startAd',
    'stopAd',
    'subscribe',
    'unsubscribe'
  ];

  for (var i in vpaidFunctions) {
    if (
      !vpaidAd.hasOwnProperty(vpaidFunctions[i]) &&
      typeof vpaidAd[vpaidFunctions[i]] !== 'function'
    ) {
      return false;
    }
  }

  return true;
};

AdManager.prototype.onClickThru = function(url, id, playerHandles) {
  if (typeof url === 'object') {
      var params = url;
      url = params[0];
      id = params[1];
      playerHandles = params[2];
  }

  if (playerHandles) {
    window.open(url, '_blank');
  }
};

AdManager.prototype.initAd = function(width, height, viewMode, desiredBitrate, creativeData, environmentVars) {
  this._creative.initAd(width, height, viewMode, desiredBitrate, creativeData, environmentVars);
};

AdManager.prototype.getAdExpanded = function getAdExpanded() {
  return this._creative.getAdExpanded();
};

AdManager.prototype.getAdSkippableState = function getAdSkippableState() {
  return this._creative.getAdSkippableState();
};

AdManager.prototype.getAdDuration = function getAdDuration() {
  return this._creative.getAdDuration();
};

AdManager.prototype.getAdWidth = function getAdWidth() {
  return this._creative.getAdWidth();
};

AdManager.prototype.getAdHeight = function getAdHeight() {
  return this._creative.getAdHeight();
};

AdManager.prototype.getAdRemainingTime = function getAdRemainingTime() {
  return this._creative.getAdRemainingTime();
};

AdManager.prototype.startAd = function startAd() {
  this._creative.startAd();
};

AdManager.prototype.stopAd = function stopAd() {
  this._creative.stopAd();
};

AdManager.prototype.setAdVolume = function setAdVolume(level) {
  this._creative.setAdVolume(level);
};

AdManager.prototype.getAdVolume = function getAdVolume() {
  return this._creative.getAdVolume();
};

AdManager.prototype.resizeAd = function resizeAd(width, height, viewMode) {
  this._creative.resizeAd(width, height, viewMode);
};

AdManager.prototype.pauseAd = function pauseAd() {
  this._creative.pauseAd();
};

AdManager.prototype.resumeAd = function resumeAd() {
  this._creative.resumeAd();
};

AdManager.prototype.expandAd = function expandAd() {
  this._creative.expandAd();
};

AdManager.prototype.collapseAd = function collapseAd() {
  this._creative.collapseAd();
};

AdManager.prototype.skipAd = function skipAd() {
  this._creative.skipAd();
};

module.exports = {
  'AdManager': AdManager
};
},{}],21:[function(require,module,exports){
var VPAIDHTML5 = require('./vpaid.html5').VPAIDHTML5;

var dashbidTag = 'http://search.spotxchange.com/vast/2.00/85394?VPAID=js&content_page_url=__page-url__&cb=__random-number__&device[os]=Android&device[devicetype]=1&device[dnt]=0';

new VPAIDHTML5(
  dashbidTag,
  document.getElementById('video-slot'),
  registerLoggingEvents
);

function registerLoggingEvents() {
  var vpaidEvents = [
    'AdStarted',
    'AdStopped',
    'AdSkipped',
    'AdLoaded',
    'AdLinearChange',
    'AdSizeChange',
    'AdExpandedChange',
    'AdSkippableStateChange',
    'AdDurationChange',
    'AdRemainingTimeChange',
    'AdVolumeChange',
    'AdImpression',
    'AdClickThru',
    'AdInteraction',
    'AdVideoStart',
    'AdVideoFirstQuartile',
    'AdVideoMidpoint',
    'AdVideoThirdQuartile',
    'AdVideoComplete',
    'AdUserAcceptInvitation',
    'AdUserMinimize',
    'AdUserClose',
    'AdPaused',
    'AdPlaying',
    'AdError',
    'AdLog'
    ];
  var logElement = document.getElementById('ad-log-body');

  var startTime = new Date().getTime();

  for (var i in vpaidEvents) {
    this.on(vpaidEvents[i], function (vpaidEvent) {
      var timeElapsed = ((new Date().getTime() - startTime) / 1000).toFixed(1);
      logElement.innerHTML = '<tr><td>' + timeElapsed + '</td><td>' +
                              vpaidEvent.type + '</td></tr>' +
                              logElement.innerHTML;
    });

  }
}
},{"./vpaid.html5":22}],22:[function(require,module,exports){
require('./../node_modules/dom4/build/dom4');    // for CustomEvent support in IE

var DMVAST = require('./../node_modules/vast-client');
var AdManager = require('./AdManager.js').AdManager;

var VPAIDHTML5 = function(adTag, videoElement, cb) {
  var that = this,
      vastTag = this.applyTagMacros(adTag);

  this._cbOnReady = cb;
  this.adInfo = {};
  this.videoElement = videoElement;

  // wait for video playback to start; browsers that block autoplay make ads sad
  videoElement.addEventListener('playing', function waitForVideoReady() {
    that.videoElement.removeEventListener('playing', waitForVideoReady);

    DMVAST.client.get(vastTag, onVASTResponse.bind(that));
  });

  // load and play ad
  function onVASTResponse(response) {
    if (!response || !response.ads.length) {
      return;
    }

    this.adInfo = response.ads[0];    // TODO: only plays first ad

    this.loadAd(function () {
      this._cbOnReady();
      this.startEventListeners();

      this.playAd();      // autoplay
    }.bind(this));
  }
};

VPAIDHTML5.prototype.applyTagMacros = function applyTagMacros(vastTag) {
  var macros = {
    '__page-url__': window.location.href,
    '__random-number__': Math.round(new Date().getTime() / 1000)
  };

  for (var macro in macros) {
    vastTag = vastTag.replace(macro, macros[macro]);
  }

  return vastTag;
};

// TODO: make Promise-compatible
VPAIDHTML5.prototype.loadAd = function init(cb) {
  var mediaFile = this.getMediaFile();

  var jsCreativeTag = document.createElement('script');
  jsCreativeTag.src = mediaFile.fileURL;
  document.head.appendChild(jsCreativeTag);

  var timeStart = new Date().getTime();
  var adReadyInterval = setInterval(function() {
    var _getVPAIDAd = window['getVPAIDAd'];

    if (_getVPAIDAd && typeof _getVPAIDAd === 'function') {
      clearInterval(adReadyInterval);

      var vpaidAd = _getVPAIDAd();
      this.adManager = new AdManager(vpaidAd);
      return cb();
    }

    var timeNow = new Date().getTime();
    if (timeNow > (timeStart + 2500)) {
      clearInterval(adReadyInterval);

      console.warn('vpaid interface unavailable... timing out.');
    }
  }.bind(this), 50);
};

VPAIDHTML5.prototype.startEventListeners = function startEventListeners() {
  this.on('AdLoaded', function (e) {
    this.adManager.startAd();
  }.bind(this));

  this.on('AdError', function (e) {
    this.killAd();
  }.bind(this));

  this.on('AdStopped', function (e) {
    this.tearDown();
  }.bind(this));

  this.on('AdError AdLog', function logEventOutput(adEvent) {
    console.log(adEvent.type + ': ' + adEvent.detail.message);
  });
};

VPAIDHTML5.prototype.playAd = function playAd() {
  var creative = this.getCreative();
  var environmentVars = {
    videoSlot: this.videoElement,
    slot: this.videoElement.parentNode
  };

  this.adManager.initAd(
    parseInt(environmentVars.videoSlot.style.width),
    parseInt(environmentVars.videoSlot.style.height),
    'normal',
    -1,
    creative.adParameters,
    environmentVars
  );
};

// for failing fast
VPAIDHTML5.prototype.killAd = function killAd() {
  console.warn('forcing ad end...');

  this.tearDown();
  this.videoElement.play();
};

// clean up ad iframes and such just in case of fatal errors
VPAIDHTML5.prototype.tearDown = function tearDown() {
  if (this.adManager && this.adManager.stopAd) {
    this.adManager.stopAd();
  }

  var adWrapper = this.videoElement.parentElement;
  var adElements = adWrapper.children;

  if (adElements.length > 1) {
    for (var i=adElements.length-1;i >= 0;i--) {
      if (adElements[i] !== this.videoElement) {
        adWrapper.removeChild(adElements[i]);
      }
    }
  }
};

// interface for .subscribe()
VPAIDHTML5.prototype.on = function on(eventName, fn) {
  var events = eventName.split(' ');

  for (var i in events) {
    var eventDetails = {
      detail: {
        onEvent: fn,
        creative: this.getCreative(),
        mediaFile: this.getMediaFile()
      }
    };
    var adEvent = new CustomEvent(events[i], eventDetails);

    this.adManager._creative.subscribe(
      function (message) {
        this.detail.message = message;
        this.detail.onEvent(this);
      }.bind(adEvent),
      events[i],
      this);
  }
};

// interface for .unsubscribe()
VPAIDHTML5.prototype.off = function off(fn, eventName) {
  var events = eventName.split(' ');

  for (var i in events) {
    this.adManager._creative.unsubscribe(fn, events[i], this);
  }
};

// find and return the first linear creative available in the VAST response
VPAIDHTML5.prototype.getCreative = function getCreative() {
  for (var i in this.adInfo.creatives) {
    if (this.adInfo.creatives[i].type === 'linear') {
      return this.adInfo.creatives[i];
    }
  }
};

// get media file with ideal dimensions for ad placement
VPAIDHTML5.prototype.getMediaFile = function getMediaFile() {
  var creative = this.getCreative(),
      idealMediaFile;

  for (var i in creative.mediaFiles) {
    var mediaFile = creative.mediaFiles[i];

    if (!idealMediaFile) {
      idealMediaFile = mediaFile;
    }

    if (
      mediaFile.mimeType == 'application/javascript' &&
      mediaFile.width > idealMediaFile.width &&
      mediaFile.width <= this.videoElement.style.width
    ) {
      idealMediaFile = mediaFile;
    }
  }

  return idealMediaFile;
};

module.exports = {
  'VPAIDHTML5': VPAIDHTML5
};

},{"./../node_modules/dom4/build/dom4":1,"./../node_modules/vast-client":10,"./AdManager.js":20}]},{},[21])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy9kb200L2J1aWxkL2RvbTQuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvbm9kZV9tb2R1bGVzL2V2ZW50cy9ldmVudHMuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvbm9kZV9tb2R1bGVzL3Zhc3QtY2xpZW50L2Rpc3QvYWQuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvbm9kZV9tb2R1bGVzL3Zhc3QtY2xpZW50L2Rpc3QvY2xpZW50LmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L2NvbXBhbmlvbmFkLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L2NyZWF0aXZlLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L2V4dGVuc2lvbi5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC9leHRlbnNpb25jaGlsZC5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC9pY29uLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L2luZGV4LmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L21lZGlhZmlsZS5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC9ub25saW5lYXIuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvbm9kZV9tb2R1bGVzL3Zhc3QtY2xpZW50L2Rpc3QvcGFyc2VyLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L3Jlc3BvbnNlLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudC9kaXN0L3RyYWNrZXIuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvbm9kZV9tb2R1bGVzL3Zhc3QtY2xpZW50L2Rpc3QvdXJsaGFuZGxlci5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC91cmxoYW5kbGVycy9mbGFzaC5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC91cmxoYW5kbGVycy94bWxodHRwcmVxdWVzdC5qcyIsIi9Vc2Vycy9kYXZlL2dpdC9kYXNoUEFJRC9ub2RlX21vZHVsZXMvdmFzdC1jbGllbnQvZGlzdC91dGlsLmpzIiwiL1VzZXJzL2RhdmUvZ2l0L2Rhc2hQQUlEL3NyYy9BZE1hbmFnZXIuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvc3JjL2Zha2VfNDA0Yjg0NTEuanMiLCIvVXNlcnMvZGF2ZS9naXQvZGFzaFBBSUQvc3JjL3ZwYWlkLmh0bWw1LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbHdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKX12YXIgZj1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwoZi5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxmLGYuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyohIChDKSBBbmRyZWEgR2lhbW1hcmNoaSAtIEBXZWJSZWZsZWN0aW9uIC0gTWl0IFN0eWxlIExpY2Vuc2UgKi9cbihmdW5jdGlvbihlKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiB0KCl7cmV0dXJuIGMuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpfWZ1bmN0aW9uIG4oZSl7cmV0dXJuIGMuY3JlYXRlRWxlbWVudChlKX1mdW5jdGlvbiByKGUsdCl7aWYoIWUpdGhyb3cgbmV3IEVycm9yKFwiRmFpbGVkIHRvIGNvbnN0cnVjdCBcIit0K1wiOiAxIGFyZ3VtZW50IHJlcXVpcmVkLCBidXQgb25seSAwIHByZXNlbnQuXCIpfWZ1bmN0aW9uIGkoZSl7aWYoZS5sZW5ndGg9PT0xKXJldHVybiBzKGVbMF0pO2Zvcih2YXIgbj10KCkscj1SLmNhbGwoZSksaT0wO2k8ZS5sZW5ndGg7aSsrKW4uYXBwZW5kQ2hpbGQocyhyW2ldKSk7cmV0dXJuIG59ZnVuY3Rpb24gcyhlKXtyZXR1cm4gdHlwZW9mIGU9PVwic3RyaW5nXCI/Yy5jcmVhdGVUZXh0Tm9kZShlKTplfWZvcih2YXIgbyx1LGEsZixsLGM9ZS5kb2N1bWVudCxoPU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkscD1PYmplY3QuZGVmaW5lUHJvcGVydHl8fGZ1bmN0aW9uKGUsdCxuKXtyZXR1cm4gaC5jYWxsKG4sXCJ2YWx1ZVwiKT9lW3RdPW4udmFsdWU6KGguY2FsbChuLFwiZ2V0XCIpJiZlLl9fZGVmaW5lR2V0dGVyX18odCxuLmdldCksaC5jYWxsKG4sXCJzZXRcIikmJmUuX19kZWZpbmVTZXR0ZXJfXyh0LG4uc2V0KSksZX0sZD1bXS5pbmRleE9mfHxmdW5jdGlvbih0KXt2YXIgbj10aGlzLmxlbmd0aDt3aGlsZShuLS0paWYodGhpc1tuXT09PXQpYnJlYWs7cmV0dXJuIG59LHY9ZnVuY3Rpb24oZSl7aWYoIWUpdGhyb3dcIlN5bnRheEVycm9yXCI7aWYody50ZXN0KGUpKXRocm93XCJJbnZhbGlkQ2hhcmFjdGVyRXJyb3JcIjtyZXR1cm4gZX0sbT1mdW5jdGlvbihlKXt2YXIgdD10eXBlb2YgZS5jbGFzc05hbWU9PVwidW5kZWZpbmVkXCIsbj10P2UuZ2V0QXR0cmlidXRlKFwiY2xhc3NcIil8fFwiXCI6ZS5jbGFzc05hbWUscj10fHx0eXBlb2Ygbj09XCJvYmplY3RcIixpPShyP3Q/bjpuLmJhc2VWYWw6bikucmVwbGFjZShiLFwiXCIpO2kubGVuZ3RoJiZxLnB1c2guYXBwbHkodGhpcyxpLnNwbGl0KHcpKSx0aGlzLl9pc1NWRz1yLHRoaXMuXz1lfSxnPXtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gbmV3IG0odGhpcyl9LHNldDpmdW5jdGlvbigpe319LHk9XCJkb200LXRtcC1cIi5jb25jYXQoTWF0aC5yYW5kb20oKSorKG5ldyBEYXRlKSkucmVwbGFjZShcIi5cIixcIi1cIiksYj0vXlxccyt8XFxzKyQvZyx3PS9cXHMrLyxFPVwiIFwiLFM9XCJjbGFzc0xpc3RcIix4PWZ1bmN0aW9uKHQsbil7aWYodGhpcy5jb250YWlucyh0KSlufHx0aGlzLnJlbW92ZSh0KTtlbHNlIGlmKG49PT11bmRlZmluZWR8fG4pbj0hMCx0aGlzLmFkZCh0KTtyZXR1cm4hIW59LFQ9ZS5Eb2N1bWVudEZyYWdtZW50JiZEb2N1bWVudEZyYWdtZW50LnByb3RvdHlwZSxOPWUuTm9kZSxDPShOfHxFbGVtZW50KS5wcm90b3R5cGUsaz1lLkNoYXJhY3RlckRhdGF8fE4sTD1rJiZrLnByb3RvdHlwZSxBPWUuRG9jdW1lbnRUeXBlLE89QSYmQS5wcm90b3R5cGUsTT0oZS5FbGVtZW50fHxOfHxlLkhUTUxFbGVtZW50KS5wcm90b3R5cGUsXz1lLkhUTUxTZWxlY3RFbGVtZW50fHxuKFwic2VsZWN0XCIpLmNvbnN0cnVjdG9yLEQ9Xy5wcm90b3R5cGUucmVtb3ZlLFA9ZS5TaGFkb3dSb290LEg9ZS5TVkdFbGVtZW50LEI9LyAvZyxqPVwiXFxcXCBcIixGPWZ1bmN0aW9uKGUpe3ZhciB0PWU9PT1cInF1ZXJ5U2VsZWN0b3JBbGxcIjtyZXR1cm4gZnVuY3Rpb24obil7dmFyIHIsaSxzLG8sdSxhLGY9dGhpcy5wYXJlbnROb2RlO2lmKGYpe2ZvcihzPXRoaXMuZ2V0QXR0cmlidXRlKFwiaWRcIil8fHksbz1zPT09eT9zOnMucmVwbGFjZShCLGopLGE9bi5zcGxpdChcIixcIiksaT0wO2k8YS5sZW5ndGg7aSsrKWFbaV09XCIjXCIrbytcIiBcIithW2ldO249YS5qb2luKFwiLFwiKX1zPT09eSYmdGhpcy5zZXRBdHRyaWJ1dGUoXCJpZFwiLHMpLHU9KGZ8fHRoaXMpW2VdKG4pLHM9PT15JiZ0aGlzLnJlbW92ZUF0dHJpYnV0ZShcImlkXCIpO2lmKHQpe2k9dS5sZW5ndGgscj1uZXcgQXJyYXkoaSk7d2hpbGUoaS0tKXJbaV09dVtpXX1lbHNlIHI9dTtyZXR1cm4gcn19LEk9ZnVuY3Rpb24oZSl7XCJxdWVyeVwiaW4gZXx8KGUucXVlcnk9TS5xdWVyeSksXCJxdWVyeUFsbFwiaW4gZXx8KGUucXVlcnlBbGw9TS5xdWVyeUFsbCl9LHE9W1wibWF0Y2hlc1wiLE0ubWF0Y2hlc1NlbGVjdG9yfHxNLndlYmtpdE1hdGNoZXNTZWxlY3Rvcnx8TS5raHRtbE1hdGNoZXNTZWxlY3Rvcnx8TS5tb3pNYXRjaGVzU2VsZWN0b3J8fE0ubXNNYXRjaGVzU2VsZWN0b3J8fE0ub01hdGNoZXNTZWxlY3Rvcnx8ZnVuY3Rpb24odCl7dmFyIG49dGhpcy5wYXJlbnROb2RlO3JldHVybiEhbiYmLTE8ZC5jYWxsKG4ucXVlcnlTZWxlY3RvckFsbCh0KSx0aGlzKX0sXCJjbG9zZXN0XCIsZnVuY3Rpb24odCl7dmFyIG49dGhpcyxyO3doaWxlKChyPW4mJm4ubWF0Y2hlcykmJiFuLm1hdGNoZXModCkpbj1uLnBhcmVudE5vZGU7cmV0dXJuIHI/bjpudWxsfSxcInByZXBlbmRcIixmdW5jdGlvbigpe3ZhciB0PXRoaXMuZmlyc3RDaGlsZCxuPWkoYXJndW1lbnRzKTt0P3RoaXMuaW5zZXJ0QmVmb3JlKG4sdCk6dGhpcy5hcHBlbmRDaGlsZChuKX0sXCJhcHBlbmRcIixmdW5jdGlvbigpe3RoaXMuYXBwZW5kQ2hpbGQoaShhcmd1bWVudHMpKX0sXCJiZWZvcmVcIixmdW5jdGlvbigpe3ZhciB0PXRoaXMucGFyZW50Tm9kZTt0JiZ0Lmluc2VydEJlZm9yZShpKGFyZ3VtZW50cyksdGhpcyl9LFwiYWZ0ZXJcIixmdW5jdGlvbigpe3ZhciB0PXRoaXMucGFyZW50Tm9kZSxuPXRoaXMubmV4dFNpYmxpbmcscj1pKGFyZ3VtZW50cyk7dCYmKG4/dC5pbnNlcnRCZWZvcmUocixuKTp0LmFwcGVuZENoaWxkKHIpKX0sXCJyZXBsYWNlXCIsZnVuY3Rpb24oKXt0aGlzLnJlcGxhY2VXaXRoLmFwcGx5KHRoaXMsYXJndW1lbnRzKX0sXCJyZXBsYWNlV2l0aFwiLGZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5wYXJlbnROb2RlO3QmJnQucmVwbGFjZUNoaWxkKGkoYXJndW1lbnRzKSx0aGlzKX0sXCJyZW1vdmVcIixmdW5jdGlvbigpe3ZhciB0PXRoaXMucGFyZW50Tm9kZTt0JiZ0LnJlbW92ZUNoaWxkKHRoaXMpfSxcInF1ZXJ5XCIsRihcInF1ZXJ5U2VsZWN0b3JcIiksXCJxdWVyeUFsbFwiLEYoXCJxdWVyeVNlbGVjdG9yQWxsXCIpXSxSPXEuc2xpY2UsVT1xLmxlbmd0aDtVO1UtPTIpe3U9cVtVLTJdLHUgaW4gTXx8KE1bdV09cVtVLTFdKSx1PT09XCJyZW1vdmVcIiYmKF8ucHJvdG90eXBlW3VdPWZ1bmN0aW9uKCl7cmV0dXJuIDA8YXJndW1lbnRzLmxlbmd0aD9ELmFwcGx5KHRoaXMsYXJndW1lbnRzKTpNLnJlbW92ZS5jYWxsKHRoaXMpfSksL14oPzpiZWZvcmV8YWZ0ZXJ8cmVwbGFjZXxyZXBsYWNlV2l0aHxyZW1vdmUpJC8udGVzdCh1KSYmKGsmJiEodSBpbiBMKSYmKExbdV09cVtVLTFdKSxBJiYhKHUgaW4gTykmJihPW3VdPXFbVS0xXSkpO2lmKC9eKD86YXBwZW5kfHByZXBlbmQpJC8udGVzdCh1KSlpZihUKXUgaW4gVHx8KFRbdV09cVtVLTFdKTtlbHNlIHRyeXt0KCkuY29uc3RydWN0b3IucHJvdG90eXBlW3VdPXFbVS0xXX1jYXRjaCh6KXt9fUkoYyk7aWYoVClJKFQpO2Vsc2UgdHJ5e0kodCgpLmNvbnN0cnVjdG9yLnByb3RvdHlwZSl9Y2F0Y2goeil7fVAmJkkoUC5wcm90b3R5cGUpLG4oXCJhXCIpLm1hdGNoZXMoXCJhXCIpfHwoTVt1XT1mdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24obil7cmV0dXJuIGUuY2FsbCh0aGlzLnBhcmVudE5vZGU/dGhpczp0KCkuYXBwZW5kQ2hpbGQodGhpcyksbil9fShNW3VdKSksbS5wcm90b3R5cGU9e2xlbmd0aDowLGFkZDpmdW5jdGlvbigpe2Zvcih2YXIgdD0wLG47dDxhcmd1bWVudHMubGVuZ3RoO3QrKyluPWFyZ3VtZW50c1t0XSx0aGlzLmNvbnRhaW5zKG4pfHxxLnB1c2guY2FsbCh0aGlzLHUpO3RoaXMuX2lzU1ZHP3RoaXMuXy5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLFwiXCIrdGhpcyk6dGhpcy5fLmNsYXNzTmFtZT1cIlwiK3RoaXN9LGNvbnRhaW5zOmZ1bmN0aW9uKGUpe3JldHVybiBmdW5jdGlvbihuKXtyZXR1cm4gVT1lLmNhbGwodGhpcyx1PXYobikpLC0xPFV9fShbXS5pbmRleE9mfHxmdW5jdGlvbihlKXtVPXRoaXMubGVuZ3RoO3doaWxlKFUtLSYmdGhpc1tVXSE9PWUpO3JldHVybiBVfSksaXRlbTpmdW5jdGlvbih0KXtyZXR1cm4gdGhpc1t0XXx8bnVsbH0scmVtb3ZlOmZ1bmN0aW9uKCl7Zm9yKHZhciB0PTAsbjt0PGFyZ3VtZW50cy5sZW5ndGg7dCsrKW49YXJndW1lbnRzW3RdLHRoaXMuY29udGFpbnMobikmJnEuc3BsaWNlLmNhbGwodGhpcyxVLDEpO3RoaXMuX2lzU1ZHP3RoaXMuXy5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLFwiXCIrdGhpcyk6dGhpcy5fLmNsYXNzTmFtZT1cIlwiK3RoaXN9LHRvZ2dsZTp4LHRvU3RyaW5nOmZ1bmN0aW9uIFcoKXtyZXR1cm4gcS5qb2luLmNhbGwodGhpcyxFKX19LEgmJiEoUyBpbiBILnByb3RvdHlwZSkmJnAoSC5wcm90b3R5cGUsUyxnKSxTIGluIGMuZG9jdW1lbnRFbGVtZW50PyhmPW4oXCJkaXZcIilbU10sZi5hZGQoXCJhXCIsXCJiXCIsXCJhXCIpLFwiYSBiXCIhPWYmJihhPWYuY29uc3RydWN0b3IucHJvdG90eXBlLFwiYWRkXCJpbiBhfHwoYT1lLlRlbXBvcmFyeVRva2VuTGlzdC5wcm90b3R5cGUpLGw9ZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKCl7dmFyIHQ9MDt3aGlsZSh0PGFyZ3VtZW50cy5sZW5ndGgpZS5jYWxsKHRoaXMsYXJndW1lbnRzW3QrK10pfX0sYS5hZGQ9bChhLmFkZCksYS5yZW1vdmU9bChhLnJlbW92ZSksYS50b2dnbGU9eCkpOnAoTSxTLGcpLFwiY29udGFpbnNcImluIEN8fHAoQyxcImNvbnRhaW5zXCIse3ZhbHVlOmZ1bmN0aW9uKGUpe3doaWxlKGUmJmUhPT10aGlzKWU9ZS5wYXJlbnROb2RlO3JldHVybiB0aGlzPT09ZX19KSxcImhlYWRcImluIGN8fHAoYyxcImhlYWRcIix7Z2V0OmZ1bmN0aW9uKCl7cmV0dXJuIG98fChvPWMuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJoZWFkXCIpWzBdKX19KSxmdW5jdGlvbigpe2Zvcih2YXIgdCxuPWUucmVxdWVzdEFuaW1hdGlvbkZyYW1lLHI9ZS5jYW5jZWxBbmltYXRpb25GcmFtZSxpPVtcIm9cIixcIm1zXCIsXCJtb3pcIixcIndlYmtpdFwiXSxzPWkubGVuZ3RoOyFyJiZzLS07KW49bnx8ZVtpW3NdK1wiUmVxdWVzdEFuaW1hdGlvbkZyYW1lXCJdLHI9ZVtpW3NdK1wiQ2FuY2VsQW5pbWF0aW9uRnJhbWVcIl18fGVbaVtzXStcIkNhbmNlbFJlcXVlc3RBbmltYXRpb25GcmFtZVwiXTtyfHwobj8odD1uLG49ZnVuY3Rpb24oZSl7dmFyIG49ITA7cmV0dXJuIHQoZnVuY3Rpb24oKXtuJiZlLmFwcGx5KHRoaXMsYXJndW1lbnRzKX0pLGZ1bmN0aW9uKCl7bj0hMX19LHI9ZnVuY3Rpb24oZSl7ZSgpfSk6KG49ZnVuY3Rpb24oZSl7cmV0dXJuIHNldFRpbWVvdXQoZSwxNSwxNSl9LHI9ZnVuY3Rpb24oZSl7Y2xlYXJUaW1lb3V0KGUpfSkpLGUucmVxdWVzdEFuaW1hdGlvbkZyYW1lPW4sZS5jYW5jZWxBbmltYXRpb25GcmFtZT1yfSgpO3RyeXtuZXcgZS5DdXN0b21FdmVudChcIj9cIil9Y2F0Y2goeil7ZS5DdXN0b21FdmVudD1mdW5jdGlvbihlLHQpe2Z1bmN0aW9uIG4obixpKXt2YXIgcz1jLmNyZWF0ZUV2ZW50KGUpO2lmKHR5cGVvZiBuIT1cInN0cmluZ1wiKXRocm93IG5ldyBFcnJvcihcIkFuIGV2ZW50IG5hbWUgbXVzdCBiZSBwcm92aWRlZFwiKTtyZXR1cm4gZT09XCJFdmVudFwiJiYocy5pbml0Q3VzdG9tRXZlbnQ9ciksaT09bnVsbCYmKGk9dCkscy5pbml0Q3VzdG9tRXZlbnQobixpLmJ1YmJsZXMsaS5jYW5jZWxhYmxlLGkuZGV0YWlsKSxzfWZ1bmN0aW9uIHIoZSx0LG4scil7dGhpcy5pbml0RXZlbnQoZSx0LG4pLHRoaXMuZGV0YWlsPXJ9cmV0dXJuIG59KGUuQ3VzdG9tRXZlbnQ/XCJDdXN0b21FdmVudFwiOlwiRXZlbnRcIix7YnViYmxlczohMSxjYW5jZWxhYmxlOiExLGRldGFpbDpudWxsfSl9dHJ5e25ldyBFdmVudChcIl9cIil9Y2F0Y2goeil7ej1mdW5jdGlvbihlKXtmdW5jdGlvbiB0KGUsdCl7cihhcmd1bWVudHMubGVuZ3RoLFwiRXZlbnRcIik7dmFyIG49Yy5jcmVhdGVFdmVudChcIkV2ZW50XCIpO3JldHVybiB0fHwodD17fSksbi5pbml0RXZlbnQoZSwhIXQuYnViYmxlcywhIXQuY2FuY2VsYWJsZSksbn1yZXR1cm4gdC5wcm90b3R5cGU9ZS5wcm90b3R5cGUsdH0oZS5FdmVudHx8ZnVuY3Rpb24oKXt9KSxwKGUsXCJFdmVudFwiLHt2YWx1ZTp6fSksRXZlbnQhPT16JiYoRXZlbnQ9eil9dHJ5e25ldyBLZXlib2FyZEV2ZW50KFwiX1wiLHt9KX1jYXRjaCh6KXt6PWZ1bmN0aW9uKHQpe2Z1bmN0aW9uIGEoZSl7Zm9yKHZhciB0PVtdLG49W1wiY3RybEtleVwiLFwiQ29udHJvbFwiLFwic2hpZnRLZXlcIixcIlNoaWZ0XCIsXCJhbHRLZXlcIixcIkFsdFwiLFwibWV0YUtleVwiLFwiTWV0YVwiLFwiYWx0R3JhcGhLZXlcIixcIkFsdEdyYXBoXCJdLHI9MDtyPG4ubGVuZ3RoO3IrPTIpZVtuW3JdXSYmdC5wdXNoKG5bcisxXSk7cmV0dXJuIHQuam9pbihcIiBcIil9ZnVuY3Rpb24gZihlLHQpe2Zvcih2YXIgbiBpbiB0KXQuaGFzT3duUHJvcGVydHkobikmJiF0Lmhhc093blByb3BlcnR5LmNhbGwoZSxuKSYmKGVbbl09dFtuXSk7cmV0dXJuIGV9ZnVuY3Rpb24gbChlLHQsbil7dHJ5e3RbZV09bltlXX1jYXRjaChyKXt9fWZ1bmN0aW9uIGgodCxvKXtyKGFyZ3VtZW50cy5sZW5ndGgsXCJLZXlib2FyZEV2ZW50XCIpLG89ZihvfHx7fSxpKTt2YXIgdT1jLmNyZWF0ZUV2ZW50KHMpLGg9by5jdHJsS2V5LHA9by5zaGlmdEtleSxkPW8uYWx0S2V5LHY9by5tZXRhS2V5LG09by5hbHRHcmFwaEtleSxnPW4+Mz9hKG8pOm51bGwseT1TdHJpbmcoby5rZXkpLGI9U3RyaW5nKG8uY2hhciksdz1vLmxvY2F0aW9uLEU9by5rZXlDb2RlfHwoby5rZXlDb2RlPXkpJiZ5LmNoYXJDb2RlQXQoMCl8fDAsUz1vLmNoYXJDb2RlfHwoby5jaGFyQ29kZT1iKSYmYi5jaGFyQ29kZUF0KDApfHwwLHg9by5idWJibGVzLFQ9by5jYW5jZWxhYmxlLE49by5yZXBlYXQsQz1vLmxvY2FsZSxrPW8udmlld3x8ZSxMO28ud2hpY2h8fChvLndoaWNoPW8ua2V5Q29kZSk7aWYoXCJpbml0S2V5RXZlbnRcImluIHUpdS5pbml0S2V5RXZlbnQodCx4LFQsayxoLGQscCx2LEUsUyk7ZWxzZSBpZigwPG4mJlwiaW5pdEtleWJvYXJkRXZlbnRcImluIHUpe0w9W3QseCxULGtdO3N3aXRjaChuKXtjYXNlIDE6TC5wdXNoKHksdyxoLHAsZCx2LG0pO2JyZWFrO2Nhc2UgMjpMLnB1c2goaCxkLHAsdixFLFMpO2JyZWFrO2Nhc2UgMzpMLnB1c2goeSx3LGgsZCxwLHYsbSk7YnJlYWs7Y2FzZSA0OkwucHVzaCh5LHcsZyxOLEMpO2JyZWFrO2RlZmF1bHQ6TC5wdXNoKGNoYXIseSx3LGcsTixDKX11LmluaXRLZXlib2FyZEV2ZW50LmFwcGx5KHUsTCl9ZWxzZSB1LmluaXRFdmVudCh0LHgsVCk7Zm9yKHkgaW4gdSlpLmhhc093blByb3BlcnR5KHkpJiZ1W3ldIT09b1t5XSYmbCh5LHUsbyk7cmV0dXJuIHV9dmFyIG49MCxpPXtcImNoYXJcIjpcIlwiLGtleTpcIlwiLGxvY2F0aW9uOjAsY3RybEtleTohMSxzaGlmdEtleTohMSxhbHRLZXk6ITEsbWV0YUtleTohMSxhbHRHcmFwaEtleTohMSxyZXBlYXQ6ITEsbG9jYWxlOm5hdmlnYXRvci5sYW5ndWFnZSxkZXRhaWw6MCxidWJibGVzOiExLGNhbmNlbGFibGU6ITEsa2V5Q29kZTowLGNoYXJDb2RlOjAsd2hpY2g6MH0sczt0cnl7dmFyIG89Yy5jcmVhdGVFdmVudChcIktleWJvYXJkRXZlbnRcIik7by5pbml0S2V5Ym9hcmRFdmVudChcImtleXVwXCIsITEsITEsZSxcIitcIiwzLCEwLCExLCEwLCExLCExKSxuPShvLmtleUlkZW50aWZpZXJ8fG8ua2V5KT09XCIrXCImJihvLmtleUxvY2F0aW9ufHxvLmxvY2F0aW9uKT09MyYmKG8uY3RybEtleT9vLmFsdEtleT8xOjM6by5zaGlmdEtleT8yOjQpfHw5fWNhdGNoKHUpe31yZXR1cm4gcz0wPG4/XCJLZXlib2FyZEV2ZW50XCI6XCJFdmVudFwiLGgucHJvdG90eXBlPXQucHJvdG90eXBlLGh9KGUuS2V5Ym9hcmRFdmVudHx8ZnVuY3Rpb24oKXt9KSxwKGUsXCJLZXlib2FyZEV2ZW50XCIse3ZhbHVlOnp9KSxLZXlib2FyZEV2ZW50IT09eiYmKEtleWJvYXJkRXZlbnQ9eil9dHJ5e25ldyBNb3VzZUV2ZW50KFwiX1wiLHt9KX1jYXRjaCh6KXt6PWZ1bmN0aW9uKHQpe2Z1bmN0aW9uIG4odCxuKXtyKGFyZ3VtZW50cy5sZW5ndGgsXCJNb3VzZUV2ZW50XCIpO3ZhciBpPWMuY3JlYXRlRXZlbnQoXCJNb3VzZUV2ZW50XCIpO3JldHVybiBufHwobj17fSksaS5pbml0TW91c2VFdmVudCh0LCEhbi5idWJibGVzLCEhbi5jYW5jZWxhYmxlLG4udmlld3x8ZSxuLmRldGFpbHx8MSxuLnNjcmVlblh8fDAsbi5zY3JlZW5ZfHwwLG4uY2xpZW50WHx8MCxuLmNsaWVudFl8fDAsISFuLmN0cmxLZXksISFuLmFsdEtleSwhIW4uc2hpZnRLZXksISFuLm1ldGFLZXksbi5idXR0b258fDAsbi5yZWxhdGVkVGFyZ2V0fHxudWxsKSxpfXJldHVybiBuLnByb3RvdHlwZT10LnByb3RvdHlwZSxufShlLk1vdXNlRXZlbnR8fGZ1bmN0aW9uKCl7fSkscChlLFwiTW91c2VFdmVudFwiLHt2YWx1ZTp6fSksTW91c2VFdmVudCE9PXomJihNb3VzZUV2ZW50PXopfX0pKHdpbmRvdyksZnVuY3Rpb24oZSl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbigpe31mdW5jdGlvbiByKGUsdCxuKXtmdW5jdGlvbiBpKGUpe2kub25jZSYmKGUuY3VycmVudFRhcmdldC5yZW1vdmVFdmVudExpc3RlbmVyKGUudHlwZSx0LGkpLGkucmVtb3ZlZD0hMCksaS5wYXNzaXZlJiYoZS5wcmV2ZW50RGVmYXVsdD1yLnByZXZlbnREZWZhdWx0KSx0eXBlb2YgaS5jYWxsYmFjaz09XCJmdW5jdGlvblwiP2kuY2FsbGJhY2suY2FsbCh0aGlzLGUpOmkuY2FsbGJhY2smJmkuY2FsbGJhY2suaGFuZGxlRXZlbnQoZSksaS5wYXNzaXZlJiZkZWxldGUgZS5wcmV2ZW50RGVmYXVsdH1yZXR1cm4gaS50eXBlPWUsaS5jYWxsYmFjaz10LGkuY2FwdHVyZT0hIW4uY2FwdHVyZSxpLnBhc3NpdmU9ISFuLnBhc3NpdmUsaS5vbmNlPSEhbi5vbmNlLGkucmVtb3ZlZD0hMSxpfXZhciB0PWUuV2Vha01hcHx8ZnVuY3Rpb24oKXtmdW5jdGlvbiBzKGUsaSxzKXtuPXMsdD0hMSxyPXVuZGVmaW5lZCxlLmRpc3BhdGNoRXZlbnQoaSl9ZnVuY3Rpb24gbyhlKXt0aGlzLnZhbHVlPWV9ZnVuY3Rpb24gdSgpe2UrKyx0aGlzLl9fY2VfXz1uZXcgaShcIkBET01NYXA6XCIrZStNYXRoLnJhbmRvbSgpKX12YXIgZT0wLHQ9ITEsbj0hMSxyO3JldHVybiBvLnByb3RvdHlwZS5oYW5kbGVFdmVudD1mdW5jdGlvbihpKXt0PSEwLG4/aS5jdXJyZW50VGFyZ2V0LnJlbW92ZUV2ZW50TGlzdGVuZXIoaS50eXBlLHRoaXMsITEpOnI9dGhpcy52YWx1ZX0sdS5wcm90b3R5cGU9e2NvbnN0cnVjdG9yOnUsXCJkZWxldGVcIjpmdW5jdGlvbihuKXtyZXR1cm4gcyhuLHRoaXMuX19jZV9fLCEwKSx0fSxnZXQ6ZnVuY3Rpb24odCl7cyh0LHRoaXMuX19jZV9fLCExKTt2YXIgbj1yO3JldHVybiByPXVuZGVmaW5lZCxufSxoYXM6ZnVuY3Rpb24obil7cmV0dXJuIHMobix0aGlzLl9fY2VfXywhMSksdH0sc2V0OmZ1bmN0aW9uKHQsbil7cmV0dXJuIHModCx0aGlzLl9fY2VfXywhMCksdC5hZGRFdmVudExpc3RlbmVyKHRoaXMuX19jZV9fLnR5cGUsbmV3IG8obiksITEpLHRoaXN9fSx1fSgpO24ucHJvdG90eXBlPShPYmplY3QuY3JlYXRlfHxPYmplY3QpKG51bGwpLHIucHJldmVudERlZmF1bHQ9ZnVuY3Rpb24oKXt9O3ZhciBpPWUuQ3VzdG9tRXZlbnQscz1PYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LG89ZS5kaXNwYXRjaEV2ZW50LHU9ZS5hZGRFdmVudExpc3RlbmVyLGE9ZS5yZW1vdmVFdmVudExpc3RlbmVyLGY9MCxsPWZ1bmN0aW9uKCl7ZisrfSxjPVtdLmluZGV4T2Z8fGZ1bmN0aW9uKHQpe3ZhciBuPXRoaXMubGVuZ3RoO3doaWxlKG4tLSlpZih0aGlzW25dPT09dClicmVhaztyZXR1cm4gbn0saD1mdW5jdGlvbihlKXtyZXR1cm5cIlwiLmNvbmNhdChlLmNhcHR1cmU/XCIxXCI6XCIwXCIsZS5wYXNzaXZlP1wiMVwiOlwiMFwiLGUub25jZT9cIjFcIjpcIjBcIil9LHAsZDt0cnl7dShcIl9cIixsLHtvbmNlOiEwfSksbyhuZXcgaShcIl9cIikpLG8obmV3IGkoXCJfXCIpKSxhKFwiX1wiLGwse29uY2U6ITB9KX1jYXRjaCh2KXt9ZiE9PTEmJmZ1bmN0aW9uKCl7ZnVuY3Rpb24gcyhlKXtyZXR1cm4gZnVuY3Rpb24ocyxvLHUpe2lmKHUmJnR5cGVvZiB1IT1cImJvb2xlYW5cIil7dmFyIGE9aS5nZXQodGhpcyksZj1oKHUpLGwscCxkO2F8fGkuc2V0KHRoaXMsYT1uZXcgbikscyBpbiBhfHwoYVtzXT17aGFuZGxlcjpbXSx3cmFwOltdfSkscD1hW3NdLGw9Yy5jYWxsKHAuaGFuZGxlcixvKSxsPDA/KGw9cC5oYW5kbGVyLnB1c2gobyktMSxwLndyYXBbbF09ZD1uZXcgbik6ZD1wLndyYXBbbF0sZiBpbiBkfHwoZFtmXT1yKHMsbyx1KSxlLmNhbGwodGhpcyxzLGRbZl0sZFtmXS5jYXB0dXJlKSl9ZWxzZSBlLmNhbGwodGhpcyxzLG8sdSl9fWZ1bmN0aW9uIG8oZSl7cmV0dXJuIGZ1bmN0aW9uKG4scixzKXtpZihzJiZ0eXBlb2YgcyE9XCJib29sZWFuXCIpe3ZhciBvPWkuZ2V0KHRoaXMpLHUsYSxmLGw7aWYobyYmbiBpbiBvKXtmPW9bbl0sYT1jLmNhbGwoZi5oYW5kbGVyLHIpO2lmKC0xPGEpe3U9aChzKSxsPWYud3JhcFthXTtpZih1IGluIGwpe2UuY2FsbCh0aGlzLG4sbFt1XSxsW3VdLmNhcHR1cmUpLGRlbGV0ZSBsW3VdO2Zvcih1IGluIGwpcmV0dXJuO2YuaGFuZGxlci5zcGxpY2UoYSwxKSxmLndyYXAuc3BsaWNlKGEsMSksZi5oYW5kbGVyLmxlbmd0aD09PTAmJmRlbGV0ZSBvW25dfX19fWVsc2UgZS5jYWxsKHRoaXMsbixyLHMpfX12YXIgaT1uZXcgdDtwPWZ1bmN0aW9uKGUpe2lmKCFlKXJldHVybjt2YXIgdD1lLnByb3RvdHlwZTt0LmFkZEV2ZW50TGlzdGVuZXI9cyh0LmFkZEV2ZW50TGlzdGVuZXIpLHQucmVtb3ZlRXZlbnRMaXN0ZW5lcj1vKHQucmVtb3ZlRXZlbnRMaXN0ZW5lcil9LGUuRXZlbnRUYXJnZXQ/cChFdmVudFRhcmdldCk6KHAoZS5UZXh0KSxwKGUuRWxlbWVudHx8ZS5IVE1MRWxlbWVudCkscChlLkhUTUxEb2N1bWVudCkscChlLldpbmRvd3x8e3Byb3RvdHlwZTplfSkscChlLlhNTEh0dHBSZXF1ZXN0KSl9KCl9KHNlbGYpOyIsIi8vIENvcHlyaWdodCBKb3llbnQsIEluYy4gYW5kIG90aGVyIE5vZGUgY29udHJpYnV0b3JzLlxuLy9cbi8vIFBlcm1pc3Npb24gaXMgaGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBhXG4vLyBjb3B5IG9mIHRoaXMgc29mdHdhcmUgYW5kIGFzc29jaWF0ZWQgZG9jdW1lbnRhdGlvbiBmaWxlcyAodGhlXG4vLyBcIlNvZnR3YXJlXCIpLCB0byBkZWFsIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmdcbi8vIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzIHRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCxcbi8vIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsIGNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXRcbi8vIHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXMgZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZVxuLy8gZm9sbG93aW5nIGNvbmRpdGlvbnM6XG4vL1xuLy8gVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWRcbi8vIGluIGFsbCBjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxuLy9cbi8vIFRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCBcIkFTIElTXCIsIFdJVEhPVVQgV0FSUkFOVFkgT0YgQU5ZIEtJTkQsIEVYUFJFU1Ncbi8vIE9SIElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0Zcbi8vIE1FUkNIQU5UQUJJTElUWSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU5cbi8vIE5PIEVWRU5UIFNIQUxMIFRIRSBBVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZIENMQUlNLFxuLy8gREFNQUdFUyBPUiBPVEhFUiBMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SXG4vLyBPVEhFUldJU0UsIEFSSVNJTkcgRlJPTSwgT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFXG4vLyBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFIFNPRlRXQVJFLlxuXG5mdW5jdGlvbiBFdmVudEVtaXR0ZXIoKSB7XG4gIHRoaXMuX2V2ZW50cyA9IHRoaXMuX2V2ZW50cyB8fCB7fTtcbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gdGhpcy5fbWF4TGlzdGVuZXJzIHx8IHVuZGVmaW5lZDtcbn1cbm1vZHVsZS5leHBvcnRzID0gRXZlbnRFbWl0dGVyO1xuXG4vLyBCYWNrd2FyZHMtY29tcGF0IHdpdGggbm9kZSAwLjEwLnhcbkV2ZW50RW1pdHRlci5FdmVudEVtaXR0ZXIgPSBFdmVudEVtaXR0ZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX2V2ZW50cyA9IHVuZGVmaW5lZDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX21heExpc3RlbmVycyA9IHVuZGVmaW5lZDtcblxuLy8gQnkgZGVmYXVsdCBFdmVudEVtaXR0ZXJzIHdpbGwgcHJpbnQgYSB3YXJuaW5nIGlmIG1vcmUgdGhhbiAxMCBsaXN0ZW5lcnMgYXJlXG4vLyBhZGRlZCB0byBpdC4gVGhpcyBpcyBhIHVzZWZ1bCBkZWZhdWx0IHdoaWNoIGhlbHBzIGZpbmRpbmcgbWVtb3J5IGxlYWtzLlxuRXZlbnRFbWl0dGVyLmRlZmF1bHRNYXhMaXN0ZW5lcnMgPSAxMDtcblxuLy8gT2J2aW91c2x5IG5vdCBhbGwgRW1pdHRlcnMgc2hvdWxkIGJlIGxpbWl0ZWQgdG8gMTAuIFRoaXMgZnVuY3Rpb24gYWxsb3dzXG4vLyB0aGF0IHRvIGJlIGluY3JlYXNlZC4gU2V0IHRvIHplcm8gZm9yIHVubGltaXRlZC5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuc2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24obikge1xuICBpZiAoIWlzTnVtYmVyKG4pIHx8IG4gPCAwIHx8IGlzTmFOKG4pKVxuICAgIHRocm93IFR5cGVFcnJvcignbiBtdXN0IGJlIGEgcG9zaXRpdmUgbnVtYmVyJyk7XG4gIHRoaXMuX21heExpc3RlbmVycyA9IG47XG4gIHJldHVybiB0aGlzO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24odHlwZSkge1xuICB2YXIgZXIsIGhhbmRsZXIsIGxlbiwgYXJncywgaSwgbGlzdGVuZXJzO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzKVxuICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuXG4gIC8vIElmIHRoZXJlIGlzIG5vICdlcnJvcicgZXZlbnQgbGlzdGVuZXIgdGhlbiB0aHJvdy5cbiAgaWYgKHR5cGUgPT09ICdlcnJvcicpIHtcbiAgICBpZiAoIXRoaXMuX2V2ZW50cy5lcnJvciB8fFxuICAgICAgICAoaXNPYmplY3QodGhpcy5fZXZlbnRzLmVycm9yKSAmJiAhdGhpcy5fZXZlbnRzLmVycm9yLmxlbmd0aCkpIHtcbiAgICAgIGVyID0gYXJndW1lbnRzWzFdO1xuICAgICAgaWYgKGVyIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgICAgdGhyb3cgZXI7IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gICAgICB9XG4gICAgICB0aHJvdyBUeXBlRXJyb3IoJ1VuY2F1Z2h0LCB1bnNwZWNpZmllZCBcImVycm9yXCIgZXZlbnQuJyk7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlciA9IHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICBpZiAoaXNVbmRlZmluZWQoaGFuZGxlcikpXG4gICAgcmV0dXJuIGZhbHNlO1xuXG4gIGlmIChpc0Z1bmN0aW9uKGhhbmRsZXIpKSB7XG4gICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAvLyBmYXN0IGNhc2VzXG4gICAgICBjYXNlIDE6XG4gICAgICAgIGhhbmRsZXIuY2FsbCh0aGlzKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIDI6XG4gICAgICAgIGhhbmRsZXIuY2FsbCh0aGlzLCBhcmd1bWVudHNbMV0pO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMzpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMsIGFyZ3VtZW50c1sxXSwgYXJndW1lbnRzWzJdKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAvLyBzbG93ZXJcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGxlbiA9IGFyZ3VtZW50cy5sZW5ndGg7XG4gICAgICAgIGFyZ3MgPSBuZXcgQXJyYXkobGVuIC0gMSk7XG4gICAgICAgIGZvciAoaSA9IDE7IGkgPCBsZW47IGkrKylcbiAgICAgICAgICBhcmdzW2kgLSAxXSA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgaGFuZGxlci5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9XG4gIH0gZWxzZSBpZiAoaXNPYmplY3QoaGFuZGxlcikpIHtcbiAgICBsZW4gPSBhcmd1bWVudHMubGVuZ3RoO1xuICAgIGFyZ3MgPSBuZXcgQXJyYXkobGVuIC0gMSk7XG4gICAgZm9yIChpID0gMTsgaSA8IGxlbjsgaSsrKVxuICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG5cbiAgICBsaXN0ZW5lcnMgPSBoYW5kbGVyLnNsaWNlKCk7XG4gICAgbGVuID0gbGlzdGVuZXJzLmxlbmd0aDtcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspXG4gICAgICBsaXN0ZW5lcnNbaV0uYXBwbHkodGhpcywgYXJncyk7XG4gIH1cblxuICByZXR1cm4gdHJ1ZTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXIgPSBmdW5jdGlvbih0eXBlLCBsaXN0ZW5lcikge1xuICB2YXIgbTtcblxuICBpZiAoIWlzRnVuY3Rpb24obGlzdGVuZXIpKVxuICAgIHRocm93IFR5cGVFcnJvcignbGlzdGVuZXIgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpXG4gICAgdGhpcy5fZXZlbnRzID0ge307XG5cbiAgLy8gVG8gYXZvaWQgcmVjdXJzaW9uIGluIHRoZSBjYXNlIHRoYXQgdHlwZSA9PT0gXCJuZXdMaXN0ZW5lclwiISBCZWZvcmVcbiAgLy8gYWRkaW5nIGl0IHRvIHRoZSBsaXN0ZW5lcnMsIGZpcnN0IGVtaXQgXCJuZXdMaXN0ZW5lclwiLlxuICBpZiAodGhpcy5fZXZlbnRzLm5ld0xpc3RlbmVyKVxuICAgIHRoaXMuZW1pdCgnbmV3TGlzdGVuZXInLCB0eXBlLFxuICAgICAgICAgICAgICBpc0Z1bmN0aW9uKGxpc3RlbmVyLmxpc3RlbmVyKSA/XG4gICAgICAgICAgICAgIGxpc3RlbmVyLmxpc3RlbmVyIDogbGlzdGVuZXIpO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzW3R5cGVdKVxuICAgIC8vIE9wdGltaXplIHRoZSBjYXNlIG9mIG9uZSBsaXN0ZW5lci4gRG9uJ3QgbmVlZCB0aGUgZXh0cmEgYXJyYXkgb2JqZWN0LlxuICAgIHRoaXMuX2V2ZW50c1t0eXBlXSA9IGxpc3RlbmVyO1xuICBlbHNlIGlmIChpc09iamVjdCh0aGlzLl9ldmVudHNbdHlwZV0pKVxuICAgIC8vIElmIHdlJ3ZlIGFscmVhZHkgZ290IGFuIGFycmF5LCBqdXN0IGFwcGVuZC5cbiAgICB0aGlzLl9ldmVudHNbdHlwZV0ucHVzaChsaXN0ZW5lcik7XG4gIGVsc2VcbiAgICAvLyBBZGRpbmcgdGhlIHNlY29uZCBlbGVtZW50LCBuZWVkIHRvIGNoYW5nZSB0byBhcnJheS5cbiAgICB0aGlzLl9ldmVudHNbdHlwZV0gPSBbdGhpcy5fZXZlbnRzW3R5cGVdLCBsaXN0ZW5lcl07XG5cbiAgLy8gQ2hlY2sgZm9yIGxpc3RlbmVyIGxlYWtcbiAgaWYgKGlzT2JqZWN0KHRoaXMuX2V2ZW50c1t0eXBlXSkgJiYgIXRoaXMuX2V2ZW50c1t0eXBlXS53YXJuZWQpIHtcbiAgICB2YXIgbTtcbiAgICBpZiAoIWlzVW5kZWZpbmVkKHRoaXMuX21heExpc3RlbmVycykpIHtcbiAgICAgIG0gPSB0aGlzLl9tYXhMaXN0ZW5lcnM7XG4gICAgfSBlbHNlIHtcbiAgICAgIG0gPSBFdmVudEVtaXR0ZXIuZGVmYXVsdE1heExpc3RlbmVycztcbiAgICB9XG5cbiAgICBpZiAobSAmJiBtID4gMCAmJiB0aGlzLl9ldmVudHNbdHlwZV0ubGVuZ3RoID4gbSkge1xuICAgICAgdGhpcy5fZXZlbnRzW3R5cGVdLndhcm5lZCA9IHRydWU7XG4gICAgICBjb25zb2xlLmVycm9yKCcobm9kZSkgd2FybmluZzogcG9zc2libGUgRXZlbnRFbWl0dGVyIG1lbW9yeSAnICtcbiAgICAgICAgICAgICAgICAgICAgJ2xlYWsgZGV0ZWN0ZWQuICVkIGxpc3RlbmVycyBhZGRlZC4gJyArXG4gICAgICAgICAgICAgICAgICAgICdVc2UgZW1pdHRlci5zZXRNYXhMaXN0ZW5lcnMoKSB0byBpbmNyZWFzZSBsaW1pdC4nLFxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9ldmVudHNbdHlwZV0ubGVuZ3RoKTtcbiAgICAgIGlmICh0eXBlb2YgY29uc29sZS50cmFjZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAvLyBub3Qgc3VwcG9ydGVkIGluIElFIDEwXG4gICAgICAgIGNvbnNvbGUudHJhY2UoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub24gPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uY2UgPSBmdW5jdGlvbih0eXBlLCBsaXN0ZW5lcikge1xuICBpZiAoIWlzRnVuY3Rpb24obGlzdGVuZXIpKVxuICAgIHRocm93IFR5cGVFcnJvcignbGlzdGVuZXIgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG5cbiAgdmFyIGZpcmVkID0gZmFsc2U7XG5cbiAgZnVuY3Rpb24gZygpIHtcbiAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGcpO1xuXG4gICAgaWYgKCFmaXJlZCkge1xuICAgICAgZmlyZWQgPSB0cnVlO1xuICAgICAgbGlzdGVuZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICB9XG4gIH1cblxuICBnLmxpc3RlbmVyID0gbGlzdGVuZXI7XG4gIHRoaXMub24odHlwZSwgZyk7XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG4vLyBlbWl0cyBhICdyZW1vdmVMaXN0ZW5lcicgZXZlbnQgaWZmIHRoZSBsaXN0ZW5lciB3YXMgcmVtb3ZlZFxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9IGZ1bmN0aW9uKHR5cGUsIGxpc3RlbmVyKSB7XG4gIHZhciBsaXN0LCBwb3NpdGlvbiwgbGVuZ3RoLCBpO1xuXG4gIGlmICghaXNGdW5jdGlvbihsaXN0ZW5lcikpXG4gICAgdGhyb3cgVHlwZUVycm9yKCdsaXN0ZW5lciBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcblxuICBpZiAoIXRoaXMuX2V2ZW50cyB8fCAhdGhpcy5fZXZlbnRzW3R5cGVdKVxuICAgIHJldHVybiB0aGlzO1xuXG4gIGxpc3QgPSB0aGlzLl9ldmVudHNbdHlwZV07XG4gIGxlbmd0aCA9IGxpc3QubGVuZ3RoO1xuICBwb3NpdGlvbiA9IC0xO1xuXG4gIGlmIChsaXN0ID09PSBsaXN0ZW5lciB8fFxuICAgICAgKGlzRnVuY3Rpb24obGlzdC5saXN0ZW5lcikgJiYgbGlzdC5saXN0ZW5lciA9PT0gbGlzdGVuZXIpKSB7XG4gICAgZGVsZXRlIHRoaXMuX2V2ZW50c1t0eXBlXTtcbiAgICBpZiAodGhpcy5fZXZlbnRzLnJlbW92ZUxpc3RlbmVyKVxuICAgICAgdGhpcy5lbWl0KCdyZW1vdmVMaXN0ZW5lcicsIHR5cGUsIGxpc3RlbmVyKTtcblxuICB9IGVsc2UgaWYgKGlzT2JqZWN0KGxpc3QpKSB7XG4gICAgZm9yIChpID0gbGVuZ3RoOyBpLS0gPiAwOykge1xuICAgICAgaWYgKGxpc3RbaV0gPT09IGxpc3RlbmVyIHx8XG4gICAgICAgICAgKGxpc3RbaV0ubGlzdGVuZXIgJiYgbGlzdFtpXS5saXN0ZW5lciA9PT0gbGlzdGVuZXIpKSB7XG4gICAgICAgIHBvc2l0aW9uID0gaTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHBvc2l0aW9uIDwgMClcbiAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgaWYgKGxpc3QubGVuZ3RoID09PSAxKSB7XG4gICAgICBsaXN0Lmxlbmd0aCA9IDA7XG4gICAgICBkZWxldGUgdGhpcy5fZXZlbnRzW3R5cGVdO1xuICAgIH0gZWxzZSB7XG4gICAgICBsaXN0LnNwbGljZShwb3NpdGlvbiwgMSk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2V2ZW50cy5yZW1vdmVMaXN0ZW5lcilcbiAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBsaXN0ZW5lcik7XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID0gZnVuY3Rpb24odHlwZSkge1xuICB2YXIga2V5LCBsaXN0ZW5lcnM7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpXG4gICAgcmV0dXJuIHRoaXM7XG5cbiAgLy8gbm90IGxpc3RlbmluZyBmb3IgcmVtb3ZlTGlzdGVuZXIsIG5vIG5lZWQgdG8gZW1pdFxuICBpZiAoIXRoaXMuX2V2ZW50cy5yZW1vdmVMaXN0ZW5lcikge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKVxuICAgICAgdGhpcy5fZXZlbnRzID0ge307XG4gICAgZWxzZSBpZiAodGhpcy5fZXZlbnRzW3R5cGVdKVxuICAgICAgZGVsZXRlIHRoaXMuX2V2ZW50c1t0eXBlXTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8vIGVtaXQgcmVtb3ZlTGlzdGVuZXIgZm9yIGFsbCBsaXN0ZW5lcnMgb24gYWxsIGV2ZW50c1xuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgIGZvciAoa2V5IGluIHRoaXMuX2V2ZW50cykge1xuICAgICAgaWYgKGtleSA9PT0gJ3JlbW92ZUxpc3RlbmVyJykgY29udGludWU7XG4gICAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycyhrZXkpO1xuICAgIH1cbiAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygncmVtb3ZlTGlzdGVuZXInKTtcbiAgICB0aGlzLl9ldmVudHMgPSB7fTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIGxpc3RlbmVycyA9IHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICBpZiAoaXNGdW5jdGlvbihsaXN0ZW5lcnMpKSB7XG4gICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcnMpO1xuICB9IGVsc2Uge1xuICAgIC8vIExJRk8gb3JkZXJcbiAgICB3aGlsZSAobGlzdGVuZXJzLmxlbmd0aClcbiAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzW2xpc3RlbmVycy5sZW5ndGggLSAxXSk7XG4gIH1cbiAgZGVsZXRlIHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24odHlwZSkge1xuICB2YXIgcmV0O1xuICBpZiAoIXRoaXMuX2V2ZW50cyB8fCAhdGhpcy5fZXZlbnRzW3R5cGVdKVxuICAgIHJldCA9IFtdO1xuICBlbHNlIGlmIChpc0Z1bmN0aW9uKHRoaXMuX2V2ZW50c1t0eXBlXSkpXG4gICAgcmV0ID0gW3RoaXMuX2V2ZW50c1t0eXBlXV07XG4gIGVsc2VcbiAgICByZXQgPSB0aGlzLl9ldmVudHNbdHlwZV0uc2xpY2UoKTtcbiAgcmV0dXJuIHJldDtcbn07XG5cbkV2ZW50RW1pdHRlci5saXN0ZW5lckNvdW50ID0gZnVuY3Rpb24oZW1pdHRlciwgdHlwZSkge1xuICB2YXIgcmV0O1xuICBpZiAoIWVtaXR0ZXIuX2V2ZW50cyB8fCAhZW1pdHRlci5fZXZlbnRzW3R5cGVdKVxuICAgIHJldCA9IDA7XG4gIGVsc2UgaWYgKGlzRnVuY3Rpb24oZW1pdHRlci5fZXZlbnRzW3R5cGVdKSlcbiAgICByZXQgPSAxO1xuICBlbHNlXG4gICAgcmV0ID0gZW1pdHRlci5fZXZlbnRzW3R5cGVdLmxlbmd0aDtcbiAgcmV0dXJuIHJldDtcbn07XG5cbmZ1bmN0aW9uIGlzRnVuY3Rpb24oYXJnKSB7XG4gIHJldHVybiB0eXBlb2YgYXJnID09PSAnZnVuY3Rpb24nO1xufVxuXG5mdW5jdGlvbiBpc051bWJlcihhcmcpIHtcbiAgcmV0dXJuIHR5cGVvZiBhcmcgPT09ICdudW1iZXInO1xufVxuXG5mdW5jdGlvbiBpc09iamVjdChhcmcpIHtcbiAgcmV0dXJuIHR5cGVvZiBhcmcgPT09ICdvYmplY3QnICYmIGFyZyAhPT0gbnVsbDtcbn1cblxuZnVuY3Rpb24gaXNVbmRlZmluZWQoYXJnKSB7XG4gIHJldHVybiBhcmcgPT09IHZvaWQgMDtcbn1cbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBWQVNUQWQ7XG5cbiAgVkFTVEFkID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFZBU1RBZCgpIHtcbiAgICAgIHRoaXMuaWQgPSBudWxsO1xuICAgICAgdGhpcy5zZXF1ZW5jZSA9IG51bGw7XG4gICAgICB0aGlzLnN5c3RlbSA9IG51bGw7XG4gICAgICB0aGlzLnRpdGxlID0gbnVsbDtcbiAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBudWxsO1xuICAgICAgdGhpcy5hZHZlcnRpc2VyID0gbnVsbDtcbiAgICAgIHRoaXMucHJpY2luZyA9IG51bGw7XG4gICAgICB0aGlzLnN1cnZleSA9IG51bGw7XG4gICAgICB0aGlzLmVycm9yVVJMVGVtcGxhdGVzID0gW107XG4gICAgICB0aGlzLmltcHJlc3Npb25VUkxUZW1wbGF0ZXMgPSBbXTtcbiAgICAgIHRoaXMuY3JlYXRpdmVzID0gW107XG4gICAgICB0aGlzLmV4dGVuc2lvbnMgPSBbXTtcbiAgICB9XG5cbiAgICByZXR1cm4gVkFTVEFkO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUQWQ7XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuMTEuMVxuKGZ1bmN0aW9uKCkge1xuICB2YXIgVkFTVENsaWVudCwgVkFTVFBhcnNlciwgVkFTVFV0aWw7XG5cbiAgVkFTVFBhcnNlciA9IHJlcXVpcmUoJy4vcGFyc2VyJyk7XG5cbiAgVkFTVFV0aWwgPSByZXF1aXJlKCcuL3V0aWwnKTtcblxuICBWQVNUQ2xpZW50ID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFZBU1RDbGllbnQoKSB7fVxuXG4gICAgVkFTVENsaWVudC5jYXBwaW5nRnJlZUx1bmNoID0gMDtcblxuICAgIFZBU1RDbGllbnQuY2FwcGluZ01pbmltdW1UaW1lSW50ZXJ2YWwgPSAwO1xuXG4gICAgVkFTVENsaWVudC5vcHRpb25zID0ge1xuICAgICAgd2l0aENyZWRlbnRpYWxzOiBmYWxzZSxcbiAgICAgIHRpbWVvdXQ6IDBcbiAgICB9O1xuXG4gICAgVkFTVENsaWVudC5nZXQgPSBmdW5jdGlvbih1cmwsIG9wdHMsIGNiKSB7XG4gICAgICB2YXIgZXh0ZW5kLCBub3csIG9wdGlvbnMsIHRpbWVTaW5jZUxhc3RDYWxsO1xuICAgICAgbm93ID0gK25ldyBEYXRlKCk7XG4gICAgICBleHRlbmQgPSBleHBvcnRzLmV4dGVuZCA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydGllcykge1xuICAgICAgICB2YXIga2V5LCB2YWw7XG4gICAgICAgIGZvciAoa2V5IGluIHByb3BlcnRpZXMpIHtcbiAgICAgICAgICB2YWwgPSBwcm9wZXJ0aWVzW2tleV07XG4gICAgICAgICAgb2JqZWN0W2tleV0gPSB2YWw7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgIH07XG4gICAgICBpZiAoIWNiKSB7XG4gICAgICAgIGlmICh0eXBlb2Ygb3B0cyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIGNiID0gb3B0cztcbiAgICAgICAgfVxuICAgICAgICBvcHRpb25zID0ge307XG4gICAgICB9XG4gICAgICBvcHRpb25zID0gZXh0ZW5kKHRoaXMub3B0aW9ucywgb3B0cyk7XG4gICAgICBpZiAodGhpcy50b3RhbENhbGxzVGltZW91dCA8IG5vdykge1xuICAgICAgICB0aGlzLnRvdGFsQ2FsbHMgPSAxO1xuICAgICAgICB0aGlzLnRvdGFsQ2FsbHNUaW1lb3V0ID0gbm93ICsgKDYwICogNjAgKiAxMDAwKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMudG90YWxDYWxscysrO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMuY2FwcGluZ0ZyZWVMdW5jaCA+PSB0aGlzLnRvdGFsQ2FsbHMpIHtcbiAgICAgICAgY2IobnVsbCwgbmV3IEVycm9yKFwiVkFTVCBjYWxsIGNhbmNlbGVkIOKAkyBGcmVlTHVuY2ggY2FwcGluZyBub3QgcmVhY2hlZCB5ZXQgXCIgKyB0aGlzLnRvdGFsQ2FsbHMgKyBcIi9cIiArIHRoaXMuY2FwcGluZ0ZyZWVMdW5jaCkpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aW1lU2luY2VMYXN0Q2FsbCA9IG5vdyAtIHRoaXMubGFzdFN1Y2Nlc3NmdWxsQWQ7XG4gICAgICBpZiAodGltZVNpbmNlTGFzdENhbGwgPCAwKSB7XG4gICAgICAgIHRoaXMubGFzdFN1Y2Nlc3NmdWxsQWQgPSAwO1xuICAgICAgfSBlbHNlIGlmICh0aW1lU2luY2VMYXN0Q2FsbCA8IHRoaXMuY2FwcGluZ01pbmltdW1UaW1lSW50ZXJ2YWwpIHtcbiAgICAgICAgY2IobnVsbCwgbmV3IEVycm9yKFwiVkFTVCBjYWxsIGNhbmNlbGVkIOKAkyAoXCIgKyB0aGlzLmNhcHBpbmdNaW5pbXVtVGltZUludGVydmFsICsgXCIpbXMgbWluaW11bSBpbnRlcnZhbCByZWFjaGVkXCIpKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgcmV0dXJuIFZBU1RQYXJzZXIucGFyc2UodXJsLCBvcHRpb25zLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKHJlc3BvbnNlLCBlcnIpIHtcbiAgICAgICAgICByZXR1cm4gY2IocmVzcG9uc2UsIGVycik7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgfTtcblxuICAgIChmdW5jdGlvbigpIHtcbiAgICAgIHZhciBkZWZpbmVQcm9wZXJ0eSwgc3RvcmFnZTtcbiAgICAgIHN0b3JhZ2UgPSBWQVNUVXRpbC5zdG9yYWdlO1xuICAgICAgZGVmaW5lUHJvcGVydHkgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG4gICAgICBbJ2xhc3RTdWNjZXNzZnVsbEFkJywgJ3RvdGFsQ2FsbHMnLCAndG90YWxDYWxsc1RpbWVvdXQnXS5mb3JFYWNoKGZ1bmN0aW9uKHByb3BlcnR5KSB7XG4gICAgICAgIGRlZmluZVByb3BlcnR5KFZBU1RDbGllbnQsIHByb3BlcnR5LCB7XG4gICAgICAgICAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBzdG9yYWdlLmdldEl0ZW0ocHJvcGVydHkpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2V0OiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgcmV0dXJuIHN0b3JhZ2Uuc2V0SXRlbShwcm9wZXJ0eSwgdmFsdWUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZSxcbiAgICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgICBpZiAoVkFTVENsaWVudC5sYXN0U3VjY2Vzc2Z1bGxBZCA9PSBudWxsKSB7XG4gICAgICAgIFZBU1RDbGllbnQubGFzdFN1Y2Nlc3NmdWxsQWQgPSAwO1xuICAgICAgfVxuICAgICAgaWYgKFZBU1RDbGllbnQudG90YWxDYWxscyA9PSBudWxsKSB7XG4gICAgICAgIFZBU1RDbGllbnQudG90YWxDYWxscyA9IDA7XG4gICAgICB9XG4gICAgICBpZiAoVkFTVENsaWVudC50b3RhbENhbGxzVGltZW91dCA9PSBudWxsKSB7XG4gICAgICAgIFZBU1RDbGllbnQudG90YWxDYWxsc1RpbWVvdXQgPSAwO1xuICAgICAgfVxuICAgIH0pKCk7XG5cbiAgICByZXR1cm4gVkFTVENsaWVudDtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gVkFTVENsaWVudDtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBWQVNUQ29tcGFuaW9uQWQ7XG5cbiAgVkFTVENvbXBhbmlvbkFkID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFZBU1RDb21wYW5pb25BZCgpIHtcbiAgICAgIHRoaXMuaWQgPSBudWxsO1xuICAgICAgdGhpcy53aWR0aCA9IDA7XG4gICAgICB0aGlzLmhlaWdodCA9IDA7XG4gICAgICB0aGlzLnR5cGUgPSBudWxsO1xuICAgICAgdGhpcy5zdGF0aWNSZXNvdXJjZSA9IG51bGw7XG4gICAgICB0aGlzLmh0bWxSZXNvdXJjZSA9IG51bGw7XG4gICAgICB0aGlzLmlmcmFtZVJlc291cmNlID0gbnVsbDtcbiAgICAgIHRoaXMuY29tcGFuaW9uQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgPSBudWxsO1xuICAgICAgdGhpcy50cmFja2luZ0V2ZW50cyA9IHt9O1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUQ29tcGFuaW9uQWQ7XG5cbiAgfSkoKTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IFZBU1RDb21wYW5pb25BZDtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBWQVNUQ3JlYXRpdmUsIFZBU1RDcmVhdGl2ZUNvbXBhbmlvbiwgVkFTVENyZWF0aXZlTGluZWFyLCBWQVNUQ3JlYXRpdmVOb25MaW5lYXIsXG4gICAgZXh0ZW5kID0gZnVuY3Rpb24oY2hpbGQsIHBhcmVudCkgeyBmb3IgKHZhciBrZXkgaW4gcGFyZW50KSB7IGlmIChoYXNQcm9wLmNhbGwocGFyZW50LCBrZXkpKSBjaGlsZFtrZXldID0gcGFyZW50W2tleV07IH0gZnVuY3Rpb24gY3RvcigpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGNoaWxkOyB9IGN0b3IucHJvdG90eXBlID0gcGFyZW50LnByb3RvdHlwZTsgY2hpbGQucHJvdG90eXBlID0gbmV3IGN0b3IoKTsgY2hpbGQuX19zdXBlcl9fID0gcGFyZW50LnByb3RvdHlwZTsgcmV0dXJuIGNoaWxkOyB9LFxuICAgIGhhc1Byb3AgPSB7fS5oYXNPd25Qcm9wZXJ0eTtcblxuICBWQVNUQ3JlYXRpdmUgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gVkFTVENyZWF0aXZlKCkge1xuICAgICAgdGhpcy50cmFja2luZ0V2ZW50cyA9IHt9O1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUQ3JlYXRpdmU7XG5cbiAgfSkoKTtcblxuICBWQVNUQ3JlYXRpdmVMaW5lYXIgPSAoZnVuY3Rpb24oc3VwZXJDbGFzcykge1xuICAgIGV4dGVuZChWQVNUQ3JlYXRpdmVMaW5lYXIsIHN1cGVyQ2xhc3MpO1xuXG4gICAgZnVuY3Rpb24gVkFTVENyZWF0aXZlTGluZWFyKCkge1xuICAgICAgVkFTVENyZWF0aXZlTGluZWFyLl9fc3VwZXJfXy5jb25zdHJ1Y3Rvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgdGhpcy50eXBlID0gXCJsaW5lYXJcIjtcbiAgICAgIHRoaXMuZHVyYXRpb24gPSAwO1xuICAgICAgdGhpcy5za2lwRGVsYXkgPSBudWxsO1xuICAgICAgdGhpcy5tZWRpYUZpbGVzID0gW107XG4gICAgICB0aGlzLnZpZGVvQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgPSBudWxsO1xuICAgICAgdGhpcy52aWRlb0NsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMgPSBbXTtcbiAgICAgIHRoaXMudmlkZW9DdXN0b21DbGlja1VSTFRlbXBsYXRlcyA9IFtdO1xuICAgICAgdGhpcy5hZFBhcmFtZXRlcnMgPSBudWxsO1xuICAgICAgdGhpcy5pY29ucyA9IFtdO1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUQ3JlYXRpdmVMaW5lYXI7XG5cbiAgfSkoVkFTVENyZWF0aXZlKTtcblxuICBWQVNUQ3JlYXRpdmVOb25MaW5lYXIgPSAoZnVuY3Rpb24oc3VwZXJDbGFzcykge1xuICAgIGV4dGVuZChWQVNUQ3JlYXRpdmVOb25MaW5lYXIsIHN1cGVyQ2xhc3MpO1xuXG4gICAgZnVuY3Rpb24gVkFTVENyZWF0aXZlTm9uTGluZWFyKCkge1xuICAgICAgVkFTVENyZWF0aXZlTm9uTGluZWFyLl9fc3VwZXJfXy5jb25zdHJ1Y3Rvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgdGhpcy50eXBlID0gXCJub25saW5lYXJcIjtcbiAgICAgIHRoaXMudmFyaWF0aW9ucyA9IFtdO1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUQ3JlYXRpdmVOb25MaW5lYXI7XG5cbiAgfSkoVkFTVENyZWF0aXZlKTtcblxuICBWQVNUQ3JlYXRpdmVDb21wYW5pb24gPSAoZnVuY3Rpb24oc3VwZXJDbGFzcykge1xuICAgIGV4dGVuZChWQVNUQ3JlYXRpdmVDb21wYW5pb24sIHN1cGVyQ2xhc3MpO1xuXG4gICAgZnVuY3Rpb24gVkFTVENyZWF0aXZlQ29tcGFuaW9uKCkge1xuICAgICAgdGhpcy50eXBlID0gXCJjb21wYW5pb25cIjtcbiAgICAgIHRoaXMudmFyaWF0aW9ucyA9IFtdO1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUQ3JlYXRpdmVDb21wYW5pb247XG5cbiAgfSkoVkFTVENyZWF0aXZlKTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBWQVNUQ3JlYXRpdmVMaW5lYXI6IFZBU1RDcmVhdGl2ZUxpbmVhcixcbiAgICBWQVNUQ3JlYXRpdmVOb25MaW5lYXI6IFZBU1RDcmVhdGl2ZU5vbkxpbmVhcixcbiAgICBWQVNUQ3JlYXRpdmVDb21wYW5pb246IFZBU1RDcmVhdGl2ZUNvbXBhbmlvblxuICB9O1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIFZBU1RBZEV4dGVuc2lvbjtcblxuICBWQVNUQWRFeHRlbnNpb24gPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gVkFTVEFkRXh0ZW5zaW9uKCkge1xuICAgICAgdGhpcy5hdHRyaWJ1dGVzID0ge307XG4gICAgICB0aGlzLmNoaWxkcmVuID0gW107XG4gICAgfVxuXG4gICAgcmV0dXJuIFZBU1RBZEV4dGVuc2lvbjtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gVkFTVEFkRXh0ZW5zaW9uO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIFZBU1RBZEV4dGVuc2lvbkNoaWxkO1xuXG4gIFZBU1RBZEV4dGVuc2lvbkNoaWxkID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFZBU1RBZEV4dGVuc2lvbkNoaWxkKCkge1xuICAgICAgdGhpcy5uYW1lID0gbnVsbDtcbiAgICAgIHRoaXMudmFsdWUgPSBudWxsO1xuICAgICAgdGhpcy5hdHRyaWJ1dGVzID0ge307XG4gICAgfVxuXG4gICAgcmV0dXJuIFZBU1RBZEV4dGVuc2lvbkNoaWxkO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUQWRFeHRlbnNpb25DaGlsZDtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBWQVNUSWNvbjtcblxuICBWQVNUSWNvbiA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBWQVNUSWNvbigpIHtcbiAgICAgIHRoaXMucHJvZ3JhbSA9IG51bGw7XG4gICAgICB0aGlzLmhlaWdodCA9IDA7XG4gICAgICB0aGlzLndpZHRoID0gMDtcbiAgICAgIHRoaXMueFBvc2l0aW9uID0gMDtcbiAgICAgIHRoaXMueVBvc2l0aW9uID0gMDtcbiAgICAgIHRoaXMuYXBpRnJhbWV3b3JrID0gbnVsbDtcbiAgICAgIHRoaXMub2Zmc2V0ID0gbnVsbDtcbiAgICAgIHRoaXMuZHVyYXRpb24gPSAwO1xuICAgICAgdGhpcy50eXBlID0gbnVsbDtcbiAgICAgIHRoaXMuc3RhdGljUmVzb3VyY2UgPSBudWxsO1xuICAgICAgdGhpcy5odG1sUmVzb3VyY2UgPSBudWxsO1xuICAgICAgdGhpcy5pZnJhbWVSZXNvdXJjZSA9IG51bGw7XG4gICAgICB0aGlzLmljb25DbGlja1Rocm91Z2hVUkxUZW1wbGF0ZSA9IG51bGw7XG4gICAgICB0aGlzLmljb25DbGlja1RyYWNraW5nVVJMVGVtcGxhdGVzID0gW107XG4gICAgICB0aGlzLmljb25WaWV3VHJhY2tpbmdVUkxUZW1wbGF0ZSA9IG51bGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIFZBU1RJY29uO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUSWNvbjtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIG1vZHVsZS5leHBvcnRzID0ge1xuICAgIGNsaWVudDogcmVxdWlyZSgnLi9jbGllbnQnKSxcbiAgICB0cmFja2VyOiByZXF1aXJlKCcuL3RyYWNrZXInKSxcbiAgICBwYXJzZXI6IHJlcXVpcmUoJy4vcGFyc2VyJyksXG4gICAgdXRpbDogcmVxdWlyZSgnLi91dGlsJylcbiAgfTtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBWQVNUTWVkaWFGaWxlO1xuXG4gIFZBU1RNZWRpYUZpbGUgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gVkFTVE1lZGlhRmlsZSgpIHtcbiAgICAgIHRoaXMuaWQgPSBudWxsO1xuICAgICAgdGhpcy5maWxlVVJMID0gbnVsbDtcbiAgICAgIHRoaXMuZGVsaXZlcnlUeXBlID0gXCJwcm9ncmVzc2l2ZVwiO1xuICAgICAgdGhpcy5taW1lVHlwZSA9IG51bGw7XG4gICAgICB0aGlzLmNvZGVjID0gbnVsbDtcbiAgICAgIHRoaXMuYml0cmF0ZSA9IDA7XG4gICAgICB0aGlzLm1pbkJpdHJhdGUgPSAwO1xuICAgICAgdGhpcy5tYXhCaXRyYXRlID0gMDtcbiAgICAgIHRoaXMud2lkdGggPSAwO1xuICAgICAgdGhpcy5oZWlnaHQgPSAwO1xuICAgICAgdGhpcy5hcGlGcmFtZXdvcmsgPSBudWxsO1xuICAgICAgdGhpcy5zY2FsYWJsZSA9IG51bGw7XG4gICAgICB0aGlzLm1haW50YWluQXNwZWN0UmF0aW8gPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUTWVkaWFGaWxlO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUTWVkaWFGaWxlO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIFZBU1ROb25MaW5lYXI7XG5cbiAgVkFTVE5vbkxpbmVhciA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBWQVNUTm9uTGluZWFyKCkge1xuICAgICAgdGhpcy5pZCA9IG51bGw7XG4gICAgICB0aGlzLndpZHRoID0gMDtcbiAgICAgIHRoaXMuaGVpZ2h0ID0gMDtcbiAgICAgIHRoaXMubWluU3VnZ2VzdGVkRHVyYXRpb24gPSBcIjAwOjAwOjAwXCI7XG4gICAgICB0aGlzLmFwaUZyYW1ld29yayA9IFwic3RhdGljXCI7XG4gICAgICB0aGlzLnR5cGUgPSBudWxsO1xuICAgICAgdGhpcy5zdGF0aWNSZXNvdXJjZSA9IG51bGw7XG4gICAgICB0aGlzLmh0bWxSZXNvdXJjZSA9IG51bGw7XG4gICAgICB0aGlzLmlmcmFtZVJlc291cmNlID0gbnVsbDtcbiAgICAgIHRoaXMubm9ubGluZWFyQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBWQVNUTm9uTGluZWFyO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUTm9uTGluZWFyO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIEV2ZW50RW1pdHRlciwgVVJMSGFuZGxlciwgVkFTVEFkLCBWQVNUQWRFeHRlbnNpb24sIFZBU1RBZEV4dGVuc2lvbkNoaWxkLCBWQVNUQ29tcGFuaW9uQWQsIFZBU1RDcmVhdGl2ZUNvbXBhbmlvbiwgVkFTVENyZWF0aXZlTGluZWFyLCBWQVNUQ3JlYXRpdmVOb25MaW5lYXIsIFZBU1RJY29uLCBWQVNUTWVkaWFGaWxlLCBWQVNUTm9uTGluZWFyLCBWQVNUUGFyc2VyLCBWQVNUUmVzcG9uc2UsIFZBU1RVdGlsLFxuICAgIGluZGV4T2YgPSBbXS5pbmRleE9mIHx8IGZ1bmN0aW9uKGl0ZW0pIHsgZm9yICh2YXIgaSA9IDAsIGwgPSB0aGlzLmxlbmd0aDsgaSA8IGw7IGkrKykgeyBpZiAoaSBpbiB0aGlzICYmIHRoaXNbaV0gPT09IGl0ZW0pIHJldHVybiBpOyB9IHJldHVybiAtMTsgfTtcblxuICBVUkxIYW5kbGVyID0gcmVxdWlyZSgnLi91cmxoYW5kbGVyJyk7XG5cbiAgVkFTVFJlc3BvbnNlID0gcmVxdWlyZSgnLi9yZXNwb25zZScpO1xuXG4gIFZBU1RBZCA9IHJlcXVpcmUoJy4vYWQnKTtcblxuICBWQVNUQWRFeHRlbnNpb24gPSByZXF1aXJlKCcuL2V4dGVuc2lvbicpO1xuXG4gIFZBU1RBZEV4dGVuc2lvbkNoaWxkID0gcmVxdWlyZSgnLi9leHRlbnNpb25jaGlsZCcpO1xuXG4gIFZBU1RVdGlsID0gcmVxdWlyZSgnLi91dGlsJyk7XG5cbiAgVkFTVENyZWF0aXZlTGluZWFyID0gcmVxdWlyZSgnLi9jcmVhdGl2ZScpLlZBU1RDcmVhdGl2ZUxpbmVhcjtcblxuICBWQVNUQ3JlYXRpdmVDb21wYW5pb24gPSByZXF1aXJlKCcuL2NyZWF0aXZlJykuVkFTVENyZWF0aXZlQ29tcGFuaW9uO1xuXG4gIFZBU1RDcmVhdGl2ZU5vbkxpbmVhciA9IHJlcXVpcmUoJy4vY3JlYXRpdmUnKS5WQVNUQ3JlYXRpdmVOb25MaW5lYXI7XG5cbiAgVkFTVE1lZGlhRmlsZSA9IHJlcXVpcmUoJy4vbWVkaWFmaWxlJyk7XG5cbiAgVkFTVEljb24gPSByZXF1aXJlKCcuL2ljb24nKTtcblxuICBWQVNUQ29tcGFuaW9uQWQgPSByZXF1aXJlKCcuL2NvbXBhbmlvbmFkJyk7XG5cbiAgVkFTVE5vbkxpbmVhciA9IHJlcXVpcmUoJy4vbm9ubGluZWFyJyk7XG5cbiAgRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRzJykuRXZlbnRFbWl0dGVyO1xuXG4gIFZBU1RQYXJzZXIgPSAoZnVuY3Rpb24oKSB7XG4gICAgdmFyIFVSTFRlbXBsYXRlRmlsdGVycztcblxuICAgIGZ1bmN0aW9uIFZBU1RQYXJzZXIoKSB7fVxuXG4gICAgVVJMVGVtcGxhdGVGaWx0ZXJzID0gW107XG5cbiAgICBWQVNUUGFyc2VyLmFkZFVSTFRlbXBsYXRlRmlsdGVyID0gZnVuY3Rpb24oZnVuYykge1xuICAgICAgaWYgKHR5cGVvZiBmdW5jID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIFVSTFRlbXBsYXRlRmlsdGVycy5wdXNoKGZ1bmMpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLnJlbW92ZVVSTFRlbXBsYXRlRmlsdGVyID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gVVJMVGVtcGxhdGVGaWx0ZXJzLnBvcCgpO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLmNvdW50VVJMVGVtcGxhdGVGaWx0ZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gVVJMVGVtcGxhdGVGaWx0ZXJzLmxlbmd0aDtcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5jbGVhclVybFRlbXBsYXRlRmlsdGVycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFVSTFRlbXBsYXRlRmlsdGVycyA9IFtdO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLnBhcnNlID0gZnVuY3Rpb24odXJsLCBvcHRpb25zLCBjYikge1xuICAgICAgaWYgKCFjYikge1xuICAgICAgICBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICBjYiA9IG9wdGlvbnM7XG4gICAgICAgIH1cbiAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMuX3BhcnNlKHVybCwgbnVsbCwgb3B0aW9ucywgZnVuY3Rpb24oZXJyLCByZXNwb25zZSkge1xuICAgICAgICByZXR1cm4gY2IocmVzcG9uc2UsIGVycik7XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci52ZW50ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgVkFTVFBhcnNlci50cmFjayA9IGZ1bmN0aW9uKHRlbXBsYXRlcywgZXJyb3JDb2RlKSB7XG4gICAgICB0aGlzLnZlbnQuZW1pdCgnVkFTVC1lcnJvcicsIGVycm9yQ29kZSk7XG4gICAgICByZXR1cm4gVkFTVFV0aWwudHJhY2sodGVtcGxhdGVzLCBlcnJvckNvZGUpO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLm9uID0gZnVuY3Rpb24oZXZlbnROYW1lLCBjYikge1xuICAgICAgcmV0dXJuIHRoaXMudmVudC5vbihldmVudE5hbWUsIGNiKTtcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5vbmNlID0gZnVuY3Rpb24oZXZlbnROYW1lLCBjYikge1xuICAgICAgcmV0dXJuIHRoaXMudmVudC5vbmNlKGV2ZW50TmFtZSwgY2IpO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLl9wYXJzZSA9IGZ1bmN0aW9uKHVybCwgcGFyZW50VVJMcywgb3B0aW9ucywgY2IpIHtcbiAgICAgIHZhciBmaWx0ZXIsIGksIGxlbjtcbiAgICAgIGlmICghY2IpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBvcHRpb25zID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgY2IgPSBvcHRpb25zO1xuICAgICAgICB9XG4gICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgIH1cbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IFVSTFRlbXBsYXRlRmlsdGVycy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBmaWx0ZXIgPSBVUkxUZW1wbGF0ZUZpbHRlcnNbaV07XG4gICAgICAgIHVybCA9IGZpbHRlcih1cmwpO1xuICAgICAgfVxuICAgICAgaWYgKHBhcmVudFVSTHMgPT0gbnVsbCkge1xuICAgICAgICBwYXJlbnRVUkxzID0gW107XG4gICAgICB9XG4gICAgICBwYXJlbnRVUkxzLnB1c2godXJsKTtcbiAgICAgIHJldHVybiBVUkxIYW5kbGVyLmdldCh1cmwsIG9wdGlvbnMsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXJyLCB4bWwpIHtcbiAgICAgICAgICB2YXIgYWQsIGNvbXBsZXRlLCBqLCBrLCBsZW4xLCBsZW4yLCBsb29wSW5kZXgsIG5vZGUsIHJlZiwgcmVmMSwgcmVzcG9uc2U7XG4gICAgICAgICAgaWYgKGVyciAhPSBudWxsKSB7XG4gICAgICAgICAgICByZXR1cm4gY2IoZXJyKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmVzcG9uc2UgPSBuZXcgVkFTVFJlc3BvbnNlKCk7XG4gICAgICAgICAgaWYgKCEoKCh4bWwgIT0gbnVsbCA/IHhtbC5kb2N1bWVudEVsZW1lbnQgOiB2b2lkIDApICE9IG51bGwpICYmIHhtbC5kb2N1bWVudEVsZW1lbnQubm9kZU5hbWUgPT09IFwiVkFTVFwiKSkge1xuICAgICAgICAgICAgcmV0dXJuIGNiKG5ldyBFcnJvcignSW52YWxpZCBWQVNUIFhNTERvY3VtZW50JykpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZWYgPSB4bWwuZG9jdW1lbnRFbGVtZW50LmNoaWxkTm9kZXM7XG4gICAgICAgICAgZm9yIChqID0gMCwgbGVuMSA9IHJlZi5sZW5ndGg7IGogPCBsZW4xOyBqKyspIHtcbiAgICAgICAgICAgIG5vZGUgPSByZWZbal07XG4gICAgICAgICAgICBpZiAobm9kZS5ub2RlTmFtZSA9PT0gJ0Vycm9yJykge1xuICAgICAgICAgICAgICByZXNwb25zZS5lcnJvclVSTFRlbXBsYXRlcy5wdXNoKF90aGlzLnBhcnNlTm9kZVRleHQobm9kZSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZWYxID0geG1sLmRvY3VtZW50RWxlbWVudC5jaGlsZE5vZGVzO1xuICAgICAgICAgIGZvciAoayA9IDAsIGxlbjIgPSByZWYxLmxlbmd0aDsgayA8IGxlbjI7IGsrKykge1xuICAgICAgICAgICAgbm9kZSA9IHJlZjFba107XG4gICAgICAgICAgICBpZiAobm9kZS5ub2RlTmFtZSA9PT0gJ0FkJykge1xuICAgICAgICAgICAgICBhZCA9IF90aGlzLnBhcnNlQWRFbGVtZW50KG5vZGUpO1xuICAgICAgICAgICAgICBpZiAoYWQgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHJlc3BvbnNlLmFkcy5wdXNoKGFkKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBfdGhpcy50cmFjayhyZXNwb25zZS5lcnJvclVSTFRlbXBsYXRlcywge1xuICAgICAgICAgICAgICAgICAgRVJST1JDT0RFOiAxMDFcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBjb21wbGV0ZSA9IGZ1bmN0aW9uKGVycm9yQWxyZWFkeVJhaXNlZCkge1xuICAgICAgICAgICAgdmFyIGwsIGxlbjMsIG5vQ3JlYXRpdmVzLCByZWYyO1xuICAgICAgICAgICAgaWYgKGVycm9yQWxyZWFkeVJhaXNlZCA9PSBudWxsKSB7XG4gICAgICAgICAgICAgIGVycm9yQWxyZWFkeVJhaXNlZCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKCFyZXNwb25zZSkge1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBub0NyZWF0aXZlcyA9IHRydWU7XG4gICAgICAgICAgICByZWYyID0gcmVzcG9uc2UuYWRzO1xuICAgICAgICAgICAgZm9yIChsID0gMCwgbGVuMyA9IHJlZjIubGVuZ3RoOyBsIDwgbGVuMzsgbCsrKSB7XG4gICAgICAgICAgICAgIGFkID0gcmVmMltsXTtcbiAgICAgICAgICAgICAgaWYgKGFkLm5leHRXcmFwcGVyVVJMICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKGFkLmNyZWF0aXZlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgbm9DcmVhdGl2ZXMgPSBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKG5vQ3JlYXRpdmVzKSB7XG4gICAgICAgICAgICAgIGlmICghZXJyb3JBbHJlYWR5UmFpc2VkKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMudHJhY2socmVzcG9uc2UuZXJyb3JVUkxUZW1wbGF0ZXMsIHtcbiAgICAgICAgICAgICAgICAgIEVSUk9SQ09ERTogMzAzXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5hZHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgIHJlc3BvbnNlID0gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBjYihudWxsLCByZXNwb25zZSk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICBsb29wSW5kZXggPSByZXNwb25zZS5hZHMubGVuZ3RoO1xuICAgICAgICAgIHdoaWxlIChsb29wSW5kZXgtLSkge1xuICAgICAgICAgICAgYWQgPSByZXNwb25zZS5hZHNbbG9vcEluZGV4XTtcbiAgICAgICAgICAgIGlmIChhZC5uZXh0V3JhcHBlclVSTCA9PSBudWxsKSB7XG4gICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgKGZ1bmN0aW9uKGFkKSB7XG4gICAgICAgICAgICAgIHZhciBiYXNlVVJMLCBwcm90b2NvbCwgcmVmMjtcbiAgICAgICAgICAgICAgaWYgKHBhcmVudFVSTHMubGVuZ3RoID49IDEwIHx8IChyZWYyID0gYWQubmV4dFdyYXBwZXJVUkwsIGluZGV4T2YuY2FsbChwYXJlbnRVUkxzLCByZWYyKSA+PSAwKSkge1xuICAgICAgICAgICAgICAgIF90aGlzLnRyYWNrKGFkLmVycm9yVVJMVGVtcGxhdGVzLCB7XG4gICAgICAgICAgICAgICAgICBFUlJPUkNPREU6IDMwMlxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJlc3BvbnNlLmFkcy5zcGxpY2UocmVzcG9uc2UuYWRzLmluZGV4T2YoYWQpLCAxKTtcbiAgICAgICAgICAgICAgICBjb21wbGV0ZSgpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZiAoYWQubmV4dFdyYXBwZXJVUkwuaW5kZXhPZignLy8nKSA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHByb3RvY29sID0gbG9jYXRpb24ucHJvdG9jb2w7XG4gICAgICAgICAgICAgICAgYWQubmV4dFdyYXBwZXJVUkwgPSBcIlwiICsgcHJvdG9jb2wgKyBhZC5uZXh0V3JhcHBlclVSTDtcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChhZC5uZXh0V3JhcHBlclVSTC5pbmRleE9mKCc6Ly8nKSA9PT0gLTEpIHtcbiAgICAgICAgICAgICAgICBiYXNlVVJMID0gdXJsLnNsaWNlKDAsIHVybC5sYXN0SW5kZXhPZignLycpKTtcbiAgICAgICAgICAgICAgICBhZC5uZXh0V3JhcHBlclVSTCA9IGJhc2VVUkwgKyBcIi9cIiArIGFkLm5leHRXcmFwcGVyVVJMO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5fcGFyc2UoYWQubmV4dFdyYXBwZXJVUkwsIHBhcmVudFVSTHMsIG9wdGlvbnMsIGZ1bmN0aW9uKGVyciwgd3JhcHBlZFJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgdmFyIGJhc2UsIGNyZWF0aXZlLCBlcnJvckFscmVhZHlSYWlzZWQsIGV2ZW50TmFtZSwgaW5kZXgsIGwsIGxlbjMsIGxlbjQsIGxlbjUsIGxlbjYsIGxlbjcsIGxlbjgsIG0sIG4sIG8sIHAsIHEsIHJlZjMsIHJlZjQsIHJlZjUsIHJlZjYsIHJlZjcsIHJlZjgsIHdyYXBwZWRBZDtcbiAgICAgICAgICAgICAgICBlcnJvckFscmVhZHlSYWlzZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBpZiAoZXJyICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLnRyYWNrKGFkLmVycm9yVVJMVGVtcGxhdGVzLCB7XG4gICAgICAgICAgICAgICAgICAgIEVSUk9SQ09ERTogMzAxXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmFkcy5zcGxpY2UocmVzcG9uc2UuYWRzLmluZGV4T2YoYWQpLCAxKTtcbiAgICAgICAgICAgICAgICAgIGVycm9yQWxyZWFkeVJhaXNlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh3cmFwcGVkUmVzcG9uc2UgPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMudHJhY2soYWQuZXJyb3JVUkxUZW1wbGF0ZXMsIHtcbiAgICAgICAgICAgICAgICAgICAgRVJST1JDT0RFOiAzMDNcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuYWRzLnNwbGljZShyZXNwb25zZS5hZHMuaW5kZXhPZihhZCksIDEpO1xuICAgICAgICAgICAgICAgICAgZXJyb3JBbHJlYWR5UmFpc2VkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuZXJyb3JVUkxUZW1wbGF0ZXMgPSByZXNwb25zZS5lcnJvclVSTFRlbXBsYXRlcy5jb25jYXQod3JhcHBlZFJlc3BvbnNlLmVycm9yVVJMVGVtcGxhdGVzKTtcbiAgICAgICAgICAgICAgICAgIGluZGV4ID0gcmVzcG9uc2UuYWRzLmluZGV4T2YoYWQpO1xuICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuYWRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgICByZWYzID0gd3JhcHBlZFJlc3BvbnNlLmFkcztcbiAgICAgICAgICAgICAgICAgIGZvciAobCA9IDAsIGxlbjMgPSByZWYzLmxlbmd0aDsgbCA8IGxlbjM7IGwrKykge1xuICAgICAgICAgICAgICAgICAgICB3cmFwcGVkQWQgPSByZWYzW2xdO1xuICAgICAgICAgICAgICAgICAgICB3cmFwcGVkQWQuZXJyb3JVUkxUZW1wbGF0ZXMgPSBhZC5lcnJvclVSTFRlbXBsYXRlcy5jb25jYXQod3JhcHBlZEFkLmVycm9yVVJMVGVtcGxhdGVzKTtcbiAgICAgICAgICAgICAgICAgICAgd3JhcHBlZEFkLmltcHJlc3Npb25VUkxUZW1wbGF0ZXMgPSBhZC5pbXByZXNzaW9uVVJMVGVtcGxhdGVzLmNvbmNhdCh3cmFwcGVkQWQuaW1wcmVzc2lvblVSTFRlbXBsYXRlcyk7XG4gICAgICAgICAgICAgICAgICAgIHdyYXBwZWRBZC5leHRlbnNpb25zID0gYWQuZXh0ZW5zaW9ucy5jb25jYXQod3JhcHBlZEFkLmV4dGVuc2lvbnMpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoYWQudHJhY2tpbmdFdmVudHMgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJlZjQgPSB3cmFwcGVkQWQuY3JlYXRpdmVzO1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAobSA9IDAsIGxlbjQgPSByZWY0Lmxlbmd0aDsgbSA8IGxlbjQ7IG0rKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUgPSByZWY0W21dO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkLnRyYWNraW5nRXZlbnRzW2NyZWF0aXZlLnR5cGVdICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmVmNSA9IE9iamVjdC5rZXlzKGFkLnRyYWNraW5nRXZlbnRzW2NyZWF0aXZlLnR5cGVdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChuID0gMCwgbGVuNSA9IHJlZjUubGVuZ3RoOyBuIDwgbGVuNTsgbisrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnROYW1lID0gcmVmNVtuXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoYmFzZSA9IGNyZWF0aXZlLnRyYWNraW5nRXZlbnRzKVtldmVudE5hbWVdIHx8IChiYXNlW2V2ZW50TmFtZV0gPSBbXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUudHJhY2tpbmdFdmVudHNbZXZlbnROYW1lXSA9IGNyZWF0aXZlLnRyYWNraW5nRXZlbnRzW2V2ZW50TmFtZV0uY29uY2F0KGFkLnRyYWNraW5nRXZlbnRzW2NyZWF0aXZlLnR5cGVdW2V2ZW50TmFtZV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChhZC52aWRlb0NsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJlZjYgPSB3cmFwcGVkQWQuY3JlYXRpdmVzO1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAobyA9IDAsIGxlbjYgPSByZWY2Lmxlbmd0aDsgbyA8IGxlbjY7IG8rKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUgPSByZWY2W29dO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0aXZlLnR5cGUgPT09ICdsaW5lYXInKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0aXZlLnZpZGVvQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcyA9IGNyZWF0aXZlLnZpZGVvQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcy5jb25jYXQoYWQudmlkZW9DbGlja1RyYWNraW5nVVJMVGVtcGxhdGVzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGFkLnZpZGVvQ3VzdG9tQ2xpY2tVUkxUZW1wbGF0ZXMgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJlZjcgPSB3cmFwcGVkQWQuY3JlYXRpdmVzO1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAocCA9IDAsIGxlbjcgPSByZWY3Lmxlbmd0aDsgcCA8IGxlbjc7IHArKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUgPSByZWY3W3BdO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0aXZlLnR5cGUgPT09ICdsaW5lYXInKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0aXZlLnZpZGVvQ3VzdG9tQ2xpY2tVUkxUZW1wbGF0ZXMgPSBjcmVhdGl2ZS52aWRlb0N1c3RvbUNsaWNrVVJMVGVtcGxhdGVzLmNvbmNhdChhZC52aWRlb0N1c3RvbUNsaWNrVVJMVGVtcGxhdGVzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGFkLnZpZGVvQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJlZjggPSB3cmFwcGVkQWQuY3JlYXRpdmVzO1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAocSA9IDAsIGxlbjggPSByZWY4Lmxlbmd0aDsgcSA8IGxlbjg7IHErKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUgPSByZWY4W3FdO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0aXZlLnR5cGUgPT09ICdsaW5lYXInICYmIChjcmVhdGl2ZS52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlID09IG51bGwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0aXZlLnZpZGVvQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgPSBhZC52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5hZHMuc3BsaWNlKCsraW5kZXgsIDAsIHdyYXBwZWRBZCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRlbGV0ZSBhZC5uZXh0V3JhcHBlclVSTDtcbiAgICAgICAgICAgICAgICByZXR1cm4gY29tcGxldGUoZXJyb3JBbHJlYWR5UmFpc2VkKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KShhZCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBjb21wbGV0ZSgpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLmNoaWxkQnlOYW1lID0gZnVuY3Rpb24obm9kZSwgbmFtZSkge1xuICAgICAgdmFyIGNoaWxkLCBpLCBsZW4sIHJlZjtcbiAgICAgIHJlZiA9IG5vZGUuY2hpbGROb2RlcztcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBjaGlsZCA9IHJlZltpXTtcbiAgICAgICAgaWYgKGNoaWxkLm5vZGVOYW1lID09PSBuYW1lKSB7XG4gICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIFZBU1RQYXJzZXIuY2hpbGRzQnlOYW1lID0gZnVuY3Rpb24obm9kZSwgbmFtZSkge1xuICAgICAgdmFyIGNoaWxkLCBjaGlsZHMsIGksIGxlbiwgcmVmO1xuICAgICAgY2hpbGRzID0gW107XG4gICAgICByZWYgPSBub2RlLmNoaWxkTm9kZXM7XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSByZWYubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgY2hpbGQgPSByZWZbaV07XG4gICAgICAgIGlmIChjaGlsZC5ub2RlTmFtZSA9PT0gbmFtZSkge1xuICAgICAgICAgIGNoaWxkcy5wdXNoKGNoaWxkKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIGNoaWxkcztcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5wYXJzZUFkRWxlbWVudCA9IGZ1bmN0aW9uKGFkRWxlbWVudCkge1xuICAgICAgdmFyIGFkVHlwZUVsZW1lbnQsIGksIGxlbiwgcmVmLCByZWYxO1xuICAgICAgcmVmID0gYWRFbGVtZW50LmNoaWxkTm9kZXM7XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSByZWYubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgYWRUeXBlRWxlbWVudCA9IHJlZltpXTtcbiAgICAgICAgaWYgKChyZWYxID0gYWRUeXBlRWxlbWVudC5ub2RlTmFtZSkgIT09IFwiV3JhcHBlclwiICYmIHJlZjEgIT09IFwiSW5MaW5lXCIpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmNvcHlOb2RlQXR0cmlidXRlKFwiaWRcIiwgYWRFbGVtZW50LCBhZFR5cGVFbGVtZW50KTtcbiAgICAgICAgdGhpcy5jb3B5Tm9kZUF0dHJpYnV0ZShcInNlcXVlbmNlXCIsIGFkRWxlbWVudCwgYWRUeXBlRWxlbWVudCk7XG4gICAgICAgIGlmIChhZFR5cGVFbGVtZW50Lm5vZGVOYW1lID09PSBcIldyYXBwZXJcIikge1xuICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlV3JhcHBlckVsZW1lbnQoYWRUeXBlRWxlbWVudCk7XG4gICAgICAgIH0gZWxzZSBpZiAoYWRUeXBlRWxlbWVudC5ub2RlTmFtZSA9PT0gXCJJbkxpbmVcIikge1xuICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlSW5MaW5lRWxlbWVudChhZFR5cGVFbGVtZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLnBhcnNlV3JhcHBlckVsZW1lbnQgPSBmdW5jdGlvbih3cmFwcGVyRWxlbWVudCkge1xuICAgICAgdmFyIGFkLCBjcmVhdGl2ZSwgaSwgbGVuLCByZWYsIHdyYXBwZXJDcmVhdGl2ZUVsZW1lbnQsIHdyYXBwZXJVUkxFbGVtZW50O1xuICAgICAgYWQgPSB0aGlzLnBhcnNlSW5MaW5lRWxlbWVudCh3cmFwcGVyRWxlbWVudCk7XG4gICAgICB3cmFwcGVyVVJMRWxlbWVudCA9IHRoaXMuY2hpbGRCeU5hbWUod3JhcHBlckVsZW1lbnQsIFwiVkFTVEFkVGFnVVJJXCIpO1xuICAgICAgaWYgKHdyYXBwZXJVUkxFbGVtZW50ICE9IG51bGwpIHtcbiAgICAgICAgYWQubmV4dFdyYXBwZXJVUkwgPSB0aGlzLnBhcnNlTm9kZVRleHQod3JhcHBlclVSTEVsZW1lbnQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgd3JhcHBlclVSTEVsZW1lbnQgPSB0aGlzLmNoaWxkQnlOYW1lKHdyYXBwZXJFbGVtZW50LCBcIlZBU1RBZFRhZ1VSTFwiKTtcbiAgICAgICAgaWYgKHdyYXBwZXJVUkxFbGVtZW50ICE9IG51bGwpIHtcbiAgICAgICAgICBhZC5uZXh0V3JhcHBlclVSTCA9IHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKHdyYXBwZXJVUkxFbGVtZW50LCBcIlVSTFwiKSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJlZiA9IGFkLmNyZWF0aXZlcztcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBjcmVhdGl2ZSA9IHJlZltpXTtcbiAgICAgICAgd3JhcHBlckNyZWF0aXZlRWxlbWVudCA9IG51bGw7XG4gICAgICAgIGlmIChjcmVhdGl2ZS50eXBlID09PSAnbGluZWFyJyB8fCBjcmVhdGl2ZS50eXBlID09PSAnbm9ubGluZWFyJykge1xuICAgICAgICAgIHdyYXBwZXJDcmVhdGl2ZUVsZW1lbnQgPSBjcmVhdGl2ZTtcbiAgICAgICAgICBpZiAod3JhcHBlckNyZWF0aXZlRWxlbWVudCAhPSBudWxsKSB7XG4gICAgICAgICAgICBpZiAod3JhcHBlckNyZWF0aXZlRWxlbWVudC50cmFja2luZ0V2ZW50cyAhPSBudWxsKSB7XG4gICAgICAgICAgICAgIGFkLnRyYWNraW5nRXZlbnRzIHx8IChhZC50cmFja2luZ0V2ZW50cyA9IHt9KTtcbiAgICAgICAgICAgICAgYWQudHJhY2tpbmdFdmVudHNbd3JhcHBlckNyZWF0aXZlRWxlbWVudC50eXBlXSA9IHdyYXBwZXJDcmVhdGl2ZUVsZW1lbnQudHJhY2tpbmdFdmVudHM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAod3JhcHBlckNyZWF0aXZlRWxlbWVudC52aWRlb0NsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMgIT0gbnVsbCkge1xuICAgICAgICAgICAgICBhZC52aWRlb0NsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMgPSB3cmFwcGVyQ3JlYXRpdmVFbGVtZW50LnZpZGVvQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh3cmFwcGVyQ3JlYXRpdmVFbGVtZW50LnZpZGVvQ2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgIT0gbnVsbCkge1xuICAgICAgICAgICAgICBhZC52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlID0gd3JhcHBlckNyZWF0aXZlRWxlbWVudC52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHdyYXBwZXJDcmVhdGl2ZUVsZW1lbnQudmlkZW9DdXN0b21DbGlja1VSTFRlbXBsYXRlcyAhPSBudWxsKSB7XG4gICAgICAgICAgICAgIGFkLnZpZGVvQ3VzdG9tQ2xpY2tVUkxUZW1wbGF0ZXMgPSB3cmFwcGVyQ3JlYXRpdmVFbGVtZW50LnZpZGVvQ3VzdG9tQ2xpY2tVUkxUZW1wbGF0ZXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoYWQubmV4dFdyYXBwZXJVUkwgIT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gYWQ7XG4gICAgICB9XG4gICAgfTtcblxuICAgIFZBU1RQYXJzZXIucGFyc2VJbkxpbmVFbGVtZW50ID0gZnVuY3Rpb24oaW5MaW5lRWxlbWVudCkge1xuICAgICAgdmFyIGFkLCBjcmVhdGl2ZSwgY3JlYXRpdmVFbGVtZW50LCBjcmVhdGl2ZVR5cGVFbGVtZW50LCBpLCBqLCBrLCBsZW4sIGxlbjEsIGxlbjIsIG5vZGUsIHJlZiwgcmVmMSwgcmVmMjtcbiAgICAgIGFkID0gbmV3IFZBU1RBZCgpO1xuICAgICAgYWQuaWQgPSBpbkxpbmVFbGVtZW50LmdldEF0dHJpYnV0ZShcImlkXCIpIHx8IG51bGw7XG4gICAgICBhZC5zZXF1ZW5jZSA9IGluTGluZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwic2VxdWVuY2VcIikgfHwgbnVsbDtcbiAgICAgIHJlZiA9IGluTGluZUVsZW1lbnQuY2hpbGROb2RlcztcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBub2RlID0gcmVmW2ldO1xuICAgICAgICBzd2l0Y2ggKG5vZGUubm9kZU5hbWUpIHtcbiAgICAgICAgICBjYXNlIFwiRXJyb3JcIjpcbiAgICAgICAgICAgIGFkLmVycm9yVVJMVGVtcGxhdGVzLnB1c2godGhpcy5wYXJzZU5vZGVUZXh0KG5vZGUpKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCJJbXByZXNzaW9uXCI6XG4gICAgICAgICAgICBhZC5pbXByZXNzaW9uVVJMVGVtcGxhdGVzLnB1c2godGhpcy5wYXJzZU5vZGVUZXh0KG5vZGUpKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCJDcmVhdGl2ZXNcIjpcbiAgICAgICAgICAgIHJlZjEgPSB0aGlzLmNoaWxkc0J5TmFtZShub2RlLCBcIkNyZWF0aXZlXCIpO1xuICAgICAgICAgICAgZm9yIChqID0gMCwgbGVuMSA9IHJlZjEubGVuZ3RoOyBqIDwgbGVuMTsgaisrKSB7XG4gICAgICAgICAgICAgIGNyZWF0aXZlRWxlbWVudCA9IHJlZjFbal07XG4gICAgICAgICAgICAgIHJlZjIgPSBjcmVhdGl2ZUVsZW1lbnQuY2hpbGROb2RlcztcbiAgICAgICAgICAgICAgZm9yIChrID0gMCwgbGVuMiA9IHJlZjIubGVuZ3RoOyBrIDwgbGVuMjsgaysrKSB7XG4gICAgICAgICAgICAgICAgY3JlYXRpdmVUeXBlRWxlbWVudCA9IHJlZjJba107XG4gICAgICAgICAgICAgICAgc3dpdGNoIChjcmVhdGl2ZVR5cGVFbGVtZW50Lm5vZGVOYW1lKSB7XG4gICAgICAgICAgICAgICAgICBjYXNlIFwiTGluZWFyXCI6XG4gICAgICAgICAgICAgICAgICAgIGNyZWF0aXZlID0gdGhpcy5wYXJzZUNyZWF0aXZlTGluZWFyRWxlbWVudChjcmVhdGl2ZVR5cGVFbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgYWQuY3JlYXRpdmVzLnB1c2goY3JlYXRpdmUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgY2FzZSBcIk5vbkxpbmVhckFkc1wiOlxuICAgICAgICAgICAgICAgICAgICBjcmVhdGl2ZSA9IHRoaXMucGFyc2VOb25MaW5lYXIoY3JlYXRpdmVUeXBlRWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChjcmVhdGl2ZSkge1xuICAgICAgICAgICAgICAgICAgICAgIGFkLmNyZWF0aXZlcy5wdXNoKGNyZWF0aXZlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgIGNhc2UgXCJDb21wYW5pb25BZHNcIjpcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRpdmUgPSB0aGlzLnBhcnNlQ29tcGFuaW9uQWQoY3JlYXRpdmVUeXBlRWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChjcmVhdGl2ZSkge1xuICAgICAgICAgICAgICAgICAgICAgIGFkLmNyZWF0aXZlcy5wdXNoKGNyZWF0aXZlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIkV4dGVuc2lvbnNcIjpcbiAgICAgICAgICAgIHRoaXMucGFyc2VFeHRlbnNpb24oYWQuZXh0ZW5zaW9ucywgdGhpcy5jaGlsZHNCeU5hbWUobm9kZSwgXCJFeHRlbnNpb25cIikpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIkFkU3lzdGVtXCI6XG4gICAgICAgICAgICBhZC5zeXN0ZW0gPSB7XG4gICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnBhcnNlTm9kZVRleHQobm9kZSksXG4gICAgICAgICAgICAgIHZlcnNpb246IG5vZGUuZ2V0QXR0cmlidXRlKFwidmVyc2lvblwiKSB8fCBudWxsXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIkFkVGl0bGVcIjpcbiAgICAgICAgICAgIGFkLnRpdGxlID0gdGhpcy5wYXJzZU5vZGVUZXh0KG5vZGUpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIkRlc2NyaXB0aW9uXCI6XG4gICAgICAgICAgICBhZC5kZXNjcmlwdGlvbiA9IHRoaXMucGFyc2VOb2RlVGV4dChub2RlKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCJBZHZlcnRpc2VyXCI6XG4gICAgICAgICAgICBhZC5hZHZlcnRpc2VyID0gdGhpcy5wYXJzZU5vZGVUZXh0KG5vZGUpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIlByaWNpbmdcIjpcbiAgICAgICAgICAgIGFkLnByaWNpbmcgPSB7XG4gICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnBhcnNlTm9kZVRleHQobm9kZSksXG4gICAgICAgICAgICAgIG1vZGVsOiBub2RlLmdldEF0dHJpYnV0ZShcIm1vZGVsXCIpIHx8IG51bGwsXG4gICAgICAgICAgICAgIGN1cnJlbmN5OiBub2RlLmdldEF0dHJpYnV0ZShcImN1cnJlbmN5XCIpIHx8IG51bGxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiU3VydmV5XCI6XG4gICAgICAgICAgICBhZC5zdXJ2ZXkgPSB0aGlzLnBhcnNlTm9kZVRleHQobm9kZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBhZDtcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5wYXJzZUV4dGVuc2lvbiA9IGZ1bmN0aW9uKGNvbGxlY3Rpb24sIGV4dGVuc2lvbnMpIHtcbiAgICAgIHZhciBjaGlsZE5vZGUsIGV4dCwgZXh0Q2hpbGQsIGV4dENoaWxkTm9kZUF0dHIsIGV4dE5vZGUsIGV4dE5vZGVBdHRyLCBpLCBqLCBrLCBsLCBsZW4sIGxlbjEsIGxlbjIsIGxlbjMsIHJlZiwgcmVmMSwgcmVmMiwgcmVzdWx0cztcbiAgICAgIHJlc3VsdHMgPSBbXTtcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IGV4dGVuc2lvbnMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgZXh0Tm9kZSA9IGV4dGVuc2lvbnNbaV07XG4gICAgICAgIGV4dCA9IG5ldyBWQVNUQWRFeHRlbnNpb24oKTtcbiAgICAgICAgaWYgKGV4dE5vZGUuYXR0cmlidXRlcykge1xuICAgICAgICAgIHJlZiA9IGV4dE5vZGUuYXR0cmlidXRlcztcbiAgICAgICAgICBmb3IgKGogPSAwLCBsZW4xID0gcmVmLmxlbmd0aDsgaiA8IGxlbjE7IGorKykge1xuICAgICAgICAgICAgZXh0Tm9kZUF0dHIgPSByZWZbal07XG4gICAgICAgICAgICBleHQuYXR0cmlidXRlc1tleHROb2RlQXR0ci5ub2RlTmFtZV0gPSBleHROb2RlQXR0ci5ub2RlVmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJlZjEgPSBleHROb2RlLmNoaWxkTm9kZXM7XG4gICAgICAgIGZvciAoayA9IDAsIGxlbjIgPSByZWYxLmxlbmd0aDsgayA8IGxlbjI7IGsrKykge1xuICAgICAgICAgIGNoaWxkTm9kZSA9IHJlZjFba107XG4gICAgICAgICAgaWYgKGNoaWxkTm9kZS5ub2RlTmFtZSAhPT0gJyN0ZXh0Jykge1xuICAgICAgICAgICAgZXh0Q2hpbGQgPSBuZXcgVkFTVEFkRXh0ZW5zaW9uQ2hpbGQoKTtcbiAgICAgICAgICAgIGV4dENoaWxkLm5hbWUgPSBjaGlsZE5vZGUubm9kZU5hbWU7XG4gICAgICAgICAgICBleHRDaGlsZC52YWx1ZSA9IHRoaXMucGFyc2VOb2RlVGV4dChjaGlsZE5vZGUpO1xuICAgICAgICAgICAgaWYgKGNoaWxkTm9kZS5hdHRyaWJ1dGVzKSB7XG4gICAgICAgICAgICAgIHJlZjIgPSBjaGlsZE5vZGUuYXR0cmlidXRlcztcbiAgICAgICAgICAgICAgZm9yIChsID0gMCwgbGVuMyA9IHJlZjIubGVuZ3RoOyBsIDwgbGVuMzsgbCsrKSB7XG4gICAgICAgICAgICAgICAgZXh0Q2hpbGROb2RlQXR0ciA9IHJlZjJbbF07XG4gICAgICAgICAgICAgICAgZXh0Q2hpbGQuYXR0cmlidXRlc1tleHRDaGlsZE5vZGVBdHRyLm5vZGVOYW1lXSA9IGV4dENoaWxkTm9kZUF0dHIubm9kZVZhbHVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBleHQuY2hpbGRyZW4ucHVzaChleHRDaGlsZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJlc3VsdHMucHVzaChjb2xsZWN0aW9uLnB1c2goZXh0KSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gcmVzdWx0cztcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5wYXJzZUNyZWF0aXZlTGluZWFyRWxlbWVudCA9IGZ1bmN0aW9uKGNyZWF0aXZlRWxlbWVudCkge1xuICAgICAgdmFyIGFkUGFyYW1zRWxlbWVudCwgYmFzZSwgY2xpY2tUcmFja2luZ0VsZW1lbnQsIGNyZWF0aXZlLCBjdXN0b21DbGlja0VsZW1lbnQsIGV2ZW50TmFtZSwgaHRtbEVsZW1lbnQsIGksIGljb24sIGljb25DbGlja1RyYWNraW5nRWxlbWVudCwgaWNvbkNsaWNrc0VsZW1lbnQsIGljb25FbGVtZW50LCBpY29uc0VsZW1lbnQsIGlmcmFtZUVsZW1lbnQsIGosIGssIGwsIGxlbiwgbGVuMSwgbGVuMTAsIGxlbjIsIGxlbjMsIGxlbjQsIGxlbjUsIGxlbjYsIGxlbjcsIGxlbjgsIGxlbjksIG0sIG1haW50YWluQXNwZWN0UmF0aW8sIG1lZGlhRmlsZSwgbWVkaWFGaWxlRWxlbWVudCwgbWVkaWFGaWxlc0VsZW1lbnQsIG4sIG8sIG9mZnNldCwgcCwgcGVyY2VudCwgcSwgciwgcmVmLCByZWYxLCByZWYxMCwgcmVmMiwgcmVmMywgcmVmNCwgcmVmNSwgcmVmNiwgcmVmNywgcmVmOCwgcmVmOSwgcywgc2NhbGFibGUsIHNraXBPZmZzZXQsIHN0YXRpY0VsZW1lbnQsIHRyYWNraW5nRWxlbWVudCwgdHJhY2tpbmdFdmVudHNFbGVtZW50LCB0cmFja2luZ1VSTFRlbXBsYXRlLCB2aWRlb0NsaWNrc0VsZW1lbnQ7XG4gICAgICBjcmVhdGl2ZSA9IG5ldyBWQVNUQ3JlYXRpdmVMaW5lYXIoKTtcbiAgICAgIGNyZWF0aXZlLmR1cmF0aW9uID0gdGhpcy5wYXJzZUR1cmF0aW9uKHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKGNyZWF0aXZlRWxlbWVudCwgXCJEdXJhdGlvblwiKSkpO1xuICAgICAgaWYgKGNyZWF0aXZlLmR1cmF0aW9uID09PSAtMSAmJiBjcmVhdGl2ZUVsZW1lbnQucGFyZW50Tm9kZS5wYXJlbnROb2RlLnBhcmVudE5vZGUubm9kZU5hbWUgIT09ICdXcmFwcGVyJykge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICAgIHNraXBPZmZzZXQgPSBjcmVhdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwic2tpcG9mZnNldFwiKTtcbiAgICAgIGlmIChza2lwT2Zmc2V0ID09IG51bGwpIHtcbiAgICAgICAgY3JlYXRpdmUuc2tpcERlbGF5ID0gbnVsbDtcbiAgICAgIH0gZWxzZSBpZiAoc2tpcE9mZnNldC5jaGFyQXQoc2tpcE9mZnNldC5sZW5ndGggLSAxKSA9PT0gXCIlXCIpIHtcbiAgICAgICAgcGVyY2VudCA9IHBhcnNlSW50KHNraXBPZmZzZXQsIDEwKTtcbiAgICAgICAgY3JlYXRpdmUuc2tpcERlbGF5ID0gY3JlYXRpdmUuZHVyYXRpb24gKiAocGVyY2VudCAvIDEwMCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjcmVhdGl2ZS5za2lwRGVsYXkgPSB0aGlzLnBhcnNlRHVyYXRpb24oc2tpcE9mZnNldCk7XG4gICAgICB9XG4gICAgICB2aWRlb0NsaWNrc0VsZW1lbnQgPSB0aGlzLmNoaWxkQnlOYW1lKGNyZWF0aXZlRWxlbWVudCwgXCJWaWRlb0NsaWNrc1wiKTtcbiAgICAgIGlmICh2aWRlb0NsaWNrc0VsZW1lbnQgIT0gbnVsbCkge1xuICAgICAgICBjcmVhdGl2ZS52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlID0gdGhpcy5wYXJzZU5vZGVUZXh0KHRoaXMuY2hpbGRCeU5hbWUodmlkZW9DbGlja3NFbGVtZW50LCBcIkNsaWNrVGhyb3VnaFwiKSk7XG4gICAgICAgIHJlZiA9IHRoaXMuY2hpbGRzQnlOYW1lKHZpZGVvQ2xpY2tzRWxlbWVudCwgXCJDbGlja1RyYWNraW5nXCIpO1xuICAgICAgICBmb3IgKGkgPSAwLCBsZW4gPSByZWYubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgICBjbGlja1RyYWNraW5nRWxlbWVudCA9IHJlZltpXTtcbiAgICAgICAgICBjcmVhdGl2ZS52aWRlb0NsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMucHVzaCh0aGlzLnBhcnNlTm9kZVRleHQoY2xpY2tUcmFja2luZ0VsZW1lbnQpKTtcbiAgICAgICAgfVxuICAgICAgICByZWYxID0gdGhpcy5jaGlsZHNCeU5hbWUodmlkZW9DbGlja3NFbGVtZW50LCBcIkN1c3RvbUNsaWNrXCIpO1xuICAgICAgICBmb3IgKGogPSAwLCBsZW4xID0gcmVmMS5sZW5ndGg7IGogPCBsZW4xOyBqKyspIHtcbiAgICAgICAgICBjdXN0b21DbGlja0VsZW1lbnQgPSByZWYxW2pdO1xuICAgICAgICAgIGNyZWF0aXZlLnZpZGVvQ3VzdG9tQ2xpY2tVUkxUZW1wbGF0ZXMucHVzaCh0aGlzLnBhcnNlTm9kZVRleHQoY3VzdG9tQ2xpY2tFbGVtZW50KSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGFkUGFyYW1zRWxlbWVudCA9IHRoaXMuY2hpbGRCeU5hbWUoY3JlYXRpdmVFbGVtZW50LCBcIkFkUGFyYW1ldGVyc1wiKTtcbiAgICAgIGlmIChhZFBhcmFtc0VsZW1lbnQgIT0gbnVsbCkge1xuICAgICAgICBjcmVhdGl2ZS5hZFBhcmFtZXRlcnMgPSB0aGlzLnBhcnNlTm9kZVRleHQoYWRQYXJhbXNFbGVtZW50KTtcbiAgICAgIH1cbiAgICAgIHJlZjIgPSB0aGlzLmNoaWxkc0J5TmFtZShjcmVhdGl2ZUVsZW1lbnQsIFwiVHJhY2tpbmdFdmVudHNcIik7XG4gICAgICBmb3IgKGsgPSAwLCBsZW4yID0gcmVmMi5sZW5ndGg7IGsgPCBsZW4yOyBrKyspIHtcbiAgICAgICAgdHJhY2tpbmdFdmVudHNFbGVtZW50ID0gcmVmMltrXTtcbiAgICAgICAgcmVmMyA9IHRoaXMuY2hpbGRzQnlOYW1lKHRyYWNraW5nRXZlbnRzRWxlbWVudCwgXCJUcmFja2luZ1wiKTtcbiAgICAgICAgZm9yIChsID0gMCwgbGVuMyA9IHJlZjMubGVuZ3RoOyBsIDwgbGVuMzsgbCsrKSB7XG4gICAgICAgICAgdHJhY2tpbmdFbGVtZW50ID0gcmVmM1tsXTtcbiAgICAgICAgICBldmVudE5hbWUgPSB0cmFja2luZ0VsZW1lbnQuZ2V0QXR0cmlidXRlKFwiZXZlbnRcIik7XG4gICAgICAgICAgdHJhY2tpbmdVUkxUZW1wbGF0ZSA9IHRoaXMucGFyc2VOb2RlVGV4dCh0cmFja2luZ0VsZW1lbnQpO1xuICAgICAgICAgIGlmICgoZXZlbnROYW1lICE9IG51bGwpICYmICh0cmFja2luZ1VSTFRlbXBsYXRlICE9IG51bGwpKSB7XG4gICAgICAgICAgICBpZiAoZXZlbnROYW1lID09PSBcInByb2dyZXNzXCIpIHtcbiAgICAgICAgICAgICAgb2Zmc2V0ID0gdHJhY2tpbmdFbGVtZW50LmdldEF0dHJpYnV0ZShcIm9mZnNldFwiKTtcbiAgICAgICAgICAgICAgaWYgKCFvZmZzZXQpIHtcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZiAob2Zmc2V0LmNoYXJBdChvZmZzZXQubGVuZ3RoIC0gMSkgPT09ICclJykge1xuICAgICAgICAgICAgICAgIGV2ZW50TmFtZSA9IFwicHJvZ3Jlc3MtXCIgKyBvZmZzZXQ7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZXZlbnROYW1lID0gXCJwcm9ncmVzcy1cIiArIChNYXRoLnJvdW5kKHRoaXMucGFyc2VEdXJhdGlvbihvZmZzZXQpKSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICgoYmFzZSA9IGNyZWF0aXZlLnRyYWNraW5nRXZlbnRzKVtldmVudE5hbWVdID09IG51bGwpIHtcbiAgICAgICAgICAgICAgYmFzZVtldmVudE5hbWVdID0gW107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjcmVhdGl2ZS50cmFja2luZ0V2ZW50c1tldmVudE5hbWVdLnB1c2godHJhY2tpbmdVUkxUZW1wbGF0ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZWY0ID0gdGhpcy5jaGlsZHNCeU5hbWUoY3JlYXRpdmVFbGVtZW50LCBcIk1lZGlhRmlsZXNcIik7XG4gICAgICBmb3IgKG0gPSAwLCBsZW40ID0gcmVmNC5sZW5ndGg7IG0gPCBsZW40OyBtKyspIHtcbiAgICAgICAgbWVkaWFGaWxlc0VsZW1lbnQgPSByZWY0W21dO1xuICAgICAgICByZWY1ID0gdGhpcy5jaGlsZHNCeU5hbWUobWVkaWFGaWxlc0VsZW1lbnQsIFwiTWVkaWFGaWxlXCIpO1xuICAgICAgICBmb3IgKG4gPSAwLCBsZW41ID0gcmVmNS5sZW5ndGg7IG4gPCBsZW41OyBuKyspIHtcbiAgICAgICAgICBtZWRpYUZpbGVFbGVtZW50ID0gcmVmNVtuXTtcbiAgICAgICAgICBtZWRpYUZpbGUgPSBuZXcgVkFTVE1lZGlhRmlsZSgpO1xuICAgICAgICAgIG1lZGlhRmlsZS5pZCA9IG1lZGlhRmlsZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiaWRcIik7XG4gICAgICAgICAgbWVkaWFGaWxlLmZpbGVVUkwgPSB0aGlzLnBhcnNlTm9kZVRleHQobWVkaWFGaWxlRWxlbWVudCk7XG4gICAgICAgICAgbWVkaWFGaWxlLmRlbGl2ZXJ5VHlwZSA9IG1lZGlhRmlsZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiZGVsaXZlcnlcIik7XG4gICAgICAgICAgbWVkaWFGaWxlLmNvZGVjID0gbWVkaWFGaWxlRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjb2RlY1wiKTtcbiAgICAgICAgICBtZWRpYUZpbGUubWltZVR5cGUgPSBtZWRpYUZpbGVFbGVtZW50LmdldEF0dHJpYnV0ZShcInR5cGVcIik7XG4gICAgICAgICAgbWVkaWFGaWxlLmFwaUZyYW1ld29yayA9IG1lZGlhRmlsZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiYXBpRnJhbWV3b3JrXCIpO1xuICAgICAgICAgIG1lZGlhRmlsZS5iaXRyYXRlID0gcGFyc2VJbnQobWVkaWFGaWxlRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJiaXRyYXRlXCIpIHx8IDApO1xuICAgICAgICAgIG1lZGlhRmlsZS5taW5CaXRyYXRlID0gcGFyc2VJbnQobWVkaWFGaWxlRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtaW5CaXRyYXRlXCIpIHx8IDApO1xuICAgICAgICAgIG1lZGlhRmlsZS5tYXhCaXRyYXRlID0gcGFyc2VJbnQobWVkaWFGaWxlRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtYXhCaXRyYXRlXCIpIHx8IDApO1xuICAgICAgICAgIG1lZGlhRmlsZS53aWR0aCA9IHBhcnNlSW50KG1lZGlhRmlsZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwid2lkdGhcIikgfHwgMCk7XG4gICAgICAgICAgbWVkaWFGaWxlLmhlaWdodCA9IHBhcnNlSW50KG1lZGlhRmlsZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiaGVpZ2h0XCIpIHx8IDApO1xuICAgICAgICAgIHNjYWxhYmxlID0gbWVkaWFGaWxlRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJzY2FsYWJsZVwiKTtcbiAgICAgICAgICBpZiAoc2NhbGFibGUgJiYgdHlwZW9mIHNjYWxhYmxlID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICBzY2FsYWJsZSA9IHNjYWxhYmxlLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICBpZiAoc2NhbGFibGUgPT09IFwidHJ1ZVwiKSB7XG4gICAgICAgICAgICAgIG1lZGlhRmlsZS5zY2FsYWJsZSA9IHRydWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHNjYWxhYmxlID09PSBcImZhbHNlXCIpIHtcbiAgICAgICAgICAgICAgbWVkaWFGaWxlLnNjYWxhYmxlID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIG1haW50YWluQXNwZWN0UmF0aW8gPSBtZWRpYUZpbGVFbGVtZW50LmdldEF0dHJpYnV0ZShcIm1haW50YWluQXNwZWN0UmF0aW9cIik7XG4gICAgICAgICAgaWYgKG1haW50YWluQXNwZWN0UmF0aW8gJiYgdHlwZW9mIG1haW50YWluQXNwZWN0UmF0aW8gPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgIG1haW50YWluQXNwZWN0UmF0aW8gPSBtYWludGFpbkFzcGVjdFJhdGlvLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICBpZiAobWFpbnRhaW5Bc3BlY3RSYXRpbyA9PT0gXCJ0cnVlXCIpIHtcbiAgICAgICAgICAgICAgbWVkaWFGaWxlLm1haW50YWluQXNwZWN0UmF0aW8gPSB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChtYWludGFpbkFzcGVjdFJhdGlvID09PSBcImZhbHNlXCIpIHtcbiAgICAgICAgICAgICAgbWVkaWFGaWxlLm1haW50YWluQXNwZWN0UmF0aW8gPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgY3JlYXRpdmUubWVkaWFGaWxlcy5wdXNoKG1lZGlhRmlsZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGljb25zRWxlbWVudCA9IHRoaXMuY2hpbGRCeU5hbWUoY3JlYXRpdmVFbGVtZW50LCBcIkljb25zXCIpO1xuICAgICAgaWYgKGljb25zRWxlbWVudCAhPSBudWxsKSB7XG4gICAgICAgIHJlZjYgPSB0aGlzLmNoaWxkc0J5TmFtZShpY29uc0VsZW1lbnQsIFwiSWNvblwiKTtcbiAgICAgICAgZm9yIChvID0gMCwgbGVuNiA9IHJlZjYubGVuZ3RoOyBvIDwgbGVuNjsgbysrKSB7XG4gICAgICAgICAgaWNvbkVsZW1lbnQgPSByZWY2W29dO1xuICAgICAgICAgIGljb24gPSBuZXcgVkFTVEljb24oKTtcbiAgICAgICAgICBpY29uLnByb2dyYW0gPSBpY29uRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJwcm9ncmFtXCIpO1xuICAgICAgICAgIGljb24uaGVpZ2h0ID0gcGFyc2VJbnQoaWNvbkVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiaGVpZ2h0XCIpIHx8IDApO1xuICAgICAgICAgIGljb24ud2lkdGggPSBwYXJzZUludChpY29uRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJ3aWR0aFwiKSB8fCAwKTtcbiAgICAgICAgICBpY29uLnhQb3NpdGlvbiA9IHRoaXMucGFyc2VYUG9zaXRpb24oaWNvbkVsZW1lbnQuZ2V0QXR0cmlidXRlKFwieFBvc2l0aW9uXCIpKTtcbiAgICAgICAgICBpY29uLnlQb3NpdGlvbiA9IHRoaXMucGFyc2VZUG9zaXRpb24oaWNvbkVsZW1lbnQuZ2V0QXR0cmlidXRlKFwieVBvc2l0aW9uXCIpKTtcbiAgICAgICAgICBpY29uLmFwaUZyYW1ld29yayA9IGljb25FbGVtZW50LmdldEF0dHJpYnV0ZShcImFwaUZyYW1ld29ya1wiKTtcbiAgICAgICAgICBpY29uLm9mZnNldCA9IHRoaXMucGFyc2VEdXJhdGlvbihpY29uRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJvZmZzZXRcIikpO1xuICAgICAgICAgIGljb24uZHVyYXRpb24gPSB0aGlzLnBhcnNlRHVyYXRpb24oaWNvbkVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiZHVyYXRpb25cIikpO1xuICAgICAgICAgIHJlZjcgPSB0aGlzLmNoaWxkc0J5TmFtZShpY29uRWxlbWVudCwgXCJIVE1MUmVzb3VyY2VcIik7XG4gICAgICAgICAgZm9yIChwID0gMCwgbGVuNyA9IHJlZjcubGVuZ3RoOyBwIDwgbGVuNzsgcCsrKSB7XG4gICAgICAgICAgICBodG1sRWxlbWVudCA9IHJlZjdbcF07XG4gICAgICAgICAgICBpY29uLnR5cGUgPSBodG1sRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjcmVhdGl2ZVR5cGVcIikgfHwgJ3RleHQvaHRtbCc7XG4gICAgICAgICAgICBpY29uLmh0bWxSZXNvdXJjZSA9IHRoaXMucGFyc2VOb2RlVGV4dChodG1sRWxlbWVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJlZjggPSB0aGlzLmNoaWxkc0J5TmFtZShpY29uRWxlbWVudCwgXCJJRnJhbWVSZXNvdXJjZVwiKTtcbiAgICAgICAgICBmb3IgKHEgPSAwLCBsZW44ID0gcmVmOC5sZW5ndGg7IHEgPCBsZW44OyBxKyspIHtcbiAgICAgICAgICAgIGlmcmFtZUVsZW1lbnQgPSByZWY4W3FdO1xuICAgICAgICAgICAgaWNvbi50eXBlID0gaWZyYW1lRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjcmVhdGl2ZVR5cGVcIikgfHwgMDtcbiAgICAgICAgICAgIGljb24uaWZyYW1lUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoaWZyYW1lRWxlbWVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJlZjkgPSB0aGlzLmNoaWxkc0J5TmFtZShpY29uRWxlbWVudCwgXCJTdGF0aWNSZXNvdXJjZVwiKTtcbiAgICAgICAgICBmb3IgKHIgPSAwLCBsZW45ID0gcmVmOS5sZW5ndGg7IHIgPCBsZW45OyByKyspIHtcbiAgICAgICAgICAgIHN0YXRpY0VsZW1lbnQgPSByZWY5W3JdO1xuICAgICAgICAgICAgaWNvbi50eXBlID0gc3RhdGljRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjcmVhdGl2ZVR5cGVcIikgfHwgMDtcbiAgICAgICAgICAgIGljb24uc3RhdGljUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoc3RhdGljRWxlbWVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGljb25DbGlja3NFbGVtZW50ID0gdGhpcy5jaGlsZEJ5TmFtZShpY29uRWxlbWVudCwgXCJJY29uQ2xpY2tzXCIpO1xuICAgICAgICAgIGlmIChpY29uQ2xpY2tzRWxlbWVudCAhPSBudWxsKSB7XG4gICAgICAgICAgICBpY29uLmljb25DbGlja1Rocm91Z2hVUkxUZW1wbGF0ZSA9IHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKGljb25DbGlja3NFbGVtZW50LCBcIkljb25DbGlja1Rocm91Z2hcIikpO1xuICAgICAgICAgICAgcmVmMTAgPSB0aGlzLmNoaWxkc0J5TmFtZShpY29uQ2xpY2tzRWxlbWVudCwgXCJJY29uQ2xpY2tUcmFja2luZ1wiKTtcbiAgICAgICAgICAgIGZvciAocyA9IDAsIGxlbjEwID0gcmVmMTAubGVuZ3RoOyBzIDwgbGVuMTA7IHMrKykge1xuICAgICAgICAgICAgICBpY29uQ2xpY2tUcmFja2luZ0VsZW1lbnQgPSByZWYxMFtzXTtcbiAgICAgICAgICAgICAgaWNvbi5pY29uQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcy5wdXNoKHRoaXMucGFyc2VOb2RlVGV4dChpY29uQ2xpY2tUcmFja2luZ0VsZW1lbnQpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgaWNvbi5pY29uVmlld1RyYWNraW5nVVJMVGVtcGxhdGUgPSB0aGlzLnBhcnNlTm9kZVRleHQodGhpcy5jaGlsZEJ5TmFtZShpY29uRWxlbWVudCwgXCJJY29uVmlld1RyYWNraW5nXCIpKTtcbiAgICAgICAgICBjcmVhdGl2ZS5pY29ucy5wdXNoKGljb24pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gY3JlYXRpdmU7XG4gICAgfTtcblxuICAgIFZBU1RQYXJzZXIucGFyc2VOb25MaW5lYXIgPSBmdW5jdGlvbihjcmVhdGl2ZUVsZW1lbnQpIHtcbiAgICAgIHZhciBiYXNlLCBjcmVhdGl2ZSwgZXZlbnROYW1lLCBodG1sRWxlbWVudCwgaSwgaWZyYW1lRWxlbWVudCwgaiwgaywgbCwgbGVuLCBsZW4xLCBsZW4yLCBsZW4zLCBsZW40LCBsZW41LCBtLCBuLCBub25saW5lYXJBZCwgbm9ubGluZWFyUmVzb3VyY2UsIHJlZiwgcmVmMSwgcmVmMiwgcmVmMywgcmVmNCwgcmVmNSwgc3RhdGljRWxlbWVudCwgdHJhY2tpbmdFbGVtZW50LCB0cmFja2luZ0V2ZW50c0VsZW1lbnQsIHRyYWNraW5nVVJMVGVtcGxhdGU7XG4gICAgICBjcmVhdGl2ZSA9IG5ldyBWQVNUQ3JlYXRpdmVOb25MaW5lYXIoKTtcbiAgICAgIHJlZiA9IHRoaXMuY2hpbGRzQnlOYW1lKGNyZWF0aXZlRWxlbWVudCwgXCJUcmFja2luZ0V2ZW50c1wiKTtcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICB0cmFja2luZ0V2ZW50c0VsZW1lbnQgPSByZWZbaV07XG4gICAgICAgIHJlZjEgPSB0aGlzLmNoaWxkc0J5TmFtZSh0cmFja2luZ0V2ZW50c0VsZW1lbnQsIFwiVHJhY2tpbmdcIik7XG4gICAgICAgIGZvciAoaiA9IDAsIGxlbjEgPSByZWYxLmxlbmd0aDsgaiA8IGxlbjE7IGorKykge1xuICAgICAgICAgIHRyYWNraW5nRWxlbWVudCA9IHJlZjFbal07XG4gICAgICAgICAgZXZlbnROYW1lID0gdHJhY2tpbmdFbGVtZW50LmdldEF0dHJpYnV0ZShcImV2ZW50XCIpO1xuICAgICAgICAgIHRyYWNraW5nVVJMVGVtcGxhdGUgPSB0aGlzLnBhcnNlTm9kZVRleHQodHJhY2tpbmdFbGVtZW50KTtcbiAgICAgICAgICBpZiAoKGV2ZW50TmFtZSAhPSBudWxsKSAmJiAodHJhY2tpbmdVUkxUZW1wbGF0ZSAhPSBudWxsKSkge1xuICAgICAgICAgICAgaWYgKChiYXNlID0gY3JlYXRpdmUudHJhY2tpbmdFdmVudHMpW2V2ZW50TmFtZV0gPT0gbnVsbCkge1xuICAgICAgICAgICAgICBiYXNlW2V2ZW50TmFtZV0gPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNyZWF0aXZlLnRyYWNraW5nRXZlbnRzW2V2ZW50TmFtZV0ucHVzaCh0cmFja2luZ1VSTFRlbXBsYXRlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJlZjIgPSB0aGlzLmNoaWxkc0J5TmFtZShjcmVhdGl2ZUVsZW1lbnQsIFwiTm9uTGluZWFyXCIpO1xuICAgICAgZm9yIChrID0gMCwgbGVuMiA9IHJlZjIubGVuZ3RoOyBrIDwgbGVuMjsgaysrKSB7XG4gICAgICAgIG5vbmxpbmVhclJlc291cmNlID0gcmVmMltrXTtcbiAgICAgICAgbm9ubGluZWFyQWQgPSBuZXcgVkFTVE5vbkxpbmVhcigpO1xuICAgICAgICBub25saW5lYXJBZC5pZCA9IG5vbmxpbmVhclJlc291cmNlLmdldEF0dHJpYnV0ZShcImlkXCIpIHx8IG51bGw7XG4gICAgICAgIG5vbmxpbmVhckFkLndpZHRoID0gbm9ubGluZWFyUmVzb3VyY2UuZ2V0QXR0cmlidXRlKFwid2lkdGhcIik7XG4gICAgICAgIG5vbmxpbmVhckFkLmhlaWdodCA9IG5vbmxpbmVhclJlc291cmNlLmdldEF0dHJpYnV0ZShcImhlaWdodFwiKTtcbiAgICAgICAgbm9ubGluZWFyQWQubWluU3VnZ2VzdGVkRHVyYXRpb24gPSBub25saW5lYXJSZXNvdXJjZS5nZXRBdHRyaWJ1dGUoXCJtaW5TdWdnZXN0ZWREdXJhdGlvblwiKTtcbiAgICAgICAgbm9ubGluZWFyQWQuYXBpRnJhbWV3b3JrID0gbm9ubGluZWFyUmVzb3VyY2UuZ2V0QXR0cmlidXRlKFwiYXBpRnJhbWV3b3JrXCIpO1xuICAgICAgICByZWYzID0gdGhpcy5jaGlsZHNCeU5hbWUobm9ubGluZWFyUmVzb3VyY2UsIFwiSFRNTFJlc291cmNlXCIpO1xuICAgICAgICBmb3IgKGwgPSAwLCBsZW4zID0gcmVmMy5sZW5ndGg7IGwgPCBsZW4zOyBsKyspIHtcbiAgICAgICAgICBodG1sRWxlbWVudCA9IHJlZjNbbF07XG4gICAgICAgICAgbm9ubGluZWFyQWQudHlwZSA9IGh0bWxFbGVtZW50LmdldEF0dHJpYnV0ZShcImNyZWF0aXZlVHlwZVwiKSB8fCAndGV4dC9odG1sJztcbiAgICAgICAgICBub25saW5lYXJBZC5odG1sUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoaHRtbEVsZW1lbnQpO1xuICAgICAgICB9XG4gICAgICAgIHJlZjQgPSB0aGlzLmNoaWxkc0J5TmFtZShub25saW5lYXJSZXNvdXJjZSwgXCJJRnJhbWVSZXNvdXJjZVwiKTtcbiAgICAgICAgZm9yIChtID0gMCwgbGVuNCA9IHJlZjQubGVuZ3RoOyBtIDwgbGVuNDsgbSsrKSB7XG4gICAgICAgICAgaWZyYW1lRWxlbWVudCA9IHJlZjRbbV07XG4gICAgICAgICAgbm9ubGluZWFyQWQudHlwZSA9IGlmcmFtZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiY3JlYXRpdmVUeXBlXCIpIHx8IDA7XG4gICAgICAgICAgbm9ubGluZWFyQWQuaWZyYW1lUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoaWZyYW1lRWxlbWVudCk7XG4gICAgICAgIH1cbiAgICAgICAgcmVmNSA9IHRoaXMuY2hpbGRzQnlOYW1lKG5vbmxpbmVhclJlc291cmNlLCBcIlN0YXRpY1Jlc291cmNlXCIpO1xuICAgICAgICBmb3IgKG4gPSAwLCBsZW41ID0gcmVmNS5sZW5ndGg7IG4gPCBsZW41OyBuKyspIHtcbiAgICAgICAgICBzdGF0aWNFbGVtZW50ID0gcmVmNVtuXTtcbiAgICAgICAgICBub25saW5lYXJBZC50eXBlID0gc3RhdGljRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjcmVhdGl2ZVR5cGVcIikgfHwgMDtcbiAgICAgICAgICBub25saW5lYXJBZC5zdGF0aWNSZXNvdXJjZSA9IHRoaXMucGFyc2VOb2RlVGV4dChzdGF0aWNFbGVtZW50KTtcbiAgICAgICAgfVxuICAgICAgICBub25saW5lYXJBZC5ub25saW5lYXJDbGlja1Rocm91Z2hVUkxUZW1wbGF0ZSA9IHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKG5vbmxpbmVhclJlc291cmNlLCBcIk5vbkxpbmVhckNsaWNrVGhyb3VnaFwiKSk7XG4gICAgICAgIGNyZWF0aXZlLnZhcmlhdGlvbnMucHVzaChub25saW5lYXJBZCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gY3JlYXRpdmU7XG4gICAgfTtcblxuICAgIFZBU1RQYXJzZXIucGFyc2VDb21wYW5pb25BZCA9IGZ1bmN0aW9uKGNyZWF0aXZlRWxlbWVudCkge1xuICAgICAgdmFyIGJhc2UsIGNsaWNrVHJhY2tpbmdFbGVtZW50LCBjb21wYW5pb25BZCwgY29tcGFuaW9uUmVzb3VyY2UsIGNyZWF0aXZlLCBldmVudE5hbWUsIGh0bWxFbGVtZW50LCBpLCBpZnJhbWVFbGVtZW50LCBqLCBrLCBsLCBsZW4sIGxlbjEsIGxlbjIsIGxlbjMsIGxlbjQsIGxlbjUsIGxlbjYsIG0sIG4sIG8sIHJlZiwgcmVmMSwgcmVmMiwgcmVmMywgcmVmNCwgcmVmNSwgcmVmNiwgc3RhdGljRWxlbWVudCwgdHJhY2tpbmdFbGVtZW50LCB0cmFja2luZ0V2ZW50c0VsZW1lbnQsIHRyYWNraW5nVVJMVGVtcGxhdGU7XG4gICAgICBjcmVhdGl2ZSA9IG5ldyBWQVNUQ3JlYXRpdmVDb21wYW5pb24oKTtcbiAgICAgIHJlZiA9IHRoaXMuY2hpbGRzQnlOYW1lKGNyZWF0aXZlRWxlbWVudCwgXCJDb21wYW5pb25cIik7XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSByZWYubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgY29tcGFuaW9uUmVzb3VyY2UgPSByZWZbaV07XG4gICAgICAgIGNvbXBhbmlvbkFkID0gbmV3IFZBU1RDb21wYW5pb25BZCgpO1xuICAgICAgICBjb21wYW5pb25BZC5pZCA9IGNvbXBhbmlvblJlc291cmNlLmdldEF0dHJpYnV0ZShcImlkXCIpIHx8IG51bGw7XG4gICAgICAgIGNvbXBhbmlvbkFkLndpZHRoID0gY29tcGFuaW9uUmVzb3VyY2UuZ2V0QXR0cmlidXRlKFwid2lkdGhcIik7XG4gICAgICAgIGNvbXBhbmlvbkFkLmhlaWdodCA9IGNvbXBhbmlvblJlc291cmNlLmdldEF0dHJpYnV0ZShcImhlaWdodFwiKTtcbiAgICAgICAgY29tcGFuaW9uQWQuY29tcGFuaW9uQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcyA9IFtdO1xuICAgICAgICByZWYxID0gdGhpcy5jaGlsZHNCeU5hbWUoY29tcGFuaW9uUmVzb3VyY2UsIFwiSFRNTFJlc291cmNlXCIpO1xuICAgICAgICBmb3IgKGogPSAwLCBsZW4xID0gcmVmMS5sZW5ndGg7IGogPCBsZW4xOyBqKyspIHtcbiAgICAgICAgICBodG1sRWxlbWVudCA9IHJlZjFbal07XG4gICAgICAgICAgY29tcGFuaW9uQWQudHlwZSA9IGh0bWxFbGVtZW50LmdldEF0dHJpYnV0ZShcImNyZWF0aXZlVHlwZVwiKSB8fCAndGV4dC9odG1sJztcbiAgICAgICAgICBjb21wYW5pb25BZC5odG1sUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoaHRtbEVsZW1lbnQpO1xuICAgICAgICB9XG4gICAgICAgIHJlZjIgPSB0aGlzLmNoaWxkc0J5TmFtZShjb21wYW5pb25SZXNvdXJjZSwgXCJJRnJhbWVSZXNvdXJjZVwiKTtcbiAgICAgICAgZm9yIChrID0gMCwgbGVuMiA9IHJlZjIubGVuZ3RoOyBrIDwgbGVuMjsgaysrKSB7XG4gICAgICAgICAgaWZyYW1lRWxlbWVudCA9IHJlZjJba107XG4gICAgICAgICAgY29tcGFuaW9uQWQudHlwZSA9IGlmcmFtZUVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiY3JlYXRpdmVUeXBlXCIpIHx8IDA7XG4gICAgICAgICAgY29tcGFuaW9uQWQuaWZyYW1lUmVzb3VyY2UgPSB0aGlzLnBhcnNlTm9kZVRleHQoaWZyYW1lRWxlbWVudCk7XG4gICAgICAgIH1cbiAgICAgICAgcmVmMyA9IHRoaXMuY2hpbGRzQnlOYW1lKGNvbXBhbmlvblJlc291cmNlLCBcIlN0YXRpY1Jlc291cmNlXCIpO1xuICAgICAgICBmb3IgKGwgPSAwLCBsZW4zID0gcmVmMy5sZW5ndGg7IGwgPCBsZW4zOyBsKyspIHtcbiAgICAgICAgICBzdGF0aWNFbGVtZW50ID0gcmVmM1tsXTtcbiAgICAgICAgICBjb21wYW5pb25BZC50eXBlID0gc3RhdGljRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJjcmVhdGl2ZVR5cGVcIikgfHwgMDtcbiAgICAgICAgICBjb21wYW5pb25BZC5zdGF0aWNSZXNvdXJjZSA9IHRoaXMucGFyc2VOb2RlVGV4dChzdGF0aWNFbGVtZW50KTtcbiAgICAgICAgfVxuICAgICAgICByZWY0ID0gdGhpcy5jaGlsZHNCeU5hbWUoY29tcGFuaW9uUmVzb3VyY2UsIFwiVHJhY2tpbmdFdmVudHNcIik7XG4gICAgICAgIGZvciAobSA9IDAsIGxlbjQgPSByZWY0Lmxlbmd0aDsgbSA8IGxlbjQ7IG0rKykge1xuICAgICAgICAgIHRyYWNraW5nRXZlbnRzRWxlbWVudCA9IHJlZjRbbV07XG4gICAgICAgICAgcmVmNSA9IHRoaXMuY2hpbGRzQnlOYW1lKHRyYWNraW5nRXZlbnRzRWxlbWVudCwgXCJUcmFja2luZ1wiKTtcbiAgICAgICAgICBmb3IgKG4gPSAwLCBsZW41ID0gcmVmNS5sZW5ndGg7IG4gPCBsZW41OyBuKyspIHtcbiAgICAgICAgICAgIHRyYWNraW5nRWxlbWVudCA9IHJlZjVbbl07XG4gICAgICAgICAgICBldmVudE5hbWUgPSB0cmFja2luZ0VsZW1lbnQuZ2V0QXR0cmlidXRlKFwiZXZlbnRcIik7XG4gICAgICAgICAgICB0cmFja2luZ1VSTFRlbXBsYXRlID0gdGhpcy5wYXJzZU5vZGVUZXh0KHRyYWNraW5nRWxlbWVudCk7XG4gICAgICAgICAgICBpZiAoKGV2ZW50TmFtZSAhPSBudWxsKSAmJiAodHJhY2tpbmdVUkxUZW1wbGF0ZSAhPSBudWxsKSkge1xuICAgICAgICAgICAgICBpZiAoKGJhc2UgPSBjb21wYW5pb25BZC50cmFja2luZ0V2ZW50cylbZXZlbnROYW1lXSA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgYmFzZVtldmVudE5hbWVdID0gW107XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29tcGFuaW9uQWQudHJhY2tpbmdFdmVudHNbZXZlbnROYW1lXS5wdXNoKHRyYWNraW5nVVJMVGVtcGxhdGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZWY2ID0gdGhpcy5jaGlsZHNCeU5hbWUoY29tcGFuaW9uUmVzb3VyY2UsIFwiQ29tcGFuaW9uQ2xpY2tUcmFja2luZ1wiKTtcbiAgICAgICAgZm9yIChvID0gMCwgbGVuNiA9IHJlZjYubGVuZ3RoOyBvIDwgbGVuNjsgbysrKSB7XG4gICAgICAgICAgY2xpY2tUcmFja2luZ0VsZW1lbnQgPSByZWY2W29dO1xuICAgICAgICAgIGNvbXBhbmlvbkFkLmNvbXBhbmlvbkNsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMucHVzaCh0aGlzLnBhcnNlTm9kZVRleHQoY2xpY2tUcmFja2luZ0VsZW1lbnQpKTtcbiAgICAgICAgfVxuICAgICAgICBjb21wYW5pb25BZC5jb21wYW5pb25DbGlja1Rocm91Z2hVUkxUZW1wbGF0ZSA9IHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKGNvbXBhbmlvblJlc291cmNlLCBcIkNvbXBhbmlvbkNsaWNrVGhyb3VnaFwiKSk7XG4gICAgICAgIGNvbXBhbmlvbkFkLmNvbXBhbmlvbkNsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZSA9IHRoaXMucGFyc2VOb2RlVGV4dCh0aGlzLmNoaWxkQnlOYW1lKGNvbXBhbmlvblJlc291cmNlLCBcIkNvbXBhbmlvbkNsaWNrVHJhY2tpbmdcIikpO1xuICAgICAgICBjcmVhdGl2ZS52YXJpYXRpb25zLnB1c2goY29tcGFuaW9uQWQpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNyZWF0aXZlO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLnBhcnNlRHVyYXRpb24gPSBmdW5jdGlvbihkdXJhdGlvblN0cmluZykge1xuICAgICAgdmFyIGR1cmF0aW9uQ29tcG9uZW50cywgaG91cnMsIG1pbnV0ZXMsIHNlY29uZHMsIHNlY29uZHNBbmRNUztcbiAgICAgIGlmICghKGR1cmF0aW9uU3RyaW5nICE9IG51bGwpKSB7XG4gICAgICAgIHJldHVybiAtMTtcbiAgICAgIH1cbiAgICAgIGR1cmF0aW9uQ29tcG9uZW50cyA9IGR1cmF0aW9uU3RyaW5nLnNwbGl0KFwiOlwiKTtcbiAgICAgIGlmIChkdXJhdGlvbkNvbXBvbmVudHMubGVuZ3RoICE9PSAzKSB7XG4gICAgICAgIHJldHVybiAtMTtcbiAgICAgIH1cbiAgICAgIHNlY29uZHNBbmRNUyA9IGR1cmF0aW9uQ29tcG9uZW50c1syXS5zcGxpdChcIi5cIik7XG4gICAgICBzZWNvbmRzID0gcGFyc2VJbnQoc2Vjb25kc0FuZE1TWzBdKTtcbiAgICAgIGlmIChzZWNvbmRzQW5kTVMubGVuZ3RoID09PSAyKSB7XG4gICAgICAgIHNlY29uZHMgKz0gcGFyc2VGbG9hdChcIjAuXCIgKyBzZWNvbmRzQW5kTVNbMV0pO1xuICAgICAgfVxuICAgICAgbWludXRlcyA9IHBhcnNlSW50KGR1cmF0aW9uQ29tcG9uZW50c1sxXSAqIDYwKTtcbiAgICAgIGhvdXJzID0gcGFyc2VJbnQoZHVyYXRpb25Db21wb25lbnRzWzBdICogNjAgKiA2MCk7XG4gICAgICBpZiAoaXNOYU4oaG91cnMgfHwgaXNOYU4obWludXRlcyB8fCBpc05hTihzZWNvbmRzIHx8IG1pbnV0ZXMgPiA2MCAqIDYwIHx8IHNlY29uZHMgPiA2MCkpKSkge1xuICAgICAgICByZXR1cm4gLTE7XG4gICAgICB9XG4gICAgICByZXR1cm4gaG91cnMgKyBtaW51dGVzICsgc2Vjb25kcztcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5wYXJzZVhQb3NpdGlvbiA9IGZ1bmN0aW9uKHhQb3NpdGlvbikge1xuICAgICAgaWYgKHhQb3NpdGlvbiA9PT0gXCJsZWZ0XCIgfHwgeFBvc2l0aW9uID09PSBcInJpZ2h0XCIpIHtcbiAgICAgICAgcmV0dXJuIHhQb3NpdGlvbjtcbiAgICAgIH1cbiAgICAgIHJldHVybiBwYXJzZUludCh4UG9zaXRpb24gfHwgMCk7XG4gICAgfTtcblxuICAgIFZBU1RQYXJzZXIucGFyc2VZUG9zaXRpb24gPSBmdW5jdGlvbih5UG9zaXRpb24pIHtcbiAgICAgIGlmICh5UG9zaXRpb24gPT09IFwidG9wXCIgfHwgeVBvc2l0aW9uID09PSBcImJvdHRvbVwiKSB7XG4gICAgICAgIHJldHVybiB5UG9zaXRpb247XG4gICAgICB9XG4gICAgICByZXR1cm4gcGFyc2VJbnQoeVBvc2l0aW9uIHx8IDApO1xuICAgIH07XG5cbiAgICBWQVNUUGFyc2VyLnBhcnNlTm9kZVRleHQgPSBmdW5jdGlvbihub2RlKSB7XG4gICAgICByZXR1cm4gbm9kZSAmJiAobm9kZS50ZXh0Q29udGVudCB8fCBub2RlLnRleHQgfHwgJycpLnRyaW0oKTtcbiAgICB9O1xuXG4gICAgVkFTVFBhcnNlci5jb3B5Tm9kZUF0dHJpYnV0ZSA9IGZ1bmN0aW9uKGF0dHJpYnV0ZU5hbWUsIG5vZGVTb3VyY2UsIG5vZGVEZXN0aW5hdGlvbikge1xuICAgICAgdmFyIGF0dHJpYnV0ZVZhbHVlO1xuICAgICAgYXR0cmlidXRlVmFsdWUgPSBub2RlU291cmNlLmdldEF0dHJpYnV0ZShhdHRyaWJ1dGVOYW1lKTtcbiAgICAgIGlmIChhdHRyaWJ1dGVWYWx1ZSkge1xuICAgICAgICByZXR1cm4gbm9kZURlc3RpbmF0aW9uLnNldEF0dHJpYnV0ZShhdHRyaWJ1dGVOYW1lLCBhdHRyaWJ1dGVWYWx1ZSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIHJldHVybiBWQVNUUGFyc2VyO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUUGFyc2VyO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIFZBU1RSZXNwb25zZTtcblxuICBWQVNUUmVzcG9uc2UgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gVkFTVFJlc3BvbnNlKCkge1xuICAgICAgdGhpcy5hZHMgPSBbXTtcbiAgICAgIHRoaXMuZXJyb3JVUkxUZW1wbGF0ZXMgPSBbXTtcbiAgICB9XG5cbiAgICByZXR1cm4gVkFTVFJlc3BvbnNlO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBWQVNUUmVzcG9uc2U7XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuMTEuMVxuKGZ1bmN0aW9uKCkge1xuICB2YXIgRXZlbnRFbWl0dGVyLCBWQVNUQ2xpZW50LCBWQVNUQ3JlYXRpdmVMaW5lYXIsIFZBU1RUcmFja2VyLCBWQVNUVXRpbCxcbiAgICBleHRlbmQgPSBmdW5jdGlvbihjaGlsZCwgcGFyZW50KSB7IGZvciAodmFyIGtleSBpbiBwYXJlbnQpIHsgaWYgKGhhc1Byb3AuY2FsbChwYXJlbnQsIGtleSkpIGNoaWxkW2tleV0gPSBwYXJlbnRba2V5XTsgfSBmdW5jdGlvbiBjdG9yKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gY2hpbGQ7IH0gY3Rvci5wcm90b3R5cGUgPSBwYXJlbnQucHJvdG90eXBlOyBjaGlsZC5wcm90b3R5cGUgPSBuZXcgY3RvcigpOyBjaGlsZC5fX3N1cGVyX18gPSBwYXJlbnQucHJvdG90eXBlOyByZXR1cm4gY2hpbGQ7IH0sXG4gICAgaGFzUHJvcCA9IHt9Lmhhc093blByb3BlcnR5O1xuXG4gIFZBU1RDbGllbnQgPSByZXF1aXJlKCcuL2NsaWVudCcpO1xuXG4gIFZBU1RVdGlsID0gcmVxdWlyZSgnLi91dGlsJyk7XG5cbiAgVkFTVENyZWF0aXZlTGluZWFyID0gcmVxdWlyZSgnLi9jcmVhdGl2ZScpLlZBU1RDcmVhdGl2ZUxpbmVhcjtcblxuICBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXI7XG5cbiAgVkFTVFRyYWNrZXIgPSAoZnVuY3Rpb24oc3VwZXJDbGFzcykge1xuICAgIGV4dGVuZChWQVNUVHJhY2tlciwgc3VwZXJDbGFzcyk7XG5cbiAgICBmdW5jdGlvbiBWQVNUVHJhY2tlcihhZCwgY3JlYXRpdmUpIHtcbiAgICAgIHZhciBldmVudE5hbWUsIGV2ZW50cywgcmVmO1xuICAgICAgdGhpcy5hZCA9IGFkO1xuICAgICAgdGhpcy5jcmVhdGl2ZSA9IGNyZWF0aXZlO1xuICAgICAgdGhpcy5tdXRlZCA9IGZhbHNlO1xuICAgICAgdGhpcy5pbXByZXNzZWQgPSBmYWxzZTtcbiAgICAgIHRoaXMuc2tpcGFibGUgPSBmYWxzZTtcbiAgICAgIHRoaXMuc2tpcERlbGF5RGVmYXVsdCA9IC0xO1xuICAgICAgdGhpcy50cmFja2luZ0V2ZW50cyA9IHt9O1xuICAgICAgdGhpcy5lbWl0QWx3YXlzRXZlbnRzID0gWydjcmVhdGl2ZVZpZXcnLCAnc3RhcnQnLCAnZmlyc3RRdWFydGlsZScsICdtaWRwb2ludCcsICd0aGlyZFF1YXJ0aWxlJywgJ2NvbXBsZXRlJywgJ3Jlc3VtZScsICdwYXVzZScsICdyZXdpbmQnLCAnc2tpcCcsICdjbG9zZUxpbmVhcicsICdjbG9zZSddO1xuICAgICAgcmVmID0gdGhpcy5jcmVhdGl2ZS50cmFja2luZ0V2ZW50cztcbiAgICAgIGZvciAoZXZlbnROYW1lIGluIHJlZikge1xuICAgICAgICBldmVudHMgPSByZWZbZXZlbnROYW1lXTtcbiAgICAgICAgdGhpcy50cmFja2luZ0V2ZW50c1tldmVudE5hbWVdID0gZXZlbnRzLnNsaWNlKDApO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMuY3JlYXRpdmUgaW5zdGFuY2VvZiBWQVNUQ3JlYXRpdmVMaW5lYXIpIHtcbiAgICAgICAgdGhpcy5zZXREdXJhdGlvbih0aGlzLmNyZWF0aXZlLmR1cmF0aW9uKTtcbiAgICAgICAgdGhpcy5za2lwRGVsYXkgPSB0aGlzLmNyZWF0aXZlLnNraXBEZWxheTtcbiAgICAgICAgdGhpcy5saW5lYXIgPSB0cnVlO1xuICAgICAgICB0aGlzLmNsaWNrVGhyb3VnaFVSTFRlbXBsYXRlID0gdGhpcy5jcmVhdGl2ZS52aWRlb0NsaWNrVGhyb3VnaFVSTFRlbXBsYXRlO1xuICAgICAgICB0aGlzLmNsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMgPSB0aGlzLmNyZWF0aXZlLnZpZGVvQ2xpY2tUcmFja2luZ1VSTFRlbXBsYXRlcztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2tpcERlbGF5ID0gLTE7XG4gICAgICAgIHRoaXMubGluZWFyID0gZmFsc2U7XG4gICAgICB9XG4gICAgICB0aGlzLm9uKCdzdGFydCcsIGZ1bmN0aW9uKCkge1xuICAgICAgICBWQVNUQ2xpZW50Lmxhc3RTdWNjZXNzZnVsbEFkID0gK25ldyBEYXRlKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBWQVNUVHJhY2tlci5wcm90b3R5cGUuc2V0RHVyYXRpb24gPSBmdW5jdGlvbihkdXJhdGlvbikge1xuICAgICAgdGhpcy5hc3NldER1cmF0aW9uID0gZHVyYXRpb247XG4gICAgICByZXR1cm4gdGhpcy5xdWFydGlsZXMgPSB7XG4gICAgICAgICdmaXJzdFF1YXJ0aWxlJzogTWF0aC5yb3VuZCgyNSAqIHRoaXMuYXNzZXREdXJhdGlvbikgLyAxMDAsXG4gICAgICAgICdtaWRwb2ludCc6IE1hdGgucm91bmQoNTAgKiB0aGlzLmFzc2V0RHVyYXRpb24pIC8gMTAwLFxuICAgICAgICAndGhpcmRRdWFydGlsZSc6IE1hdGgucm91bmQoNzUgKiB0aGlzLmFzc2V0RHVyYXRpb24pIC8gMTAwXG4gICAgICB9O1xuICAgIH07XG5cbiAgICBWQVNUVHJhY2tlci5wcm90b3R5cGUuc2V0UHJvZ3Jlc3MgPSBmdW5jdGlvbihwcm9ncmVzcykge1xuICAgICAgdmFyIGV2ZW50TmFtZSwgZXZlbnRzLCBpLCBsZW4sIHBlcmNlbnQsIHF1YXJ0aWxlLCByZWYsIHNraXBEZWxheSwgdGltZTtcbiAgICAgIHNraXBEZWxheSA9IHRoaXMuc2tpcERlbGF5ID09PSBudWxsID8gdGhpcy5za2lwRGVsYXlEZWZhdWx0IDogdGhpcy5za2lwRGVsYXk7XG4gICAgICBpZiAoc2tpcERlbGF5ICE9PSAtMSAmJiAhdGhpcy5za2lwYWJsZSkge1xuICAgICAgICBpZiAoc2tpcERlbGF5ID4gcHJvZ3Jlc3MpIHtcbiAgICAgICAgICB0aGlzLmVtaXQoJ3NraXAtY291bnRkb3duJywgc2tpcERlbGF5IC0gcHJvZ3Jlc3MpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2tpcGFibGUgPSB0cnVlO1xuICAgICAgICAgIHRoaXMuZW1pdCgnc2tpcC1jb3VudGRvd24nLCAwKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKHRoaXMubGluZWFyICYmIHRoaXMuYXNzZXREdXJhdGlvbiA+IDApIHtcbiAgICAgICAgZXZlbnRzID0gW107XG4gICAgICAgIGlmIChwcm9ncmVzcyA+IDApIHtcbiAgICAgICAgICBldmVudHMucHVzaChcInN0YXJ0XCIpO1xuICAgICAgICAgIHBlcmNlbnQgPSBNYXRoLnJvdW5kKHByb2dyZXNzIC8gdGhpcy5hc3NldER1cmF0aW9uICogMTAwKTtcbiAgICAgICAgICBldmVudHMucHVzaChcInByb2dyZXNzLVwiICsgcGVyY2VudCArIFwiJVwiKTtcbiAgICAgICAgICBldmVudHMucHVzaChcInByb2dyZXNzLVwiICsgKE1hdGgucm91bmQocHJvZ3Jlc3MpKSk7XG4gICAgICAgICAgcmVmID0gdGhpcy5xdWFydGlsZXM7XG4gICAgICAgICAgZm9yIChxdWFydGlsZSBpbiByZWYpIHtcbiAgICAgICAgICAgIHRpbWUgPSByZWZbcXVhcnRpbGVdO1xuICAgICAgICAgICAgaWYgKCh0aW1lIDw9IHByb2dyZXNzICYmIHByb2dyZXNzIDw9ICh0aW1lICsgMSkpKSB7XG4gICAgICAgICAgICAgIGV2ZW50cy5wdXNoKHF1YXJ0aWxlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZm9yIChpID0gMCwgbGVuID0gZXZlbnRzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgZXZlbnROYW1lID0gZXZlbnRzW2ldO1xuICAgICAgICAgIHRoaXMudHJhY2soZXZlbnROYW1lLCB0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocHJvZ3Jlc3MgPCB0aGlzLnByb2dyZXNzKSB7XG4gICAgICAgICAgdGhpcy50cmFjayhcInJld2luZFwiKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMucHJvZ3Jlc3MgPSBwcm9ncmVzcztcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLnNldE11dGVkID0gZnVuY3Rpb24obXV0ZWQpIHtcbiAgICAgIGlmICh0aGlzLm11dGVkICE9PSBtdXRlZCkge1xuICAgICAgICB0aGlzLnRyYWNrKG11dGVkID8gXCJtdXRlXCIgOiBcInVubXV0ZVwiKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLm11dGVkID0gbXV0ZWQ7XG4gICAgfTtcblxuICAgIFZBU1RUcmFja2VyLnByb3RvdHlwZS5zZXRQYXVzZWQgPSBmdW5jdGlvbihwYXVzZWQpIHtcbiAgICAgIGlmICh0aGlzLnBhdXNlZCAhPT0gcGF1c2VkKSB7XG4gICAgICAgIHRoaXMudHJhY2socGF1c2VkID8gXCJwYXVzZVwiIDogXCJyZXN1bWVcIik7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5wYXVzZWQgPSBwYXVzZWQ7XG4gICAgfTtcblxuICAgIFZBU1RUcmFja2VyLnByb3RvdHlwZS5zZXRGdWxsc2NyZWVuID0gZnVuY3Rpb24oZnVsbHNjcmVlbikge1xuICAgICAgaWYgKHRoaXMuZnVsbHNjcmVlbiAhPT0gZnVsbHNjcmVlbikge1xuICAgICAgICB0aGlzLnRyYWNrKGZ1bGxzY3JlZW4gPyBcImZ1bGxzY3JlZW5cIiA6IFwiZXhpdEZ1bGxzY3JlZW5cIik7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5mdWxsc2NyZWVuID0gZnVsbHNjcmVlbjtcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLnNldFNraXBEZWxheSA9IGZ1bmN0aW9uKGR1cmF0aW9uKSB7XG4gICAgICBpZiAodHlwZW9mIGR1cmF0aW9uID09PSAnbnVtYmVyJykge1xuICAgICAgICByZXR1cm4gdGhpcy5za2lwRGVsYXkgPSBkdXJhdGlvbjtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICghdGhpcy5pbXByZXNzZWQpIHtcbiAgICAgICAgdGhpcy5pbXByZXNzZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnRyYWNrVVJMcyh0aGlzLmFkLmltcHJlc3Npb25VUkxUZW1wbGF0ZXMpO1xuICAgICAgICByZXR1cm4gdGhpcy50cmFjayhcImNyZWF0aXZlVmlld1wiKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLmVycm9yV2l0aENvZGUgPSBmdW5jdGlvbihlcnJvckNvZGUpIHtcbiAgICAgIHJldHVybiB0aGlzLnRyYWNrVVJMcyh0aGlzLmFkLmVycm9yVVJMVGVtcGxhdGVzLCB7XG4gICAgICAgIEVSUk9SQ09ERTogZXJyb3JDb2RlXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLmNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy50cmFjayhcImNvbXBsZXRlXCIpO1xuICAgIH07XG5cbiAgICBWQVNUVHJhY2tlci5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLnRyYWNrKHRoaXMubGluZWFyID8gXCJjbG9zZUxpbmVhclwiIDogXCJjbG9zZVwiKTtcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLnN0b3AgPSBmdW5jdGlvbigpIHt9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLnNraXAgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMudHJhY2soXCJza2lwXCIpO1xuICAgICAgcmV0dXJuIHRoaXMudHJhY2tpbmdFdmVudHMgPSBbXTtcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLmNsaWNrID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgY2xpY2tUaHJvdWdoVVJMLCByZWYsIHZhcmlhYmxlcztcbiAgICAgIGlmICgocmVmID0gdGhpcy5jbGlja1RyYWNraW5nVVJMVGVtcGxhdGVzKSAhPSBudWxsID8gcmVmLmxlbmd0aCA6IHZvaWQgMCkge1xuICAgICAgICB0aGlzLnRyYWNrVVJMcyh0aGlzLmNsaWNrVHJhY2tpbmdVUkxUZW1wbGF0ZXMpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMuY2xpY2tUaHJvdWdoVVJMVGVtcGxhdGUgIT0gbnVsbCkge1xuICAgICAgICBpZiAodGhpcy5saW5lYXIpIHtcbiAgICAgICAgICB2YXJpYWJsZXMgPSB7XG4gICAgICAgICAgICBDT05URU5UUExBWUhFQUQ6IHRoaXMucHJvZ3Jlc3NGb3JtYXRlZCgpXG4gICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICBjbGlja1Rocm91Z2hVUkwgPSBWQVNUVXRpbC5yZXNvbHZlVVJMVGVtcGxhdGVzKFt0aGlzLmNsaWNrVGhyb3VnaFVSTFRlbXBsYXRlXSwgdmFyaWFibGVzKVswXTtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW1pdChcImNsaWNrdGhyb3VnaFwiLCBjbGlja1Rocm91Z2hVUkwpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBWQVNUVHJhY2tlci5wcm90b3R5cGUudHJhY2sgPSBmdW5jdGlvbihldmVudE5hbWUsIG9uY2UpIHtcbiAgICAgIHZhciBpZHgsIHRyYWNraW5nVVJMVGVtcGxhdGVzO1xuICAgICAgaWYgKG9uY2UgPT0gbnVsbCkge1xuICAgICAgICBvbmNlID0gZmFsc2U7XG4gICAgICB9XG4gICAgICBpZiAoZXZlbnROYW1lID09PSAnY2xvc2VMaW5lYXInICYmICgodGhpcy50cmFja2luZ0V2ZW50c1tldmVudE5hbWVdID09IG51bGwpICYmICh0aGlzLnRyYWNraW5nRXZlbnRzWydjbG9zZSddICE9IG51bGwpKSkge1xuICAgICAgICBldmVudE5hbWUgPSAnY2xvc2UnO1xuICAgICAgfVxuICAgICAgdHJhY2tpbmdVUkxUZW1wbGF0ZXMgPSB0aGlzLnRyYWNraW5nRXZlbnRzW2V2ZW50TmFtZV07XG4gICAgICBpZHggPSB0aGlzLmVtaXRBbHdheXNFdmVudHMuaW5kZXhPZihldmVudE5hbWUpO1xuICAgICAgaWYgKHRyYWNraW5nVVJMVGVtcGxhdGVzICE9IG51bGwpIHtcbiAgICAgICAgdGhpcy5lbWl0KGV2ZW50TmFtZSwgJycpO1xuICAgICAgICB0aGlzLnRyYWNrVVJMcyh0cmFja2luZ1VSTFRlbXBsYXRlcyk7XG4gICAgICB9IGVsc2UgaWYgKGlkeCAhPT0gLTEpIHtcbiAgICAgICAgdGhpcy5lbWl0KGV2ZW50TmFtZSwgJycpO1xuICAgICAgfVxuICAgICAgaWYgKG9uY2UgPT09IHRydWUpIHtcbiAgICAgICAgZGVsZXRlIHRoaXMudHJhY2tpbmdFdmVudHNbZXZlbnROYW1lXTtcbiAgICAgICAgaWYgKGlkeCA+IC0xKSB7XG4gICAgICAgICAgdGhpcy5lbWl0QWx3YXlzRXZlbnRzLnNwbGljZShpZHgsIDEpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIFZBU1RUcmFja2VyLnByb3RvdHlwZS50cmFja1VSTHMgPSBmdW5jdGlvbihVUkxUZW1wbGF0ZXMsIHZhcmlhYmxlcykge1xuICAgICAgaWYgKHZhcmlhYmxlcyA9PSBudWxsKSB7XG4gICAgICAgIHZhcmlhYmxlcyA9IHt9O1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMubGluZWFyKSB7XG4gICAgICAgIHZhcmlhYmxlc1tcIkNPTlRFTlRQTEFZSEVBRFwiXSA9IHRoaXMucHJvZ3Jlc3NGb3JtYXRlZCgpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIFZBU1RVdGlsLnRyYWNrKFVSTFRlbXBsYXRlcywgdmFyaWFibGVzKTtcbiAgICB9O1xuXG4gICAgVkFTVFRyYWNrZXIucHJvdG90eXBlLnByb2dyZXNzRm9ybWF0ZWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBoLCBtLCBtcywgcywgc2Vjb25kcztcbiAgICAgIHNlY29uZHMgPSBwYXJzZUludCh0aGlzLnByb2dyZXNzKTtcbiAgICAgIGggPSBzZWNvbmRzIC8gKDYwICogNjApO1xuICAgICAgaWYgKGgubGVuZ3RoIDwgMikge1xuICAgICAgICBoID0gXCIwXCIgKyBoO1xuICAgICAgfVxuICAgICAgbSA9IHNlY29uZHMgLyA2MCAlIDYwO1xuICAgICAgaWYgKG0ubGVuZ3RoIDwgMikge1xuICAgICAgICBtID0gXCIwXCIgKyBtO1xuICAgICAgfVxuICAgICAgcyA9IHNlY29uZHMgJSA2MDtcbiAgICAgIGlmIChzLmxlbmd0aCA8IDIpIHtcbiAgICAgICAgcyA9IFwiMFwiICsgbTtcbiAgICAgIH1cbiAgICAgIG1zID0gcGFyc2VJbnQoKHRoaXMucHJvZ3Jlc3MgLSBzZWNvbmRzKSAqIDEwMCk7XG4gICAgICByZXR1cm4gaCArIFwiOlwiICsgbSArIFwiOlwiICsgcyArIFwiLlwiICsgbXM7XG4gICAgfTtcblxuICAgIHJldHVybiBWQVNUVHJhY2tlcjtcblxuICB9KShFdmVudEVtaXR0ZXIpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gVkFTVFRyYWNrZXI7XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuMTEuMVxuKGZ1bmN0aW9uKCkge1xuICB2YXIgVVJMSGFuZGxlciwgZmxhc2gsIHhocjtcblxuICB4aHIgPSByZXF1aXJlKCcuL3VybGhhbmRsZXJzL3htbGh0dHByZXF1ZXN0Jyk7XG5cbiAgZmxhc2ggPSByZXF1aXJlKCcuL3VybGhhbmRsZXJzL2ZsYXNoJyk7XG5cbiAgVVJMSGFuZGxlciA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBVUkxIYW5kbGVyKCkge31cblxuICAgIFVSTEhhbmRsZXIuZ2V0ID0gZnVuY3Rpb24odXJsLCBvcHRpb25zLCBjYikge1xuICAgICAgdmFyIHJlZiwgcmVzcG9uc2U7XG4gICAgICBpZiAoIWNiKSB7XG4gICAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIGNiID0gb3B0aW9ucztcbiAgICAgICAgfVxuICAgICAgICBvcHRpb25zID0ge307XG4gICAgICB9XG4gICAgICBpZiAob3B0aW9ucy5yZXNwb25zZSAhPSBudWxsKSB7XG4gICAgICAgIHJlc3BvbnNlID0gb3B0aW9ucy5yZXNwb25zZTtcbiAgICAgICAgZGVsZXRlIG9wdGlvbnMucmVzcG9uc2U7XG4gICAgICAgIHJldHVybiBjYihudWxsLCByZXNwb25zZSk7XG4gICAgICB9IGVsc2UgaWYgKChyZWYgPSBvcHRpb25zLnVybGhhbmRsZXIpICE9IG51bGwgPyByZWYuc3VwcG9ydGVkKCkgOiB2b2lkIDApIHtcbiAgICAgICAgcmV0dXJuIG9wdGlvbnMudXJsaGFuZGxlci5nZXQodXJsLCBvcHRpb25zLCBjYik7XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiB3aW5kb3cgPT09IFwidW5kZWZpbmVkXCIgfHwgd2luZG93ID09PSBudWxsKSB7XG4gICAgICAgIHJldHVybiByZXF1aXJlKCcuL3VybGhhbmRsZXJzLycgKyAnbm9kZScpLmdldCh1cmwsIG9wdGlvbnMsIGNiKTtcbiAgICAgIH0gZWxzZSBpZiAoeGhyLnN1cHBvcnRlZCgpKSB7XG4gICAgICAgIHJldHVybiB4aHIuZ2V0KHVybCwgb3B0aW9ucywgY2IpO1xuICAgICAgfSBlbHNlIGlmIChmbGFzaC5zdXBwb3J0ZWQoKSkge1xuICAgICAgICByZXR1cm4gZmxhc2guZ2V0KHVybCwgb3B0aW9ucywgY2IpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGNiKG5ldyBFcnJvcignQ3VycmVudCBjb250ZXh0IGlzIG5vdCBzdXBwb3J0ZWQgYnkgYW55IG9mIHRoZSBkZWZhdWx0IFVSTEhhbmRsZXJzLiBQbGVhc2UgcHJvdmlkZSBhIGN1c3RvbSBVUkxIYW5kbGVyJykpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gVVJMSGFuZGxlcjtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gVVJMSGFuZGxlcjtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS4xMS4xXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBGbGFzaFVSTEhhbmRsZXI7XG5cbiAgRmxhc2hVUkxIYW5kbGVyID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIEZsYXNoVVJMSGFuZGxlcigpIHt9XG5cbiAgICBGbGFzaFVSTEhhbmRsZXIueGRyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgeGRyO1xuICAgICAgaWYgKHdpbmRvdy5YRG9tYWluUmVxdWVzdCkge1xuICAgICAgICB4ZHIgPSBuZXcgWERvbWFpblJlcXVlc3QoKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB4ZHI7XG4gICAgfTtcblxuICAgIEZsYXNoVVJMSGFuZGxlci5zdXBwb3J0ZWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiAhIXRoaXMueGRyKCk7XG4gICAgfTtcblxuICAgIEZsYXNoVVJMSGFuZGxlci5nZXQgPSBmdW5jdGlvbih1cmwsIG9wdGlvbnMsIGNiKSB7XG4gICAgICB2YXIgeGRyLCB4bWxEb2N1bWVudDtcbiAgICAgIGlmICh4bWxEb2N1bWVudCA9IHR5cGVvZiB3aW5kb3cuQWN0aXZlWE9iamVjdCA9PT0gXCJmdW5jdGlvblwiID8gbmV3IHdpbmRvdy5BY3RpdmVYT2JqZWN0KFwiTWljcm9zb2Z0LlhNTERPTVwiKSA6IHZvaWQgMCkge1xuICAgICAgICB4bWxEb2N1bWVudC5hc3luYyA9IGZhbHNlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGNiKG5ldyBFcnJvcignRmxhc2hVUkxIYW5kbGVyOiBNaWNyb3NvZnQuWE1MRE9NIGZvcm1hdCBub3Qgc3VwcG9ydGVkJykpO1xuICAgICAgfVxuICAgICAgeGRyID0gdGhpcy54ZHIoKTtcbiAgICAgIHhkci5vcGVuKCdHRVQnLCB1cmwpO1xuICAgICAgeGRyLnRpbWVvdXQgPSBvcHRpb25zLnRpbWVvdXQgfHwgMDtcbiAgICAgIHhkci53aXRoQ3JlZGVudGlhbHMgPSBvcHRpb25zLndpdGhDcmVkZW50aWFscyB8fCBmYWxzZTtcbiAgICAgIHhkci5zZW5kKCk7XG4gICAgICB4ZHIub25wcm9ncmVzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICByZXR1cm4geGRyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB4bWxEb2N1bWVudC5sb2FkWE1MKHhkci5yZXNwb25zZVRleHQpO1xuICAgICAgICByZXR1cm4gY2IobnVsbCwgeG1sRG9jdW1lbnQpO1xuICAgICAgfTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEZsYXNoVVJMSGFuZGxlcjtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gRmxhc2hVUkxIYW5kbGVyO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjExLjFcbihmdW5jdGlvbigpIHtcbiAgdmFyIFhIUlVSTEhhbmRsZXI7XG5cbiAgWEhSVVJMSGFuZGxlciA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBYSFJVUkxIYW5kbGVyKCkge31cblxuICAgIFhIUlVSTEhhbmRsZXIueGhyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgeGhyO1xuICAgICAgeGhyID0gbmV3IHdpbmRvdy5YTUxIdHRwUmVxdWVzdCgpO1xuICAgICAgaWYgKCd3aXRoQ3JlZGVudGlhbHMnIGluIHhocikge1xuICAgICAgICByZXR1cm4geGhyO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBYSFJVUkxIYW5kbGVyLnN1cHBvcnRlZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuICEhdGhpcy54aHIoKTtcbiAgICB9O1xuXG4gICAgWEhSVVJMSGFuZGxlci5nZXQgPSBmdW5jdGlvbih1cmwsIG9wdGlvbnMsIGNiKSB7XG4gICAgICB2YXIgeGhyO1xuICAgICAgaWYgKHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCA9PT0gJ2h0dHBzOicgJiYgdXJsLmluZGV4T2YoJ2h0dHA6Ly8nKSA9PT0gMCkge1xuICAgICAgICByZXR1cm4gY2IobmV3IEVycm9yKCdYSFJVUkxIYW5kbGVyOiBDYW5ub3QgZ28gZnJvbSBIVFRQUyB0byBIVFRQLicpKTtcbiAgICAgIH1cbiAgICAgIHRyeSB7XG4gICAgICAgIHhociA9IHRoaXMueGhyKCk7XG4gICAgICAgIHhoci5vcGVuKCdHRVQnLCB1cmwpO1xuICAgICAgICB4aHIudGltZW91dCA9IG9wdGlvbnMudGltZW91dCB8fCAwO1xuICAgICAgICB4aHIud2l0aENyZWRlbnRpYWxzID0gb3B0aW9ucy53aXRoQ3JlZGVudGlhbHMgfHwgZmFsc2U7XG4gICAgICAgIHhoci5vdmVycmlkZU1pbWVUeXBlICYmIHhoci5vdmVycmlkZU1pbWVUeXBlKCd0ZXh0L3htbCcpO1xuICAgICAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XG4gICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgIHJldHVybiBjYihudWxsLCB4aHIucmVzcG9uc2VYTUwpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGNiKG5ldyBFcnJvcihcIlhIUlVSTEhhbmRsZXI6IFwiICsgeGhyLnN0YXR1c1RleHQpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiB4aHIuc2VuZCgpO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgcmV0dXJuIGNiKG5ldyBFcnJvcignWEhSVVJMSGFuZGxlcjogVW5leHBlY3RlZCBlcnJvcicpKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIFhIUlVSTEhhbmRsZXI7XG5cbiAgfSkoKTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IFhIUlVSTEhhbmRsZXI7XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuMTEuMVxuKGZ1bmN0aW9uKCkge1xuICB2YXIgVkFTVFV0aWw7XG5cbiAgVkFTVFV0aWwgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gVkFTVFV0aWwoKSB7fVxuXG4gICAgVkFTVFV0aWwudHJhY2sgPSBmdW5jdGlvbihVUkxUZW1wbGF0ZXMsIHZhcmlhYmxlcykge1xuICAgICAgdmFyIFVSTCwgVVJMcywgaSwgaiwgbGVuLCByZXN1bHRzO1xuICAgICAgVVJMcyA9IHRoaXMucmVzb2x2ZVVSTFRlbXBsYXRlcyhVUkxUZW1wbGF0ZXMsIHZhcmlhYmxlcyk7XG4gICAgICByZXN1bHRzID0gW107XG4gICAgICBmb3IgKGogPSAwLCBsZW4gPSBVUkxzLmxlbmd0aDsgaiA8IGxlbjsgaisrKSB7XG4gICAgICAgIFVSTCA9IFVSTHNbal07XG4gICAgICAgIGlmICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdyAhPT0gbnVsbCkge1xuICAgICAgICAgIGkgPSBuZXcgSW1hZ2UoKTtcbiAgICAgICAgICByZXN1bHRzLnB1c2goaS5zcmMgPSBVUkwpO1xuICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiByZXN1bHRzO1xuICAgIH07XG5cbiAgICBWQVNUVXRpbC5yZXNvbHZlVVJMVGVtcGxhdGVzID0gZnVuY3Rpb24oVVJMVGVtcGxhdGVzLCB2YXJpYWJsZXMpIHtcbiAgICAgIHZhciBVUkxUZW1wbGF0ZSwgVVJMcywgaiwga2V5LCBsZW4sIG1hY3JvMSwgbWFjcm8yLCByZXNvbHZlVVJMLCB2YWx1ZTtcbiAgICAgIFVSTHMgPSBbXTtcbiAgICAgIGlmICh2YXJpYWJsZXMgPT0gbnVsbCkge1xuICAgICAgICB2YXJpYWJsZXMgPSB7fTtcbiAgICAgIH1cbiAgICAgIGlmICghKFwiQ0FDSEVCVVNUSU5HXCIgaW4gdmFyaWFibGVzKSkge1xuICAgICAgICB2YXJpYWJsZXNbXCJDQUNIRUJVU1RJTkdcIl0gPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAxLjBlKzEwKTtcbiAgICAgIH1cbiAgICAgIHZhcmlhYmxlc1tcInJhbmRvbVwiXSA9IHZhcmlhYmxlc1tcIkNBQ0hFQlVTVElOR1wiXTtcbiAgICAgIGZvciAoaiA9IDAsIGxlbiA9IFVSTFRlbXBsYXRlcy5sZW5ndGg7IGogPCBsZW47IGorKykge1xuICAgICAgICBVUkxUZW1wbGF0ZSA9IFVSTFRlbXBsYXRlc1tqXTtcbiAgICAgICAgcmVzb2x2ZVVSTCA9IFVSTFRlbXBsYXRlO1xuICAgICAgICBpZiAoIXJlc29sdmVVUkwpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBmb3IgKGtleSBpbiB2YXJpYWJsZXMpIHtcbiAgICAgICAgICB2YWx1ZSA9IHZhcmlhYmxlc1trZXldO1xuICAgICAgICAgIG1hY3JvMSA9IFwiW1wiICsga2V5ICsgXCJdXCI7XG4gICAgICAgICAgbWFjcm8yID0gXCIlJVwiICsga2V5ICsgXCIlJVwiO1xuICAgICAgICAgIHJlc29sdmVVUkwgPSByZXNvbHZlVVJMLnJlcGxhY2UobWFjcm8xLCB2YWx1ZSk7XG4gICAgICAgICAgcmVzb2x2ZVVSTCA9IHJlc29sdmVVUkwucmVwbGFjZShtYWNybzIsIHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICBVUkxzLnB1c2gocmVzb2x2ZVVSTCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gVVJMcztcbiAgICB9O1xuXG4gICAgVkFTVFV0aWwuc3RvcmFnZSA9IChmdW5jdGlvbigpIHtcbiAgICAgIHZhciBkYXRhLCBpc0Rpc2FibGVkLCBzdG9yYWdlLCBzdG9yYWdlRXJyb3I7XG4gICAgICB0cnkge1xuICAgICAgICBzdG9yYWdlID0gdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiAmJiB3aW5kb3cgIT09IG51bGwgPyB3aW5kb3cubG9jYWxTdG9yYWdlIHx8IHdpbmRvdy5zZXNzaW9uU3RvcmFnZSA6IG51bGw7XG4gICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBzdG9yYWdlRXJyb3IgPSBlcnJvcjtcbiAgICAgICAgc3RvcmFnZSA9IG51bGw7XG4gICAgICB9XG4gICAgICBpc0Rpc2FibGVkID0gZnVuY3Rpb24oc3RvcmUpIHtcbiAgICAgICAgdmFyIGUsIHRlc3RWYWx1ZTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICB0ZXN0VmFsdWUgPSAnX19WQVNUVXRpbF9fJztcbiAgICAgICAgICBzdG9yZS5zZXRJdGVtKHRlc3RWYWx1ZSwgdGVzdFZhbHVlKTtcbiAgICAgICAgICBpZiAoc3RvcmUuZ2V0SXRlbSh0ZXN0VmFsdWUpICE9PSB0ZXN0VmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICBlID0gZXJyb3I7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfTtcbiAgICAgIGlmICgoc3RvcmFnZSA9PSBudWxsKSB8fCBpc0Rpc2FibGVkKHN0b3JhZ2UpKSB7XG4gICAgICAgIGRhdGEgPSB7fTtcbiAgICAgICAgc3RvcmFnZSA9IHtcbiAgICAgICAgICBsZW5ndGg6IDAsXG4gICAgICAgICAgZ2V0SXRlbTogZnVuY3Rpb24oa2V5KSB7XG4gICAgICAgICAgICByZXR1cm4gZGF0YVtrZXldO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2V0SXRlbTogZnVuY3Rpb24oa2V5LCB2YWx1ZSkge1xuICAgICAgICAgICAgZGF0YVtrZXldID0gdmFsdWU7XG4gICAgICAgICAgICB0aGlzLmxlbmd0aCA9IE9iamVjdC5rZXlzKGRhdGEpLmxlbmd0aDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHJlbW92ZUl0ZW06IGZ1bmN0aW9uKGtleSkge1xuICAgICAgICAgICAgZGVsZXRlIGRhdGFba2V5XTtcbiAgICAgICAgICAgIHRoaXMubGVuZ3RoID0gT2JqZWN0LmtleXMoZGF0YSkubGVuZ3RoO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY2xlYXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgZGF0YSA9IHt9O1xuICAgICAgICAgICAgdGhpcy5sZW5ndGggPSAwO1xuICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzdG9yYWdlO1xuICAgIH0pKCk7XG5cbiAgICByZXR1cm4gVkFTVFV0aWw7XG5cbiAgfSkoKTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IFZBU1RVdGlsO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwidmFyIEFkTWFuYWdlciA9IGZ1bmN0aW9uKHZwYWlkQ3JlYXRpdmUpIHtcbiAgdGhpcy5fY3JlYXRpdmUgPSB2cGFpZENyZWF0aXZlO1xuXG4gIGlmICghdGhpcy5pc1ZhbGlkSW50ZXJmYWNlKHZwYWlkQ3JlYXRpdmUpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdnBhaWRDcmVhdGl2ZS5zdWJzY3JpYmUodGhpcy5vbkNsaWNrVGhydSwgJ0FkQ2xpY2tUaHJ1JywgdGhpcyk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLmlzVmFsaWRJbnRlcmZhY2UgPSBmdW5jdGlvbiAodnBhaWRBZCkge1xuICB2YXIgdnBhaWRGdW5jdGlvbnMgPSBbXG4gICAgJ2NvbGxhcHNlQWQnLFxuICAgICdleHBhbmRBZCcsXG4gICAgJ2hhbmRzaGFrZVZlcnNpb24nLFxuICAgICdpbml0QWQnLFxuICAgICdwYXVzZUFkJyxcbiAgICAncmVzaXplQWQnLFxuICAgICdyZXN1bWVBZCcsXG4gICAgJ3NraXBBZCcsXG4gICAgJ3N0YXJ0QWQnLFxuICAgICdzdG9wQWQnLFxuICAgICdzdWJzY3JpYmUnLFxuICAgICd1bnN1YnNjcmliZSdcbiAgXTtcblxuICBmb3IgKHZhciBpIGluIHZwYWlkRnVuY3Rpb25zKSB7XG4gICAgaWYgKFxuICAgICAgIXZwYWlkQWQuaGFzT3duUHJvcGVydHkodnBhaWRGdW5jdGlvbnNbaV0pICYmXG4gICAgICB0eXBlb2YgdnBhaWRBZFt2cGFpZEZ1bmN0aW9uc1tpXV0gIT09ICdmdW5jdGlvbidcbiAgICApIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdHJ1ZTtcbn07XG5cbkFkTWFuYWdlci5wcm90b3R5cGUub25DbGlja1RocnUgPSBmdW5jdGlvbih1cmwsIGlkLCBwbGF5ZXJIYW5kbGVzKSB7XG4gIGlmICh0eXBlb2YgdXJsID09PSAnb2JqZWN0Jykge1xuICAgICAgdmFyIHBhcmFtcyA9IHVybDtcbiAgICAgIHVybCA9IHBhcmFtc1swXTtcbiAgICAgIGlkID0gcGFyYW1zWzFdO1xuICAgICAgcGxheWVySGFuZGxlcyA9IHBhcmFtc1syXTtcbiAgfVxuXG4gIGlmIChwbGF5ZXJIYW5kbGVzKSB7XG4gICAgd2luZG93Lm9wZW4odXJsLCAnX2JsYW5rJyk7XG4gIH1cbn07XG5cbkFkTWFuYWdlci5wcm90b3R5cGUuaW5pdEFkID0gZnVuY3Rpb24od2lkdGgsIGhlaWdodCwgdmlld01vZGUsIGRlc2lyZWRCaXRyYXRlLCBjcmVhdGl2ZURhdGEsIGVudmlyb25tZW50VmFycykge1xuICB0aGlzLl9jcmVhdGl2ZS5pbml0QWQod2lkdGgsIGhlaWdodCwgdmlld01vZGUsIGRlc2lyZWRCaXRyYXRlLCBjcmVhdGl2ZURhdGEsIGVudmlyb25tZW50VmFycyk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLmdldEFkRXhwYW5kZWQgPSBmdW5jdGlvbiBnZXRBZEV4cGFuZGVkKCkge1xuICByZXR1cm4gdGhpcy5fY3JlYXRpdmUuZ2V0QWRFeHBhbmRlZCgpO1xufTtcblxuQWRNYW5hZ2VyLnByb3RvdHlwZS5nZXRBZFNraXBwYWJsZVN0YXRlID0gZnVuY3Rpb24gZ2V0QWRTa2lwcGFibGVTdGF0ZSgpIHtcbiAgcmV0dXJuIHRoaXMuX2NyZWF0aXZlLmdldEFkU2tpcHBhYmxlU3RhdGUoKTtcbn07XG5cbkFkTWFuYWdlci5wcm90b3R5cGUuZ2V0QWREdXJhdGlvbiA9IGZ1bmN0aW9uIGdldEFkRHVyYXRpb24oKSB7XG4gIHJldHVybiB0aGlzLl9jcmVhdGl2ZS5nZXRBZER1cmF0aW9uKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLmdldEFkV2lkdGggPSBmdW5jdGlvbiBnZXRBZFdpZHRoKCkge1xuICByZXR1cm4gdGhpcy5fY3JlYXRpdmUuZ2V0QWRXaWR0aCgpO1xufTtcblxuQWRNYW5hZ2VyLnByb3RvdHlwZS5nZXRBZEhlaWdodCA9IGZ1bmN0aW9uIGdldEFkSGVpZ2h0KCkge1xuICByZXR1cm4gdGhpcy5fY3JlYXRpdmUuZ2V0QWRIZWlnaHQoKTtcbn07XG5cbkFkTWFuYWdlci5wcm90b3R5cGUuZ2V0QWRSZW1haW5pbmdUaW1lID0gZnVuY3Rpb24gZ2V0QWRSZW1haW5pbmdUaW1lKCkge1xuICByZXR1cm4gdGhpcy5fY3JlYXRpdmUuZ2V0QWRSZW1haW5pbmdUaW1lKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnN0YXJ0QWQgPSBmdW5jdGlvbiBzdGFydEFkKCkge1xuICB0aGlzLl9jcmVhdGl2ZS5zdGFydEFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnN0b3BBZCA9IGZ1bmN0aW9uIHN0b3BBZCgpIHtcbiAgdGhpcy5fY3JlYXRpdmUuc3RvcEFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnNldEFkVm9sdW1lID0gZnVuY3Rpb24gc2V0QWRWb2x1bWUobGV2ZWwpIHtcbiAgdGhpcy5fY3JlYXRpdmUuc2V0QWRWb2x1bWUobGV2ZWwpO1xufTtcblxuQWRNYW5hZ2VyLnByb3RvdHlwZS5nZXRBZFZvbHVtZSA9IGZ1bmN0aW9uIGdldEFkVm9sdW1lKCkge1xuICByZXR1cm4gdGhpcy5fY3JlYXRpdmUuZ2V0QWRWb2x1bWUoKTtcbn07XG5cbkFkTWFuYWdlci5wcm90b3R5cGUucmVzaXplQWQgPSBmdW5jdGlvbiByZXNpemVBZCh3aWR0aCwgaGVpZ2h0LCB2aWV3TW9kZSkge1xuICB0aGlzLl9jcmVhdGl2ZS5yZXNpemVBZCh3aWR0aCwgaGVpZ2h0LCB2aWV3TW9kZSk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnBhdXNlQWQgPSBmdW5jdGlvbiBwYXVzZUFkKCkge1xuICB0aGlzLl9jcmVhdGl2ZS5wYXVzZUFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnJlc3VtZUFkID0gZnVuY3Rpb24gcmVzdW1lQWQoKSB7XG4gIHRoaXMuX2NyZWF0aXZlLnJlc3VtZUFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLmV4cGFuZEFkID0gZnVuY3Rpb24gZXhwYW5kQWQoKSB7XG4gIHRoaXMuX2NyZWF0aXZlLmV4cGFuZEFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLmNvbGxhcHNlQWQgPSBmdW5jdGlvbiBjb2xsYXBzZUFkKCkge1xuICB0aGlzLl9jcmVhdGl2ZS5jb2xsYXBzZUFkKCk7XG59O1xuXG5BZE1hbmFnZXIucHJvdG90eXBlLnNraXBBZCA9IGZ1bmN0aW9uIHNraXBBZCgpIHtcbiAgdGhpcy5fY3JlYXRpdmUuc2tpcEFkKCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgJ0FkTWFuYWdlcic6IEFkTWFuYWdlclxufTsiLCJ2YXIgVlBBSURIVE1MNSA9IHJlcXVpcmUoJy4vdnBhaWQuaHRtbDUnKS5WUEFJREhUTUw1O1xuXG52YXIgZGFzaGJpZFRhZyA9ICdodHRwOi8vc2VhcmNoLnNwb3R4Y2hhbmdlLmNvbS92YXN0LzIuMDAvODUzOTQ/VlBBSUQ9anMmY29udGVudF9wYWdlX3VybD1fX3BhZ2UtdXJsX18mY2I9X19yYW5kb20tbnVtYmVyX18mZGV2aWNlW29zXT1BbmRyb2lkJmRldmljZVtkZXZpY2V0eXBlXT0xJmRldmljZVtkbnRdPTAnO1xuXG5uZXcgVlBBSURIVE1MNShcbiAgZGFzaGJpZFRhZyxcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3ZpZGVvLXNsb3QnKSxcbiAgcmVnaXN0ZXJMb2dnaW5nRXZlbnRzXG4pO1xuXG5mdW5jdGlvbiByZWdpc3RlckxvZ2dpbmdFdmVudHMoKSB7XG4gIHZhciB2cGFpZEV2ZW50cyA9IFtcbiAgICAnQWRTdGFydGVkJyxcbiAgICAnQWRTdG9wcGVkJyxcbiAgICAnQWRTa2lwcGVkJyxcbiAgICAnQWRMb2FkZWQnLFxuICAgICdBZExpbmVhckNoYW5nZScsXG4gICAgJ0FkU2l6ZUNoYW5nZScsXG4gICAgJ0FkRXhwYW5kZWRDaGFuZ2UnLFxuICAgICdBZFNraXBwYWJsZVN0YXRlQ2hhbmdlJyxcbiAgICAnQWREdXJhdGlvbkNoYW5nZScsXG4gICAgJ0FkUmVtYWluaW5nVGltZUNoYW5nZScsXG4gICAgJ0FkVm9sdW1lQ2hhbmdlJyxcbiAgICAnQWRJbXByZXNzaW9uJyxcbiAgICAnQWRDbGlja1RocnUnLFxuICAgICdBZEludGVyYWN0aW9uJyxcbiAgICAnQWRWaWRlb1N0YXJ0JyxcbiAgICAnQWRWaWRlb0ZpcnN0UXVhcnRpbGUnLFxuICAgICdBZFZpZGVvTWlkcG9pbnQnLFxuICAgICdBZFZpZGVvVGhpcmRRdWFydGlsZScsXG4gICAgJ0FkVmlkZW9Db21wbGV0ZScsXG4gICAgJ0FkVXNlckFjY2VwdEludml0YXRpb24nLFxuICAgICdBZFVzZXJNaW5pbWl6ZScsXG4gICAgJ0FkVXNlckNsb3NlJyxcbiAgICAnQWRQYXVzZWQnLFxuICAgICdBZFBsYXlpbmcnLFxuICAgICdBZEVycm9yJyxcbiAgICAnQWRMb2cnXG4gICAgXTtcbiAgdmFyIGxvZ0VsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYWQtbG9nLWJvZHknKTtcblxuICB2YXIgc3RhcnRUaW1lID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG5cbiAgZm9yICh2YXIgaSBpbiB2cGFpZEV2ZW50cykge1xuICAgIHRoaXMub24odnBhaWRFdmVudHNbaV0sIGZ1bmN0aW9uICh2cGFpZEV2ZW50KSB7XG4gICAgICB2YXIgdGltZUVsYXBzZWQgPSAoKG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gc3RhcnRUaW1lKSAvIDEwMDApLnRvRml4ZWQoMSk7XG4gICAgICBsb2dFbGVtZW50LmlubmVySFRNTCA9ICc8dHI+PHRkPicgKyB0aW1lRWxhcHNlZCArICc8L3RkPjx0ZD4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZwYWlkRXZlbnQudHlwZSArICc8L3RkPjwvdHI+JyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsb2dFbGVtZW50LmlubmVySFRNTDtcbiAgICB9KTtcblxuICB9XG59IiwicmVxdWlyZSgnLi8uLi9ub2RlX21vZHVsZXMvZG9tNC9idWlsZC9kb200Jyk7ICAgIC8vIGZvciBDdXN0b21FdmVudCBzdXBwb3J0IGluIElFXG5cbnZhciBETVZBU1QgPSByZXF1aXJlKCcuLy4uL25vZGVfbW9kdWxlcy92YXN0LWNsaWVudCcpO1xudmFyIEFkTWFuYWdlciA9IHJlcXVpcmUoJy4vQWRNYW5hZ2VyLmpzJykuQWRNYW5hZ2VyO1xuXG52YXIgVlBBSURIVE1MNSA9IGZ1bmN0aW9uKGFkVGFnLCB2aWRlb0VsZW1lbnQsIGNiKSB7XG4gIHZhciB0aGF0ID0gdGhpcyxcbiAgICAgIHZhc3RUYWcgPSB0aGlzLmFwcGx5VGFnTWFjcm9zKGFkVGFnKTtcblxuICB0aGlzLl9jYk9uUmVhZHkgPSBjYjtcbiAgdGhpcy5hZEluZm8gPSB7fTtcbiAgdGhpcy52aWRlb0VsZW1lbnQgPSB2aWRlb0VsZW1lbnQ7XG5cbiAgLy8gd2FpdCBmb3IgdmlkZW8gcGxheWJhY2sgdG8gc3RhcnQ7IGJyb3dzZXJzIHRoYXQgYmxvY2sgYXV0b3BsYXkgbWFrZSBhZHMgc2FkXG4gIHZpZGVvRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdwbGF5aW5nJywgZnVuY3Rpb24gd2FpdEZvclZpZGVvUmVhZHkoKSB7XG4gICAgdGhhdC52aWRlb0VsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcigncGxheWluZycsIHdhaXRGb3JWaWRlb1JlYWR5KTtcblxuICAgIERNVkFTVC5jbGllbnQuZ2V0KHZhc3RUYWcsIG9uVkFTVFJlc3BvbnNlLmJpbmQodGhhdCkpO1xuICB9KTtcblxuICAvLyBsb2FkIGFuZCBwbGF5IGFkXG4gIGZ1bmN0aW9uIG9uVkFTVFJlc3BvbnNlKHJlc3BvbnNlKSB7XG4gICAgaWYgKCFyZXNwb25zZSB8fCAhcmVzcG9uc2UuYWRzLmxlbmd0aCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuYWRJbmZvID0gcmVzcG9uc2UuYWRzWzBdOyAgICAvLyBUT0RPOiBvbmx5IHBsYXlzIGZpcnN0IGFkXG5cbiAgICB0aGlzLmxvYWRBZChmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLl9jYk9uUmVhZHkoKTtcbiAgICAgIHRoaXMuc3RhcnRFdmVudExpc3RlbmVycygpO1xuXG4gICAgICB0aGlzLnBsYXlBZCgpOyAgICAgIC8vIGF1dG9wbGF5XG4gICAgfS5iaW5kKHRoaXMpKTtcbiAgfVxufTtcblxuVlBBSURIVE1MNS5wcm90b3R5cGUuYXBwbHlUYWdNYWNyb3MgPSBmdW5jdGlvbiBhcHBseVRhZ01hY3Jvcyh2YXN0VGFnKSB7XG4gIHZhciBtYWNyb3MgPSB7XG4gICAgJ19fcGFnZS11cmxfXyc6IHdpbmRvdy5sb2NhdGlvbi5ocmVmLFxuICAgICdfX3JhbmRvbS1udW1iZXJfXyc6IE1hdGgucm91bmQobmV3IERhdGUoKS5nZXRUaW1lKCkgLyAxMDAwKVxuICB9O1xuXG4gIGZvciAodmFyIG1hY3JvIGluIG1hY3Jvcykge1xuICAgIHZhc3RUYWcgPSB2YXN0VGFnLnJlcGxhY2UobWFjcm8sIG1hY3Jvc1ttYWNyb10pO1xuICB9XG5cbiAgcmV0dXJuIHZhc3RUYWc7XG59O1xuXG4vLyBUT0RPOiBtYWtlIFByb21pc2UtY29tcGF0aWJsZVxuVlBBSURIVE1MNS5wcm90b3R5cGUubG9hZEFkID0gZnVuY3Rpb24gaW5pdChjYikge1xuICB2YXIgbWVkaWFGaWxlID0gdGhpcy5nZXRNZWRpYUZpbGUoKTtcblxuICB2YXIganNDcmVhdGl2ZVRhZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICBqc0NyZWF0aXZlVGFnLnNyYyA9IG1lZGlhRmlsZS5maWxlVVJMO1xuICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKGpzQ3JlYXRpdmVUYWcpO1xuXG4gIHZhciB0aW1lU3RhcnQgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgdmFyIGFkUmVhZHlJbnRlcnZhbCA9IHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuICAgIHZhciBfZ2V0VlBBSURBZCA9IHdpbmRvd1snZ2V0VlBBSURBZCddO1xuXG4gICAgaWYgKF9nZXRWUEFJREFkICYmIHR5cGVvZiBfZ2V0VlBBSURBZCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgY2xlYXJJbnRlcnZhbChhZFJlYWR5SW50ZXJ2YWwpO1xuXG4gICAgICB2YXIgdnBhaWRBZCA9IF9nZXRWUEFJREFkKCk7XG4gICAgICB0aGlzLmFkTWFuYWdlciA9IG5ldyBBZE1hbmFnZXIodnBhaWRBZCk7XG4gICAgICByZXR1cm4gY2IoKTtcbiAgICB9XG5cbiAgICB2YXIgdGltZU5vdyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIGlmICh0aW1lTm93ID4gKHRpbWVTdGFydCArIDI1MDApKSB7XG4gICAgICBjbGVhckludGVydmFsKGFkUmVhZHlJbnRlcnZhbCk7XG5cbiAgICAgIGNvbnNvbGUud2FybigndnBhaWQgaW50ZXJmYWNlIHVuYXZhaWxhYmxlLi4uIHRpbWluZyBvdXQuJyk7XG4gICAgfVxuICB9LmJpbmQodGhpcyksIDUwKTtcbn07XG5cblZQQUlESFRNTDUucHJvdG90eXBlLnN0YXJ0RXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbiBzdGFydEV2ZW50TGlzdGVuZXJzKCkge1xuICB0aGlzLm9uKCdBZExvYWRlZCcsIGZ1bmN0aW9uIChlKSB7XG4gICAgdGhpcy5hZE1hbmFnZXIuc3RhcnRBZCgpO1xuICB9LmJpbmQodGhpcykpO1xuXG4gIHRoaXMub24oJ0FkRXJyb3InLCBmdW5jdGlvbiAoZSkge1xuICAgIHRoaXMua2lsbEFkKCk7XG4gIH0uYmluZCh0aGlzKSk7XG5cbiAgdGhpcy5vbignQWRTdG9wcGVkJywgZnVuY3Rpb24gKGUpIHtcbiAgICB0aGlzLnRlYXJEb3duKCk7XG4gIH0uYmluZCh0aGlzKSk7XG5cbiAgdGhpcy5vbignQWRFcnJvciBBZExvZycsIGZ1bmN0aW9uIGxvZ0V2ZW50T3V0cHV0KGFkRXZlbnQpIHtcbiAgICBjb25zb2xlLmxvZyhhZEV2ZW50LnR5cGUgKyAnOiAnICsgYWRFdmVudC5kZXRhaWwubWVzc2FnZSk7XG4gIH0pO1xufTtcblxuVlBBSURIVE1MNS5wcm90b3R5cGUucGxheUFkID0gZnVuY3Rpb24gcGxheUFkKCkge1xuICB2YXIgY3JlYXRpdmUgPSB0aGlzLmdldENyZWF0aXZlKCk7XG4gIHZhciBlbnZpcm9ubWVudFZhcnMgPSB7XG4gICAgdmlkZW9TbG90OiB0aGlzLnZpZGVvRWxlbWVudCxcbiAgICBzbG90OiB0aGlzLnZpZGVvRWxlbWVudC5wYXJlbnROb2RlXG4gIH07XG5cbiAgdGhpcy5hZE1hbmFnZXIuaW5pdEFkKFxuICAgIHBhcnNlSW50KGVudmlyb25tZW50VmFycy52aWRlb1Nsb3Quc3R5bGUud2lkdGgpLFxuICAgIHBhcnNlSW50KGVudmlyb25tZW50VmFycy52aWRlb1Nsb3Quc3R5bGUuaGVpZ2h0KSxcbiAgICAnbm9ybWFsJyxcbiAgICAtMSxcbiAgICBjcmVhdGl2ZS5hZFBhcmFtZXRlcnMsXG4gICAgZW52aXJvbm1lbnRWYXJzXG4gICk7XG59O1xuXG4vLyBmb3IgZmFpbGluZyBmYXN0XG5WUEFJREhUTUw1LnByb3RvdHlwZS5raWxsQWQgPSBmdW5jdGlvbiBraWxsQWQoKSB7XG4gIGNvbnNvbGUud2FybignZm9yY2luZyBhZCBlbmQuLi4nKTtcblxuICB0aGlzLnRlYXJEb3duKCk7XG4gIHRoaXMudmlkZW9FbGVtZW50LnBsYXkoKTtcbn07XG5cbi8vIGNsZWFuIHVwIGFkIGlmcmFtZXMgYW5kIHN1Y2gganVzdCBpbiBjYXNlIG9mIGZhdGFsIGVycm9yc1xuVlBBSURIVE1MNS5wcm90b3R5cGUudGVhckRvd24gPSBmdW5jdGlvbiB0ZWFyRG93bigpIHtcbiAgaWYgKHRoaXMuYWRNYW5hZ2VyICYmIHRoaXMuYWRNYW5hZ2VyLnN0b3BBZCkge1xuICAgIHRoaXMuYWRNYW5hZ2VyLnN0b3BBZCgpO1xuICB9XG5cbiAgdmFyIGFkV3JhcHBlciA9IHRoaXMudmlkZW9FbGVtZW50LnBhcmVudEVsZW1lbnQ7XG4gIHZhciBhZEVsZW1lbnRzID0gYWRXcmFwcGVyLmNoaWxkcmVuO1xuXG4gIGlmIChhZEVsZW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICBmb3IgKHZhciBpPWFkRWxlbWVudHMubGVuZ3RoLTE7aSA+PSAwO2ktLSkge1xuICAgICAgaWYgKGFkRWxlbWVudHNbaV0gIT09IHRoaXMudmlkZW9FbGVtZW50KSB7XG4gICAgICAgIGFkV3JhcHBlci5yZW1vdmVDaGlsZChhZEVsZW1lbnRzW2ldKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn07XG5cbi8vIGludGVyZmFjZSBmb3IgLnN1YnNjcmliZSgpXG5WUEFJREhUTUw1LnByb3RvdHlwZS5vbiA9IGZ1bmN0aW9uIG9uKGV2ZW50TmFtZSwgZm4pIHtcbiAgdmFyIGV2ZW50cyA9IGV2ZW50TmFtZS5zcGxpdCgnICcpO1xuXG4gIGZvciAodmFyIGkgaW4gZXZlbnRzKSB7XG4gICAgdmFyIGV2ZW50RGV0YWlscyA9IHtcbiAgICAgIGRldGFpbDoge1xuICAgICAgICBvbkV2ZW50OiBmbixcbiAgICAgICAgY3JlYXRpdmU6IHRoaXMuZ2V0Q3JlYXRpdmUoKSxcbiAgICAgICAgbWVkaWFGaWxlOiB0aGlzLmdldE1lZGlhRmlsZSgpXG4gICAgICB9XG4gICAgfTtcbiAgICB2YXIgYWRFdmVudCA9IG5ldyBDdXN0b21FdmVudChldmVudHNbaV0sIGV2ZW50RGV0YWlscyk7XG5cbiAgICB0aGlzLmFkTWFuYWdlci5fY3JlYXRpdmUuc3Vic2NyaWJlKFxuICAgICAgZnVuY3Rpb24gKG1lc3NhZ2UpIHtcbiAgICAgICAgdGhpcy5kZXRhaWwubWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgICAgIHRoaXMuZGV0YWlsLm9uRXZlbnQodGhpcyk7XG4gICAgICB9LmJpbmQoYWRFdmVudCksXG4gICAgICBldmVudHNbaV0sXG4gICAgICB0aGlzKTtcbiAgfVxufTtcblxuLy8gaW50ZXJmYWNlIGZvciAudW5zdWJzY3JpYmUoKVxuVlBBSURIVE1MNS5wcm90b3R5cGUub2ZmID0gZnVuY3Rpb24gb2ZmKGZuLCBldmVudE5hbWUpIHtcbiAgdmFyIGV2ZW50cyA9IGV2ZW50TmFtZS5zcGxpdCgnICcpO1xuXG4gIGZvciAodmFyIGkgaW4gZXZlbnRzKSB7XG4gICAgdGhpcy5hZE1hbmFnZXIuX2NyZWF0aXZlLnVuc3Vic2NyaWJlKGZuLCBldmVudHNbaV0sIHRoaXMpO1xuICB9XG59O1xuXG4vLyBmaW5kIGFuZCByZXR1cm4gdGhlIGZpcnN0IGxpbmVhciBjcmVhdGl2ZSBhdmFpbGFibGUgaW4gdGhlIFZBU1QgcmVzcG9uc2VcblZQQUlESFRNTDUucHJvdG90eXBlLmdldENyZWF0aXZlID0gZnVuY3Rpb24gZ2V0Q3JlYXRpdmUoKSB7XG4gIGZvciAodmFyIGkgaW4gdGhpcy5hZEluZm8uY3JlYXRpdmVzKSB7XG4gICAgaWYgKHRoaXMuYWRJbmZvLmNyZWF0aXZlc1tpXS50eXBlID09PSAnbGluZWFyJykge1xuICAgICAgcmV0dXJuIHRoaXMuYWRJbmZvLmNyZWF0aXZlc1tpXTtcbiAgICB9XG4gIH1cbn07XG5cbi8vIGdldCBtZWRpYSBmaWxlIHdpdGggaWRlYWwgZGltZW5zaW9ucyBmb3IgYWQgcGxhY2VtZW50XG5WUEFJREhUTUw1LnByb3RvdHlwZS5nZXRNZWRpYUZpbGUgPSBmdW5jdGlvbiBnZXRNZWRpYUZpbGUoKSB7XG4gIHZhciBjcmVhdGl2ZSA9IHRoaXMuZ2V0Q3JlYXRpdmUoKSxcbiAgICAgIGlkZWFsTWVkaWFGaWxlO1xuXG4gIGZvciAodmFyIGkgaW4gY3JlYXRpdmUubWVkaWFGaWxlcykge1xuICAgIHZhciBtZWRpYUZpbGUgPSBjcmVhdGl2ZS5tZWRpYUZpbGVzW2ldO1xuXG4gICAgaWYgKCFpZGVhbE1lZGlhRmlsZSkge1xuICAgICAgaWRlYWxNZWRpYUZpbGUgPSBtZWRpYUZpbGU7XG4gICAgfVxuXG4gICAgaWYgKFxuICAgICAgbWVkaWFGaWxlLm1pbWVUeXBlID09ICdhcHBsaWNhdGlvbi9qYXZhc2NyaXB0JyAmJlxuICAgICAgbWVkaWFGaWxlLndpZHRoID4gaWRlYWxNZWRpYUZpbGUud2lkdGggJiZcbiAgICAgIG1lZGlhRmlsZS53aWR0aCA8PSB0aGlzLnZpZGVvRWxlbWVudC5zdHlsZS53aWR0aFxuICAgICkge1xuICAgICAgaWRlYWxNZWRpYUZpbGUgPSBtZWRpYUZpbGU7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGlkZWFsTWVkaWFGaWxlO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICdWUEFJREhUTUw1JzogVlBBSURIVE1MNVxufTtcbiJdfQ==
